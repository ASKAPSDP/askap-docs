Calibration solvers
===================

Two calibration solvers are available in ASKAPsoft. The *solver* parset parameter defines 
the type of the calibration solver to use with the choice between *SVD*, and *LSQR*. The *SVD* solver
is based on calculation of the singular value decomposition. It generally performs well for small problem sizes,
but shows poor performance for large number of antennas. It does not require setting free parameters.

The iterative least-squares solver *LSQR* (C. Paige & M. Saunders, 1982) is a high performance solver, that is capable of performing calibration 
on the SKA scales (i.e., very large number of antennas). It scales nearly linearly with the number of data, i.e.,
quadratically with the number of antennas.
It requires setting several free parameters, such as stopping criteria and damping parameters.
Due to its general least-square nature, it also allows adding additional constraints into the system of equations 
(for example, solution smoothness in frequency). For more details, see https://doi.org/10.1145/355984.355989

Common Parameters
-----------------

Parameters for all solvers:

+-------------------+--------------+--------------+--------------------------------------------------------+
|**Parameter**      |**Type**      |**Default**   |**Description**                                         |
+===================+==============+==============+========================================================+
|solver             |string        |SVD           |Selection of solver. Either "SVD" or "LSQR".            |
+-------------------+--------------+--------------+--------------------------------------------------------+

The **SVD** solver does not require any additional parameters.
Additional parameters understood by the **LSQR** solver are given in the following section.

LSQR Solver Parameters
----------------------

All parameters given in the next table have **solver.LSQR** prefix (e.g., Cbpcalibrator.solver.LSQR.niter).

+-------------------+--------------+--------------+--------------------------------------------------------+
|**Parameter**      |**Type**      |**Default**   |**Description**                                         |
+===================+==============+==============+========================================================+
|rmin               |float         |1.e-13        |Minimum relative residual stopping criterion.           |
+-------------------+--------------+--------------+--------------------------------------------------------+
|niter              |int           |100           |Maximum number of minor cycles.                         |
|                   |              |              |This stopping criterion is used when minimum relative   |
|                   |              |              |residual stopping criterion could not be reached within |
|                   |              |              |the specified number of solver iterations.              |
|                   |              |              |In practice 10-15 iterations are often sufficient.      |
+-------------------+--------------+--------------+--------------------------------------------------------+
|alpha              |float         |0.01          |The damping on the solution perturbation (that is       |
|                   |              |              |performed at every major cycle). Nonzero damping values |
|                   |              |              |help dealing with rank deficient systems.               |
|                   |              |              |Testing has shown the default works fine for gain or    |
|                   |              |              |bandpass alone, but that values of 1e5 to 3e6 are often |
|                   |              |              |needed when solving for leakages alone or combined with |
|                   |              |              |gains or bandpass. For data with a fair amount of       |
|                   |              |              |flagging use 3e5 to 1e6 and solve leakages combined with|
|                   |              |              |gains or bandpass to get agreement with SVD results.    |
+-------------------+--------------+--------------+--------------------------------------------------------+
|alpha_leak         |float         |alpha         |The same type of damping as alpha, but applied to       |
|                   |              |              |leakages only. When it is defined explicitly then       |
|                   |              |              |alpha is used for gains, and alpha_leak for leakages.   |
+-------------------+--------------+--------------+--------------------------------------------------------+
|verbose            |bool          |false         |The value of "true" enables lots of output.             |
+-------------------+--------------+--------------+--------------------------------------------------------+
|parallelMatrix     |bool          |false         |Enables mode where all frequency channels               |
|                   |              |              |are solved together, and therefore can be coupled       |
|                   |              |              |via frequency-dependant constraints (e.g. smoothness).  |
|                   |              |              |The columns of the (block-diagonal) matrix,             |
|                   |              |              |and the bandpass solution                               |
|                   |              |              |are split between different ranks.                      |
|                   |              |              |This option is only supported in ccalibrator,           |
|                   |              |              |and must be accompanied by the following options:       |
|                   |              |              |*Ccalibrator.solver=LSQR*, *Ccalibrator.solve=bandpass*.|
|                   |              |              |The data partitioning is then defined via setting       |
|                   |              |              |*Ccalibrator.chanperworker*, and *Ccalibrator.chunk*,   |
|                   |              |              |in analogous to *Channels* option in cimager            |
|                   |              |              |(see :doc:`cimager` for more details).                  |
+-------------------+--------------+--------------+--------------------------------------------------------+
|smoothing          |bool          |false         |Enables smoothing constraints along frequency channels. |
|                   |              |              |This allows obtaining smoother gain phase and amplitude.|
|                   |              |              |Must be used together                                   |
|                   |              |              |with *Ccalibrator.solve=bandpass* option.               |
+-------------------+--------------+--------------+--------------------------------------------------------+
|normalizeColumns   |bool          |false         |Precondition the matrix by normalizing the columns.     |
|                   |              |              |The solution is then sought in the scaled variables.    |
+-------------------+--------------+--------------+--------------------------------------------------------+

Smoothing Parameters
--------------------
All parameters given in the next table have **solver.LSQR.smoothing** prefix (e.g., Ccalibrator.solver.LSQR.smoothing.type).

+-------------------+--------------+--------------+--------------------------------------------------------+
|**Parameter**      |**Type**      |**Default**   |**Description**                                         |
+===================+==============+==============+========================================================+
|type               |int           |2             |Type of smoothing gradient operator: 0-forward diff,    |
|                   |              |              |1-central diff, 2-Laplacian, 4-Bilaplacian.             |
+-------------------+--------------+--------------+--------------------------------------------------------+
|variableType       |int           |0             |Type of smoothing variable: 0-real/imag gain parts,     |
|                   |              |              |1-gain magnitude, 2-gain phase, 3-magnitude and phase.  |
+-------------------+--------------+--------------+--------------------------------------------------------+
|accuracy           |int           |2             |Finite difference accuracy order. Support 2, 4, and 6.  |
+-------------------+--------------+--------------+--------------------------------------------------------+
|minWeight          |float         |0.0           |Starting smoothing weight (at the first major cycle).   |
+-------------------+--------------+--------------+--------------------------------------------------------+
|maxWeight          |float         |3.e+6         |Final smoothing weight (after *nsteps* cycles).         |
|                   |              |              |Basically, this controls the degree of smoothness.      |
+-------------------+--------------+--------------+--------------------------------------------------------+
|level              |float         |0.0           |The smoothing level, e.g. weight^2||∇g-level||^2.       |
|                   |              |              |Zero corresponds to maximum smoothing,                  |
|                   |              |              |and higher values to less smooth solutions.             |
+-------------------+--------------+--------------+--------------------------------------------------------+
|nsteps             |int           |10            |The number of major cycles during which the smoothing   |
|                   |              |              |weight is increased from *minWeight* to *maxWeight*,    |
|                   |              |              |using logarithmic steps.                                |
+-------------------+--------------+--------------+--------------------------------------------------------+


Examples:
---------

LSQR Solver
~~~~~~~~~~~

.. code-block:: bash

    Cbpcalibrator.solver                            = LSQR
    Cbpcalibrator.solver.LSQR.niter                 = 100
    Cbpcalibrator.solver.LSQR.rmin                  = 1.e-13
    Cbpcalibrator.solver.LSQR.alpha                 = 0.01
    Cbpcalibrator.solver.LSQR.verbose               = false
    
LSQR Solver with smoothing constraints
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The follwing example distributes 200 channels to 4 worker (50 channels per worker), so it must be run using 5 MPI processors (4 worker and 1 master).
Only the minimal set of relevant parameters to setup the solver is shown below.
Note, that the ccalibrator and the parallel matrix mode should be used for smoothing constraints.

.. code-block:: bash

    Ccalibrator.solve                             = bandpass
    Ccalibrator.solver                            = LSQR
    #
    Ccalibrator.nChan                             = 200
    Ccalibrator.chanperworker                     = 50
    Ccalibrator.chunk                             = %w
    #
    Ccalibrator.ncycles                           = 20
    #
    Ccalibrator.solver.LSQR.parallelMatrix        = true
    Ccalibrator.solver.LSQR.smoothing             = true
    Ccalibrator.solver.LSQR.smoothing.maxWeight   = 3.e+10
    Ccalibrator.solver.LSQR.smoothing.type        = 4

