User Parameters - Spectral-line Imaging
=======================================

There are several steps involved in running the spectral-line imaging,
with several optional pre-processing steps:

1. The :doc:`../calim/mssplit` tool may be used to copy a nominated
   channel range, and/or average a nominated number of channels
   together, to form a new spectral-line MS.
2. The gains solution from the continuum self-calibration can be
   applied to the spectral-line MS using ccalapply.
3. The continuum can be subtracted from the spectral-line MS using
   ccontsubtract. The continuum is represented by either the clean
   model from the continuum imaging, the component catalogue generated
   by Selavy, or a model image constructed by Cmodel from that catalogue.
   The Selavy parameters used are those described on
   :doc:`ContinuumSourcefinding`.
4. The continuum (or residual continuum after the previous subtraction) can also
   be fitted and subtracted by the uvlin feature of ccontsubtract which allows
   visibility-based modeling and subtraction method using basis
   functions comprising of polynomials and harmonics. This implements some of
   the functionality of the previous (separate) tool to do this:
5. The tool ``remUVcont`` allows removal of both continuum as well
   as instrument-induced non-idealities. As the name suggests this is
   a visibility-based modeling and subtraction method using basis
   functions comprising of polynomials and harmonics with useful
   frills suitable for dealing with instrumental systematics.

These preprocessing steps, other than continuum subtraction, may be
done in a single slurm job if ``SINGLE_JOB_PREIMAGING=true`` (which is
the case by default).

Following this pre-processing, the resulting MS is imaged by either the
imager task (the default), or the old simager, creating a set of
spectral cubes. Imager provides the ability to image in the
barycentric reference frame, and allows (for efficiency purposes) the
option of writing out multiple sub-cubes (each having a subset of the
full range of channels).

The output spectral fram can be specified by the ``FREQ_FRAME_SL``
parameter, which can be one of "topo", "bary", "lsrk". For the latter
two, the direction used for the frame conversion is taken from the
``DOPPLER_TRACKING_DIRECTION`` parameter. This can take one of two
special values: "beam" (the default) means use the centre direction of
the current beam (and so each beam will get a slightly different
correction), and "target" means use the pointing centre (if there is
more than one interleave position, the first is used). Alternatively,
a direction specification of the form "[ra, dec, equinox]" can be
provided.

The point at which the Doppler correction is done is chosen by the
parameter ``DOPPLER_CORRECTION_IN_IMAGER``. If ``true``, the
correction will be done at the start of the imager job, when the data
is read in. If ``false``, it will be done in the mssplit job that runs
prior to imaging. This is the job that makes a copy of the data (with
an optional channel-range cut), triggered by ``DO_COPY_SL``.

In the latter case, the frame correction can make use of interpolation
to improve the accuracy, with the type of interpolation given by
``DOPPLER_CORRECTION_INTERPOLATION``.

When the mssplit option is used, it is advisable to not use the UV
continuum subtraction option ``DO_CONT_SUB_SL``, since the continuum
subtraction is done after the splitting, and the doppler correction
will change the location of topocentric features such as beamforming
regions. A check is in place to prevent inadvertent use in this
scenario, although an override parameter
``OVERRIDE_CONTSUB_MSSPLIT_DOPPLER_CHECK`` exists to avoid the check.

For the imaging, the default approach (as for the continuum imaging)
is to use preconditioning to weight the visibilities, although
traditional weighting is also now available through the the use of
``TRADITIONAL_WEIGHT_PARAM_SEPCTRAL_LIST`` and
``TRADITIONAL_WEIGHT_SPECTRAL_ROBUSTNESS``.

A prototype option is available (using
``DO_MFS_STARTING_MODEL_SPECTRAL``) to use the model image from the
MFS (continuum) imaging as the starting point for the clean model in
the spectral cube imaging. This will only work when the cellsize in
the spectral image matches that in the continuum image, and is best
used when the continuum is still present in the spectral MS (ie. no
UV-based continuum subtraction has been used).


Following imaging, the cube statistics are calculated, using a
distributed task within the same slurm job as the spectral
imaging. This produces a file listing a series of statistics for each
channel, as well as a plot of the statistics. See :doc:`validation`
for examples. These statistics are then used to identify problematic
channels (for example, due to divergence in the imaging caused by RFI)
that are then masked. Blank channels (those not imaged, e.g. due to
barycentric correction or missing data) are also masked. The
statistics file is then regenerated.

A final task can perform image-based continuum subtraction.  The
default tool is the :doc:`../calim/imcontsub` tool. This fits and
subtracts a low-order polynomial to each spectrum in the cube
separately. It is able to fit independently in blocks of channels,
which allows it to take into account potential discontinuities at the
edges of beam-forming intervals.

Alternatively, there are two python tools available for this step,
selected by setting ``IMCONTSUB_USE_PYTHON=true``, and choosing the
script via the ``SPECTRAL_IMSUB_SCRIPT`` parameter. These were
originally developed as demonstration tools, which led to the
development of the *imcontsub* tool. The first option uses the
*robust_contsub_mpi.py* script in the legacy/ directory, which has a
similar behaviour to the *imcontsub* tool. The second uses the
*contsub_im.py* script which uses a Savitzky-Golay filter to find and
remove the spectral baseline, again in each spectrum of the cube
separately. The former script is MPI-enabled, allowing it to run on
multiple cores (defined by ``NUM_CORES_IMCONTSUB``). The latter is
still a serial task, so will take considerably longer.

The image-based continuum subtraction can take into account the
potential discontinuities at the edges of beam-forming intervals. This
is assumes the intervals are a constant size (given by
``SPECTRAL_IMSUB_BLOCKSIZE``), and with some offset relative to the
start of the spectrum (given by ``SPECTRAL_IMSUB_SHIFT``, where it
takes the value of the number of channels from the start of the first
interval to the start of the spectrum). By default, these parameters
are derived from the edge frequencies recorded in the SB obs
variables, but this may be over-ridden by specifying one or both of
these parameters (if only one is given, the other defaults to
zero). Providing these parameters will be necessary for SBs older
than 26120. Note also that **contsub_im.py** does not provide this
functionality.

The variables presented below work in the same manner as those for the
continuum imaging, albeit with names that clearly refer to the
spectral-imaging.

Whether or not the spectral processing is done is governed by the
``DO_SPECTRAL_PROCESSING`` parameter - the default approach of the
pipeline is to not do any of the processing above, but if this
parameter is set to ``true`` then it falls to the individual switches
for each task. Each of these default to true, so if
``DO_SPECTRAL_PROCESSING`` is turned on then everything will be done.

A note on the imagers and the output formats. The default approach is
to use the new imager **imager** (:doc:`../calim/imager`) to produce
the spectral-line cubes. The legacy spectral imager application
**simager** can be used by setting ``DO_ALT_IMAGER_SPECTRAL`` or
``DO_ALT_IMAGER`` to false. The latter is the switch controlling all
types of imaging, but can be overridden by the former, if provided.

The default output format is CASA images, although FITS files can be
written directly by setting ``IMAGETYPE_SPECTRAL`` to ``fits`` (rather
than ``casa``). This will only work with the new imager, as
**simager** does not have this functionality. This mode is still
in development, so may not be completely reliable. The recommended
method for getting images into FITS format is still to use the
``DO_CONVERT_TO_FITS`` flag, which makes use of the
:doc:`../calim/imagetofits` application. A single FITS file can be
produced by setting ``ALT_IMAGER_SINGLE_FILE=true``.

Optional Image products
-----------------------

Imager allows users to specify whether or not to write optional output
products such as the residual, weights, natural psf, preconditioned psf etc. images.
These are aimed at minimising disk I/O and occupancy where possible.

Note that information on weights is required by the linear mosaicking applications: linmos
and linmos-mpi. For non A-Projection gridders like WProject, where the weights value
across the image extent is constant, imager can write out the weight value
into an ascii text file that linmos can make use of. This can be specified using
``WRITE_WEIGHTS_LOG_SPECTRAL`` parameter for the spectral line imaging case.

For A-project gridders, or when snapshot imaging is turned on, one will have to
necessarily write out the weights images for use in linmos. An exception will be
thrown by processASKAP.sh if this condition is violated.

The imager can also optionally output the gridded visibility cubes. This can be
requested by setting ``WRITE_UVGRIDS_SPECTRAL=true``.

More details on writing these optional image products are discussed in the sections
below. See also :doc:`ScienceFieldContinuumImaging` and :doc:`ScienceFieldMosaicking`.

In addition, cube statistics and plots thereof (see :doc:`validation`
for examples) will be generated for each cube, and copied to a
directory within the diagnostics directory. This will be avaiable for
download from CASDA within the calibration-metadata-processing-logs
evalution tar file.


Spectral Imaging Parameters
---------------------------

+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| Variable                                        | Default                             | Parset equivalent                       | Description                                                       |
+=================================================+=====================================+=========================================+===================================================================+
| ``DO_SPECTRAL_PROCESSING``                      | false                               | none                                    | Whether to do the spectral-line processing.                       |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``JOB_TIME_SPECTRAL_IMAGE``                     | ``JOB_TIME_DEFAULT`` (24:00:00)     | none                                    | Time request for imaging the spectral-line data                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``IMAGETYPE_SPECTRAL``                          | fits                                | imagetype (:doc:`../calim/imager`)      | Image format to use - can be either 'casa' or 'fits', although    |
|                                                 |                                     |                                         | 'fits' can only be given in conjunction with                      |
|                                                 |                                     |                                         | ``DO_ALT_IMAGER_SPECTRAL=true``.                                  |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| **Preparation of spectral dataset**             |                                     |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DO_COPY_SL``                                  | false                               | none                                    | Whether to copy a channel range of the original                   |
|                                                 |                                     |                                         | full-spectral-resolution measurement set into a new MS. If        |
|                                                 |                                     |                                         | the original MS is original.ms, this will create original_SL.ms.  |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DO_AVERAGE_SL``                               | false                               | none                                    | Whether to average channels of the spectral dataset when creating |
|                                                 |                                     |                                         | the spectral ("_SL.ms") measurement set                           |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``NUM_CHAN_SL_AVERAGE``                         | 2                                   | width (:doc:`../calim/mssplit`)         | How many channels to average together when ``DO_AVERAGE_SL=true``.|
|                                                 |                                     |                                         | If this doesn't evenly divide the number of channels, and error is|
|                                                 |                                     |                                         | raised and the pipeline exits prior to submitting any jobs.       |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``JOB_TIME_SPECTRAL_SPLIT``                     | ``JOB_TIME_DEFAULT`` (24:00:00)     | none                                    | Time request for splitting out a subset of the spectral data      |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CHAN_RANGE_SL_SCIENCE``                       | "1-``NUM_CHAN_SCIENCE``"            | channel (:doc:`../calim/mssplit`)       | The range of channels to copy from the original dataset (1-based).|
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``TILENCHAN_SL``                                | 1                                   | stman.tilenchan                         | The number of channels in the tile size used for the new MS. The  |
|                                                 |                                     | (:doc:`../calim/mssplit`)               | tile size defines the minimum amount read at a time.              |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DO_APPLY_CAL_SL``                             | true                                | none                                    | Whether to apply the gains calibration determined from the        |
|                                                 |                                     |                                         | continuum self-calibration (see ``GAINS_CAL_TABLE`` in            |
|                                                 |                                     |                                         | :doc:`ScienceFieldContinuumImaging`).                             |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``JOB_TIME_SPECTRAL_APPLYCAL``                  | ``JOB_TIME_DEFAULT`` (24:00:00)     | none                                    | Time request for applying the gains calibration to the spectral   |
|                                                 |                                     |                                         | data                                                              |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DO_CONT_SUB_SL``                              | true                                | none                                    | Whether to subtract a continuum model from the spectral-line      |
|                                                 |                                     |                                         | dataset. If true, the clean model from the continuum imaging will |
|                                                 |                                     |                                         | be used to represent the continuum, and this will be subtracted   |
|                                                 |                                     |                                         | from the spectral-line dataset (either the original               |
|                                                 |                                     |                                         | full-spectral-resolution one, or the reduced-channel-range copy), |
|                                                 |                                     |                                         | which gets overwritten.                                           |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``JOB_TIME_SPECTRAL_CONTSUB``                   | ``JOB_TIME_DEFAULT`` (24:00:00)     | none                                    | Time request for subtracting the continuum from the spectral data |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| **Continuum subtraction**                       |                                     |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_NTASKS``                              | 32 (1 if                            | none                                    | Number of cores used for the continuum subtraction job            |
|                                                 | ``USE_PARALLEL_WRITE_MS=false``)    |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_NPPN``                                | 32 (1 if                            | none                                    | Number of cores per node used for the continuum subtraction job   |
|                                                 | ``USE_PARALLEL_WRITE_MS=false``)    |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_METHOD``                              | CleanModel                          | none                                    | This defines which method is used to determine the continuum that |
|                                                 |                                     |                                         | is to be subtracted. It can take one of four values: **Cmodel**,  |
|                                                 |                                     |                                         | which uses a model image constructed by Cmodel                    |
|                                                 |                                     |                                         | (:doc:`../calim/cmodel`) from a continuum components catalogue    |
|                                                 |                                     |                                         | generated by Selavy (:doc:`../analysis/selavy`); **Components**,  |
|                                                 |                                     |                                         | which uses the Selavy catalogue directly by in the form of        |
|                                                 |                                     |                                         | components; **CleanModel**, in which case the clean model from    |
|                                                 |                                     |                                         | the continuum imaging will be used; or **remUVcont**, that models |
|                                                 |                                     |                                         | each visibility spectrum using basis functions comprising of a    |
|                                                 |                                     |                                         | combination of polynomials (equivalent to miriad UVLIN) and       |
|                                                 |                                     |                                         | harmonics to remove continuum as well instrumental artifacts.     |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CMODEL_NEAREST_CONTSUB``                      | true                                | Cmodel.nearest (:doc:`../calim/cmodel`) | For ``CONTSUB_METHOD=Cmodel`` whether to use nearest neighbour    |
|                                                 |                                     |                                         | interpolation for point sources. If set to false, Lanczos5        |
|                                                 |                                     |                                         | interpolation will be used.                                       |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_UVLIN``                               | false                               | CContSubtract.doUVlin                   | Whether to subtract a fitted continuum model. If subtraction of   |
|                                                 |                                     |                                         | an image or component based model is also requested, the fitting  |
|                                                 |                                     |                                         | will be done on the residual visibilities.                        |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_UVLIN_POLY_ORDER``                    | 1                                   | CContSubtract.uvlin.order               | Order of the polynomial function used in fitting                  |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_UVLIN_HARM_ORDER``                    | 0                                   | CContSubtract.uvlin.harmonic            | Order of the harmonic function used in fitting. Each order adds   |
|                                                 |                                     |                                         | an additional sine and cosine term                                |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_UVLIN_WIDTH``                         | 0                                   |CContSubtract.uvlin.width                | The data will be divided into bins of 'width' channels that are   |
|                                                 |                                     |                                         | then fitted independently.                                        |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_UVLIN_OFFSET``                        | 0                                   |CContSubtract.uvlin.offset               | When fitting in bins, shift the bins left by 'offset' channels to |
|                                                 |                                     |                                         | match beam forming intervals                                      |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_UVLIN_THRESHOLD``                     | 2.5                                 |CContSubtract.uvlin.threshold            | Exclude outliers from the continuum fit. This first determines a  |
|                                                 |                                     |                                         | robust estimate of rms and then rejects channels more than        |
|                                                 |                                     |                                         | threshold*rms from the model. Set to zero to skip thresholding.   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_UVLIN_DIRECTION``                     | None                                |CContSubtract.uvlin.direction            | Specify direction to rotate visibilities to before doing the fit  |
|                                                 |                                     |                                         | to each spectrum. After the fit/subtract the visibilities are     |
|                                                 |                                     |                                         | rotated back. A general Direction Measure is supported,           |
|                                                 |                                     |                                         | e.g., [12h34m56.7,-23.34.45.6,J2000] or SUN                       |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``REMUVCONT_POLY_ORDER``                        | ""                                  | none                                    | Order of the polynomial function used in fitting                  |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``REMUVCONT_HARM_ORDER``                        | ""                                  | none                                    | Order of the harmonic function used in fitting                    |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``REMUVCONT_N_WIN``                             | ""                                  | none                                    | The data will be divided into nWin windows and a moving fit is    |
|                                                 |                                     |                                         | done to derive a smooth model                                     |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``REMUVCONT_N_TAPER``                           | ""                                  | none                                    | Gaussian Taper width in number of channels to be used in an       |
|                                                 |                                     |                                         | intermediate step where the spectrum is passed through a low pass |
|                                                 |                                     |                                         | filter to interpolate across flagged channels. The good data are  |
|                                                 |                                     |                                         | not altered. This helps make the fitting robust.                  |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``REMUVCONT_N_ITER``                            | ""                                  | none                                    | The intermediate los pass filter is an iterative method based on  |
|                                                 |                                     |                                         | FFT. This parameter specifies the number of iterations. Usually   |
|                                                 |                                     |                                         | a few tens of iterations suffices. To avoid the low-pass filter   |
|                                                 |                                     |                                         | step, use 0                                                       |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``REMUVCONT_F54``                               | ""                                  | none                                    | Set this to an integer value  N such that                         |
|                                                 |                                     |                                         | ``54 x N = beam-forming width`` in units of number of channels.   |
|                                                 |                                     |                                         | If this parameter is set, the fitting will be done only within    |
|                                                 |                                     |                                         | beam-forming intervals. This is necessary to be robust against    |
|                                                 |                                     |                                         | any discontinuities at the beam-forming and/or ODC update         |
|                                                 |                                     |                                         | intervals.                                                        |
|                                                 |                                     |                                         | Note1: When this option is  turned ON ``REMUVCONT_N_WIN``         |
|                                                 |                                     |                                         | is computed internally.                                           |
|                                                 |                                     |                                         | Note2: For small windows, choose ``REMUVCONT_POLY_ORDER`` and     |
|                                                 |                                     |                                         | ``REMUVCONT_HARM_ORDER`` carefully. Use low orders.               |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``GRIDDER_CONTSUB``                             | ""                                  | gridder (:doc:`../calim/gridder`)       | Specify griddder to use: WProject or MPIWProject. Defaults to the |
|                                                 |                                     |                                         | value of ``GRIDDER`` (ie. the continuum imaging one). MPIWProject |
|                                                 |                                     |                                         | can save memory and time when using multiple ranks                |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``GRIDDER_CONTSUB_CFRANK``                      | 16                                  | MPIWProject.cfrank                      | The number of ranks used to distribute the convolution functions  |
|                                                 |                                     | (:doc:`../calim/gridder`)               | in the gridding for the continuum subtraction. Only used when     |
|                                                 |                                     |                                         | ``GRIDDER_CONTSUB=MPIWProject``.                                  |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_SELAVY_NSUBX``                        | 6                                   | nsubx (:doc:`../analysis/selavy`)       | Division of image in x-direction for source-finding               |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_SELAVY_NSUBY``                        | 3                                   | nsuby (:doc:`../analysis/selavy`)       | Division of image in y-direction for source-finding               |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_SELAVY_THRESHOLD``                    | 6                                   | snrCut (:doc:`../analysis/selavy`)      | SNR threshold for detection with Selavy in determining components |
|                                                 |                                     |                                         | to go into the continuum model.                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_MODEL_FLUX_LIMIT``                    | 10uJy                               | flux_limit (:doc:`../calim/cmodel`)     | Flux limit applied to component catalogue - only components       |
|                                                 |                                     |                                         | brighter than this will be included in the model image. Parameter |
|                                                 |                                     |                                         | takes the form of a number+units string.                          |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_SELAVY_FLAG_ADJACENT``                | true                                | flagAdjacent                            | Whether to enforce pixels in islands to be contiguous.            |
|                                                 |                                     | (:doc:`../analysis/selavy`)             |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_SELAVY_SPATIAL_THRESHOLD``            | 5                                   | threshSpatial                           | If ``CONTSUB_SELAVY_FLAG_ADJACENT=false``, this is the threshold  |
|                                                 |                                     | (:doc:`../analysis/selavy`)             | in pixels within which islands are joined.                        |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_SELAVY_SPECTRAL_INDEX_THRESHOLD``     | ""                                  | spectralTerms.threshold                 | Threshold applied to component peak fluxes in determining         |
|                                                 |                                     | (:doc:`../analysis/postprocessing`)     | which have a spectral index (and curvature) value reported        |
|                                                 |                                     |                                         | in the component catalogue. Not used if left blank. Takes         |
|                                                 |                                     |                                         | precedence over ``CONTSUB_SELAVY_SPECTRAL_INDEX_THRESHOLD_SNR``.  |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CONTSUB_SELAVY_SPECTRAL_INDEX_THRESHOLD_SNR`` | 50.                                 | spectralTerms.thresholdSNR              | Threshold applied to component peak signal-to-noise values        |
|                                                 |                                     | (:doc:`../analysis/postprocessing`)     | in determining which have a spectral index (and curvature)        |
|                                                 |                                     |                                         | value reported in the component catalogue. Not used if left       |
|                                                 |                                     |                                         | blank.                                                            |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| **Basic variables for imaging**                 |                                     |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DO_SPECTRAL_IMAGING``                         | true                                | none                                    | Whether to do the spectral imaging                                |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``NUM_CORES_SPECIMG_SCI``                       | 200 (galaxy), 433                   | none                                    | The total number of cores allocated to the spectral-imaging       |
|                                                 | (petrichor/setonix)                 |                                         | job. One will be the master, while the rest will be devoted to    |
|                                                 |                                     |                                         | imaging individual channels. This is not used when                |
|                                                 |                                     |                                         | ``DO_ALT_IMAGER_SPECTRAL=true`` - instead, it is set to ``NCHAN / |
|                                                 |                                     |                                         | NCHAN_PER_CORE_SL + 1``. If ``NCHAN_PER_CORE_SL`` does not evenly |
|                                                 |                                     |                                         | divide into NCHAN, then an error is raised and no jobs are        |
|                                                 |                                     |                                         | submitted.                                                        |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CORES_PER_NODE_SPEC_IMAGING``                 | 20 (galaxy), 64 (petrichor), 109    | none                                    | The number of cores per node to use.                              |
|                                                 | (setonix)                           |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``IMAGE_BASE_SPECTRAL``                         | i.%t.SB%s.cube                      | Helps form Images.name                  | The base name for image cubes: if ``IMAGE_BASE_SPECTRAL=i.blah``  |
|                                                 |                                     | (:doc:`../calim/simager`)               | then we'll get image.i.blah, image.i.blah.restored, psf.i.blah    |
|                                                 |                                     |                                         | etc. The %s wildcard will be resolved into the scheduling block   |
|                                                 |                                     |                                         | ID, and the %t will be resolved into the "target", or scheduling  |
|                                                 |                                     |                                         | block alias.                                                      |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DIRECTION_SCI``                               | none                                | Images.direction                        | The direction parameter for the image cubes, i.e. the central     |
|                                                 |                                     | (:doc:`../calim/simager`)               | position. Can be left out, in which case it will be determined    |
|                                                 |                                     |                                         | from the measurement set by mslist. This is the same input        |
|                                                 |                                     |                                         | parameter as that used for the continuum imaging.                 |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``NUM_PIXELS_SPECTRAL``                         | 1024                                | Images.shape                            | The number of spatial pixels along the side for the image cubes.  |
|                                                 |                                     | (:doc:`../calim/simager`)               | Needs to be specified (unlike the continuum imaging case).        |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CELLSIZE_SPECTRAL``                           | 8                                   | Images.cellsize                         | The spatial pixel size for the image cubes. Must be specified.    |
|                                                 |                                     | (:doc:`../calim/simager`)               |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``REST_FREQUENCY_SPECTRAL``                     | HI                                  | Images.restFrequency                    | The rest frequency for the cube. Can be a quantity string (eg.    |
|                                                 |                                     | (:doc:`../calim/simager`)               | 1234.567MHz), or the special string 'HI' (which is 1420.405751786 |
|                                                 |                                     |                                         | MHz). If blank, no rest frequency will be written to the cube.    |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_IMAGE_MAXUV``                        | 2000                                | MaxUV                                   | A maximum UV distance (in metres) to apply in the data selection  |
|                                                 |                                     | (:doc:`../calim/data_selection`)        | step. Only used if a positive value is applied.                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_IMAGE_MINUV``                        | 0                                   | MinUV                                   | A minimum UV distance (in metres) to apply in the data selection  |
|                                                 |                                     | (:doc:`../calim/data_selection`)        | step. Only used if a positive value is applied.                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DO_MASK_BAD_CHANNELS``                        | true                                | none                                    | Whether to mask out bad or blank channels from the spectral cube. |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``MASK_CHANS_USE_SIG``                          | false                               | none                                    | Whether to use the "signifcance", or the ratio of the             |
|                                                 |                                     |                                         | 1-percent statistic to the MADFM, to determine the bad            |
|                                                 |                                     |                                         | channels.                                                         |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``MASK_CHANS_SIG_THRESHOLD``                    | 10.                                 | none                                    | The significance level at which to reject channels.               |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``MASK_CHANS_USE_NOISE``                        | true                                | none                                    | Whether to mask out bad channels on the basis of the MADFM        |
|                                                 |                                     |                                         | value alone.                                                      |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``MASK_CHANS_NOISE_THRESHOLD``                  | 1000.                               | none                                    | The value of MADFM (in mJy/beam), above which a channel is        |
|                                                 |                                     |                                         | deemed bad.                                                       |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``MASK_CHANS_BLANK``                            | true                                | none                                    | Whether to mask out blank channels from the continuum             |
|                                                 |                                     |                                         | cube.                                                             |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| **Gridding**                                    |                                     |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``GRIDDER_SPECTRAL``                            | MPIWProject                         | gridder                                 | Specify griddder to use: WProject or MPIWProject.                 |
|                                                 |                                     | (:doc:`../calim/gridder`)               | MPIWProject can save memory and time when using multiple ranks    |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DO_NYQUIST_GRIDDING_SPECTRAL``                | false                               | Images.nyquistgridding                  | Whether to turn on Nyquist gridding.                              |
|                                                 |                                     | (:doc:`../calim/imager`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``GRIDDER_SPECTRAL_SNAPSHOT_IMAGING``           | false                               | snapshotimaging                         | Whether to use snapshot imaging when gridding.                    |
|                                                 |                                     | (:doc:`../calim/gridder`)               |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``GRIDDER_SPECTRAL_SNAPSHOT_WTOL``              | 2600                                | snapshotimaging.wtolerance              | The wtolerance parameter controlling how frequently to snapshot.  |
|                                                 |                                     | (:doc:`../calim/gridder`)               |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``GRIDDER_SPECTRAL_SNAPSHOT_LONGTRACK``         | true                                | snapshotimaging.longtrack               | The longtrack parameter controlling how the best-fit W plane is   |
|                                                 |                                     | (:doc:`../calim/gridder`)               | determined when using snapshots.                                  |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``GRIDDER_SPECTRAL_SNAPSHOT_CLIPPING``          | 0.01                                | snapshotimaging.clipping                | If greater than zero, this fraction of the full image width       |
|                                                 |                                     | (:doc:`../calim/gridder`)               | is set to zero. Useful when imaging at high declination as        |
|                                                 |                                     |                                         | the edges can generate artefacts.                                 |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``GRIDDER_SPECTRAL_WMAX``                       | 2600                                | MPIWProject.wmax                        | The wmax parameter for the gridder. The default for this depends  |
|                                                 | (``GRIDDER_SNAPSHOT_IMAGING=true``) | (:doc:`../calim/gridder`)               | on whether snapshot imaging is invoked or not                     |
|                                                 | or 35000                            |                                         | (``GRIDDER_SNAPSHOT_IMAGING``).                                   |
|                                                 | (``GRIDDER_SNAPSHOT_IMAGING=false``)|                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``GRIDDER_WMAXCLIP``                            | false                               | MPIWProject.wmaxclip                    | The wmaxclip parameter, used for all gridders. By default, a      |
|                                                 |                                     | (:doc:`../calim/gridder`)               | hard cut for w planes on [-wmax,wmax] is made and values          |
|                                                 |                                     |                                         | outside this range will trigger an error. When true, w values     |
|                                                 |                                     |                                         | outside this range are ignored.                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``GRIDDER_SPECTRAL_NWPLANES``                   | 557                                 | MPIWProject.nwplanes                    | The nwplanes parameter for the gridder.                           |
|                                                 |                                     | (:doc:`../calim/gridder`)               |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``GRIDDER_SPECTRAL_OVERSAMPLE``                 | 8                                   | MPIWProject.oversample                  | The oversampling factor for the gridder.                          |
|                                                 |                                     | (:doc:`../calim/gridder`)               |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``GRIDDER_SPECTRAL_MAXSUPPORT``                 | 512                                 | MPIWProject.maxsupport                  | The maxsupport parameter for the gridder. The default for this    |
|                                                 | (``GRIDDER_SNAPSHOT_IMAGING=true``) | (:doc:`../calim/gridder`)               | depends on whether snapshot imaging is invoked or not             |
|                                                 | or 2048                             |                                         | (``GRIDDER_SNAPSHOT_IMAGING``).                                   |
|                                                 | (``GRIDDER_SNAPSHOT_IMAGING=false``)|                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``GRIDDER_SPECTRAL_SHARECF``                    | true                                | MPIWProject.sharecf                     | Whether to use a (static) cache for the convolution functions     |
|                                                 |                                     | (:doc:`../calim/gridder`)               | in the gridder.                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``GRIDDER_SPECTRAL_CFRANK``                     | 16                                  | MPIWProject.cfrank                      | Number of ranks per node to use to speed up convolution function  |
|                                                 |                                     | (:doc:`../calim/gridder`)               | calculation. Useful range: ~4 to #ranks/node.                     |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| **Cleaning**                                    |                                     |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SOLVER_SPECTRAL``                             | Clean                               | solver                                  | Which solver to use. You will mostly want to leave this as        |
|                                                 |                                     | (:doc:`../calim/solver`)                | 'Clean', but there is a 'Dirty' solver available.                 |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CLEAN_SPECTRAL_ALGORITHM``                    | BasisfunctionMFS                    | Clean.algorithm                         | The name of the clean algorithm to use.                           |
|                                                 |                                     | (:doc:`../calim/solver`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DO_MFS_STARTING_MODEL_SPECTRAL``              | false                               | none                                    | Whether to use the MFS model image as the starting point for      |
|                                                 |                                     |                                         | the cleaning of the spectral cube.                                |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CLEAN_SPECTRAL_MINORCYCLE_NITER``             | 1600                                | Clean.niter                             | The number of iterations for the minor cycle clean.               |
|                                                 |                                     | (:doc:`../calim/solver`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CLEAN_SPECTRAL_GAIN``                         | 0.1                                 | Clean.gain                              | The loop gain (fraction of peak subtracted per minor cycle).      |
|                                                 |                                     | (:doc:`../calim/solver`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CLEAN_SPECTRAL_PSFWIDTH``                     | 256                                 | Clean.psfwidth                          | The width of the psf patch used in the minor cycle.               |
|                                                 |                                     | (:doc:`../calim/solver`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CLEAN_SPECTRAL_SCALES``                       | ``"[0,3,10,30]"``                   | Clean.scales                            | Set of scales (in pixels) to use with the multi-scale clean.      |
|                                                 |                                     | (:doc:`../calim/solver`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CLEAN_SPECTRAL_THRESHOLD_MINORCYCLE``         | ``"[45%, 5.0sigma, 0.5sigma]"``     | threshold.minorcycle                    | Threshold for the minor cycle loop.                               |
|                                                 |                                     | (:doc:`../calim/solver`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CLEAN_SPECTRAL_THRESHOLD_MAJORCYCLE``         | 0.5mJy                              | threshold.majorcycle                    | The target peak residual. Major cycles stop if this is reached. A |
|                                                 |                                     | (:doc:`../calim/solver`)                | negative number ensures all major cycles requested are done.      |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CLEAN_SPECTRAL_NUM_MAJORCYCLES``              | 3                                   | ncycles                                 | Number of major cycles.                                           |
|                                                 |                                     | (:doc:`../calim/solver`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CLEAN_WRITE_AT_MAJOR_CYCLE``                  | false                               | Images.writeAtMajorCycle                | If true, the intermediate images will be written (with a .cycle   |
|                                                 |                                     | (:doc:`../calim/simager`)               | suffix) after the end of each major cycle.                        |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CLEAN_SPECTRAL_SOLUTIONTYPE``                 | MAXBASE                             | Clean.solutiontype (see discussion      | The type of peak finding algorithm to use in the                  |
|                                                 |                                     | at :doc:`../recipes/Imaging`)           | deconvolution. Choices are MAXCHISQ, MAXTERM0, or MAXBASE.        |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
|  ``CLEAN_SPECTRAL_DETECT_DIVERGENCE``           | true                                | Clean.detectdivergence                  | Whether to detect that the deconvolution is starting to           |
|                                                 |                                     | (:doc:`../calim/solver`)                | diverge (in which case it is stopped).                            |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CLEAN_SPECTRAL_DETECT_MILD_DIVERGENCE``       | true                                | Clean.detectmilddivergence              | Whether to use the mild form of divergence detection, that        |
|                                                 |                                     |  (:doc:`../calim/solver`)               | should skip to the next major cycle if residuals increase by      |
|                                                 |                                     |                                         | 10% or more.                                                      |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CLEAN_SPECTRAL_NOISEBOXSIZE``                 | 0                                   | Clean.noiseboxsize                      | Size of the box used to determine the local noise value for       |
|                                                 |                                     | (:doc:`../calim/solver`)                | setting thresholds (when a "sigma" threshold is given). A         |
|                                                 |                                     |                                         | value of 0 means the entire image is used.                        |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| **Preconditioning**                             |                                     |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``PRECONDITIONER_LIST_SPECTRAL``                | ``"[Wiener, GaussianTaper]"``       | preconditioner.Names                    | List of preconditioners to apply.                                 |
|                                                 |                                     | (:doc:`../calim/solver`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``PRECONDITIONER_SPECTRAL_GAUSS_TAPER``         | ``"[20arcsec, 20arcsec, 0deg]"``    | preconditioner.GaussianTaper            | Size of the Gaussian taper - either single value (for circular    |
|                                                 |                                     | (:doc:`../calim/solver`)                | taper) or 3 values giving an elliptical size.                     |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``PRECONDITIONER_SPECTRAL_GAUSS_TAPER_IS_PSF``  | ``"false"``                         | preconditioner.GaussianTaper.isPsfSize  | Decide if rather than applying the specified taper, the taper     |
|                                                 |                                     | (:doc:`../calim/solver`)                | should be adjusted to ensure that the output image planes have    |
|                                                 |                                     |                                         | the specified resolution.                                         |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``PRECONDITIONER_SPECTRAL_GAUSS_TAPER_TOL``     | 0.005                               | preconditioner.GaussianTaper.tolerance  | Fractional tolerance for the fitted beam size when                |
|                                                 |                                     | (:doc:`../calim/solver`)                | ``PRECONDITIONER_SPECTRAL_GAUSS_TAPER_IS_PSF = true``             |
|                                                 |                                     |                                         | The default is set to 0.5%                                        |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
|  ``PRECONDITIONER_SPECTRAL_WIENER_ROBUSTNESS``  | 0.5                                 | preconditioner.Wiener.robustness        | Robustness value for the Wiener filter.                           |
|                                                 |                                     | (:doc:`../calim/solver`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``PRECONDITIONER_SPECTRAL_WIENER_TAPER``        | ``""``                              | preconditioner.Wiener.taper             | Size of gaussian taper applied in image domain to Wiener filter.  |
|                                                 |                                     | (:doc:`../calim/solver`)                | Ignored if blank (ie. ``""``).                                    |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``TRADITIONAL_WEIGHT_PARAM_SPECTRAL_LIST``      | ``""``                              | uvweight (:doc:`../calim/imager`)       | List of parameters for application of traditional weights.        |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``TRADITIONAL_WEIGHT_SPECTRAL_ROBUSTNESS``      | -0.5                                | uvweight.robustness                     | The value of robustness. Only used if there is Robust among       |
|                                                 |                                     |  (:doc:`../calim/imager`)               | the list of parameters given by                                   |
|                                                 |                                     |                                         | ``TRADITIONAL_WEIGHT_PARAM_SPECTRAL_LIST``.                       |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| **Restoring**                                   |                                     |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``RESTORE_SPECTRAL``                            | true                                | restore                                 | Whether to restore the image cubes.                               |
|                                                 |                                     | (:doc:`../calim/simager`)               |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``RESTORING_BEAM_SPECTRAL``                     | fit                                 | restore.beam                            | Restoring beam to use: 'fit' will fit the PSF in each channel     |
|                                                 |                                     | (:doc:`../calim/simager`)               | separately to determine the appropriate beam for that channel,    |
|                                                 |                                     |                                         | else give a size (such as ``“[30arcsec, 30arcsec, 0deg]”``).      |
|                                                 |                                     |                                         | NB: If ``PRECONDITIONER_SPECTRAL_GAUSS_TAPER_IS_PSF=true``        |
|                                                 |                                     |                                         | the restoring beam will be derived such that the output image     |
|                                                 |                                     |                                         | planes have the fixed specified resolution.                       |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``RESTORING_BEAM_CUTOFF_SPECTRAL``              | 0.5                                 | restore.beam.cutoff                     | Cutoff value used in determining the support for the fitting      |
|                                                 |                                     | (:doc:`../calim/simager`)               | (ie. the rectangular area given to the fitting routine). Value is |
|                                                 |                                     |                                         | a fraction of the peak.                                           |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``RESTORING_BEAM_REFERENCE``                    | mid                                 | restore.beamReference                   | Which channel to use as the reference when writing the restoring  |
|                                                 |                                     | (:doc:`../calim/simager`)               | beam to the image cube. Can be an integer as the channel number   |
|                                                 |                                     |                                         | (0-based), or one of 'mid' (the middle channel), 'first' or 'last'|
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| **New imager parameters**                       |                                     |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DO_ALT_IMAGER_SPECTRAL``                      | ``""``                              | none                                    | If true, the spectral-line imaging is done by imager              |
|                                                 |                                     |                                         | (doc:`../calim/imager`). If false, it is done by simager          |
|                                                 |                                     |                                         | (:doc:`../calim/simager`). When true, the following parameters are|
|                                                 |                                     |                                         | used. If left blank (the default), the value is given by the      |
|                                                 |                                     |                                         | overall parameter ``DO_ALT_IMAGER`` (see                          |
|                                                 |                                     |                                         | :doc:`ControlParameters`).                                        |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``NCHAN_PER_CORE_SL``                           | 64 (galaxy), 36 (petrichor/setonix) | nchanpercore                            | The number of channels each core will process.                    |
|                                                 |                                     | (:doc:`../calim/imager`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``NUM_SPECTRAL_WRITERS``                        | ``""``                              | nwriters (:doc:`../calim/imager`)       | The number of writers used by imager. Unless                      |
|                                                 |                                     |                                         | ``ALT_IMAGER_SINGLE_FILE=true``, this will equate to the number of|
|                                                 |                                     |                                         | distinct spectral cubes produced. In the case of multiple cubes,  |
|                                                 |                                     |                                         | each will be a sub-band of the full bandwidth. No combination of  |
|                                                 |                                     |                                         | the sub-cubes is currently done. The number of writers will be    |
|                                                 |                                     |                                         | reduced to the number of workers in the job if necessary.         |
|                                                 |                                     |                                         | If left blank, the number of writers will be the number of worker |
|                                                 |                                     |                                         | nodes in the imaging job.                                         |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``ALT_IMAGER_SINGLE_FILE``                      | true                                | singleoutputfile                        | Whether to write a single cube, even with multiple writers (ie.   |
|                                                 |                                     | (:doc:`../calim/imager`)                | ``NUM_SPECTRAL_WRITERS>1``). Only works when                      |
|                                                 |                                     |                                         | ``IMAGETYPE_SPECTRAL=fits``                                       |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``FREQ_FRAME_SL``                               | bary                                | freqframe (:doc:`../calim/imager`)      | The reference frame in which to write the spectral cube - one of  |
|                                                 |                                     |                                         | topo, bary, lsrk. Anything else (or an unset value) default to    |
|                                                 |                                     |                                         | bary. The frame conversion is done assuming a fixed direction     |
|                                                 |                                     |                                         | (using the dopplertracking.direction parameter on                 |
|                                                 |                                     |                                         | :doc:`../calim/imager`). This direection is governed by the       |
|                                                 |                                     |                                         | ``DOPPLER_TRACKING_DIRECTION`` parameter.                         |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DOPPLER_TRACKING_DIRECTION``                  | beam                                | dopplertracking.direction               | The specific direction to use for doppler tracking (when          |
|                                                 |                                     | (:doc:`../calim/imager`)                | ``FREQ_FRAME_SL`` is bary or lsrk. Can be "beam", "target", or a  |
|                                                 |                                     |                                         | direction string "[ra,dec,equinox]".                              |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DOPPLER_CORRECTION_IN_IMAGER``                | true                                | dopplertracking (:doc:`../calim/imager`)| If true, the doppler correction is done by imager, else it is done|
|                                                 |                                     | dopplercorrection                       | by mssplit in the preparation of the spectral MS.                 |
|                                                 |                                     | (:doc:`../calim/mssplit`)               |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DOPPLER_CORRECTION_INTERPOLATION``            | cubic                               | interpolation (:doc:`../calim/mssplit`) | Form of interpolation to use in the Doppler correction when the   |
|                                                 |                                     |                                         | mssplit option is used. Does not apply if                         |
|                                                 |                                     |                                         | ``DOPPLER_CORRECTION_IN_IMAGER=true``.                            |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``OVERRIDE_CONTSUB_MSSPLIT_DOPPLER_CHECK``      | false                               | none                                    | If true, the usual check to ensure we don't combine mssplit       |
|                                                 |                                     |                                         | doppler correction and UV continuum subtraction is not applied.   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``OUTPUT_CHANNELS_SL``                          | ``""``                              | Frequencies                             | The output channels for the spectral cube. Should be of the form  |
|                                                 |                                     | (:doc:`../calim/imager`)                | [number,start,width], with the start and width parameters are in  |
|                                                 |                                     |                                         | Hz. If not given, the behaviour is to use the same frequency      |
|                                                 |                                     |                                         | values as the input MS, albeit in the requested frequency frame.  |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| **Optional outputs**                            |                                     |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``WRITE_RESIDUAL_SPECTRAL``                     | true                                | write.residualimage                     | Whether to write the residual image cube                          |
|                                                 |                                     | (:doc:`../calim/imager`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``WRITE_PSF_RAW_SPECTRAL``                      | false                               | write.psfrawimage                       | Whether to write the naturally weighted psf image cube            |
|                                                 |                                     | (:doc:`../calim/imager`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``WRITE_PSF_IMAGE_SPECTRAL``                    | false                               | write.psfimage                          | Whether to write the preconditioned psf image cube                |
|                                                 |                                     | (:doc:`../calim/imager`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``WRITE_WEIGHTS_IMAGE_SPECTRAL``                | false                               | write.weightsimage                      | Whether to write the weights image cube                           |
|                                                 |                                     | (:doc:`../calim/imager`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``WRITE_WEIGHTS_LOG_SPECTRAL``                  | true                                | write.weightslog                        | Option to write the weight spectra into an ascii text file        |
|                                                 |                                     | (:doc:`../calim/imager`)                |                                                                   |
|                                                 |                                     |                                         | Note 1: Should not be used with A-project gridders.               |
|                                                 |                                     |                                         |                                                                   |
|                                                 |                                     |                                         | Note 2: For snapshot imaging, this option is internally forced to |
|                                                 |                                     |                                         | "false" by the pipeline scripts. Set                              |
|                                                 |                                     |                                         | ``WRITE_WEIGHTS_IMAGE_CONT=true`` in this case.                   |
|                                                 |                                     |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``WRITE_MODEL_IMAGE_SPECTRAL``                  | true                                | write.modelimage                        | Whether to write the model image cube                             |
|                                                 |                                     | (:doc:`../calim/imager`)                |                                                                   |
|                                                 |                                     |                                         | Note: The pipeline ensures that intermediate model images get     |
|                                                 |                                     |                                         | generated if required by selfcal jobs.                            |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``WRITE_UVGRIDS_SPECTRAL``                      | false                               | write.grids                             | Whether to write the UV-grids (cubes only)                        |
|                                                 |                                     | (:doc:`../calim/imager`)                |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``UVGRID_EXPORT_WHEN_IMAGING``                  | false                               | none                                    | Whether to write the UV-grids from within the spectral imaging    |
|                                                 |                                     |                                         | job. If false (the default), it will write them (when             |
|                                                 |                                     |                                         | ``WRITE_UVGRIDS_SPECTRAL=true``) in a separate task, that is      |
|                                                 |                                     |                                         | parameterised by the parameters below. If true, it is done only as|
|                                                 |                                     |                                         | part of the spectral imaging job, and so takes on the same        |
|                                                 |                                     |                                         | parameters as for imaging.                                        |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| **Image-based continuum subtraction**           |                                     |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DO_SPECTRAL_IMSUB``                           | true                                | none                                    | Whether to run an image-based continuum-subtraction task on the   |
|                                                 |                                     |                                         | spectral cube after creation.                                     |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``JOB_TIME_SPECTRAL_IMCONTSUB``                 | ``JOB_TIME_DEFAULT`` (24:00:00)     | none                                    | Time request for image-based continuum subtraction                |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``IMCONTSUB_USE_PYTHON``                        | false                               | none                                    | Whether to use one of the python tools (see next item), instead of|
|                                                 |                                     |                                         | the default tool *imcontsub* (:doc:`../calim/imcontsub`).         |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_IMSUB_IMAGEACCESS``                  | individual (collective when run on  | imcontsub.imageaccess                   | The type of image access method used within the *imcontsub* tool: |
|                                                 | galaxy)                             | (:doc:`../calim/imcontsub`)             | this can be 'collective' or 'individual'.                         |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_IMSUB_SCRIPT``                       | ``"robust_contsub_mpi.py"``         | none                                    | The name of the legacy python script to use for image-based       |
|                                                 |                                     |                                         | continuum subtraction. The only two accepted values are           |
|                                                 |                                     |                                         | "robust_contsub_mpi.py" and "contsub_im.py". Anything else        |
|                                                 |                                     |                                         | reverts to the default.                                           |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``NUM_CORES_IMCONTSUB``                         | 256                                 | none                                    | Number of cores to use for the robust_contsub_mpi.py              |
|                                                 |                                     |                                         | script. Ignored if contsub_im.py is used. Must divide evenly into |
|                                                 |                                     |                                         | the number of spatial pixels in the spectral cube.                |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_IMSUB_VERBOSE``                      | true                                | none                                    | Whether to use verbose output in the logging for the image-based  |
|                                                 |                                     |                                         | continuum subtraction.                                            |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_IMSUB_THRESHOLD``                    | 2.0                                 | imcontsub.threshold                     | Threshold [sigma] to mask outliers prior to fitting the continuum |
|                                                 |                                     | (:doc:`../calim/imcontsub`) or          | baseline.                                                         |
|                                                 |                                     | 'threshold' parameter in                |                                                                   |
|                                                 |                                     | robust_contsub_mpi.py                   |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_IMSUB_FIT_ORDER``                    | 2                                   | imcontsub.order                         | Order of the polynomial to fit to the continuum baseline.         |
|                                                 |                                     | (:doc:`../calim/imcontsub`) or          |                                                                   |
|                                                 |                                     | 'fit_order' parameter in                |                                                                   |
|                                                 |                                     | robust_contsub_mpi.py                   |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_IMSUB_CHAN_SAMPLING``                | 1                                   | none ('n_every' parameter in            | If set to n, we use only every nth channel in the polynomial fit  |
|                                                 |                                     | robust_contsub_mpi.py)                  | (1 uses every channel). Only for "robust_contsub_mpi.py"          |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_IMSUB_BLOCKSIZE``                    | ""                                  | imcontsub.blocksize                     | Do the fitting and subtracting in blocks of n channels. A value of|
|                                                 |                                     | (:doc:`../calim/imcontsub`) or          | 0 will result in all channels being used at once.                 |
|                                                 |                                     | 'blocksize' parameter in                | By default, the value for this will be determined from the weights|
|                                                 |                                     | robust_contsub_mpi.py                   | information in the SB obsvariables.                               |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_IMSUB_SHIFT``                        | ""                                  | imcontsub.shift                         | Shift the edges of the blocks to the left by this many channels.  |
|          .                                      |                                     | (:doc:`../calim/imcontsub`) or 'shift'  | By default, the value for this will be determined from the weights|
|                                                 |                                     | parameter in robust_contub_mpi.py       | information in the SB obsvariables.                               |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_IMSUB_INTERLEAVE``                   | false                               | imcontsub.interleave                    | Interleave the blocks by a half, but only take the middle of each |
|                                                 |                                     | (:doc:`../calim/imcontsub`) or          | interleave. This reduces edge effects                             |
|                                                 |                                     | 'interleave' parameter in               |                                                                   |
|                                                 |                                     | robust_contsub_mpi.py                   |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_IMSUB_LOG_SAMPLING``                 | 1                                   | none ('log_every' parameter in          | How frequently the log messages from "robust_contsub_mpi.py"      |
|                                                 |                                     | robust_contsub_mpi.py)                  | should be written (1 means every channel).                        |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_IMSUB_SG_FILTERWIDTH``               | 200                                 | none ('filterwidth' parameter in        | The half-width of the Savitzky-Golay filter for baseline smoothing|
|                                                 |                                     | contsub_im.py)                          | in the "contsub_im.py" script.                                    |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_IMSUB_SG_BINWIDTH``                  | 4                                   | none ('binwidth' parameter in           | The bin width used for binning the spectrum before continuum      |
|                                                 |                                     | contsub_im.py)                          | subtraction ("contsub_im.py" only).                               |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+



Visibility handling: UV-grid export and MS cutouts & copies
-----------------------------------------------------------

The pipeline allows for the export of the UV grids that would be used
in the spectral imaging, along with their inclusion in the deposit
into CASDA. By default this is done through an additional job that
runs imager - it is a separate job to the spectral imaging, to allow
for different cellsizes or UV ranges to be applied. It can, however,
be done from within the spectral imaging job itself, by setting
``UVGRID_EXPORT_WHEN_IMAGING=true``. If the separate job is used,
there are a set of specific parameters available to fine-tune the
export.

The default approach is to save the complex grids as separate real &
imaginary FITS files (FITS does not allow for complex-valued image
arrays) - on pair each for image types "visgrid", "psfgrid", and
"pcfgrid". These files will be written into a directory with the same
name as the spectral MS, save for replacing the ".ms" extension with
".uvgrid". The FITS headers for the files are extracted to .hdr text
files, and the FITS files are then compressed with gzip (the .hdr
files are just to allow ease of examination without unzipping the
images). The gzip process will compress the files noticeably, due to
the large amount of zero-valued pixels in them. This directory will be
included for deposit into CASDA, where it will be tarred and made
available alongside the measurement sets.

Copies of the measurementsets themselves can be made in a couple of
ways. If a set of known positions & velocities is provided, cutouts of
the relevant beam MSs can be made for archiving into CASDA. These will
be of a predefined width (in channels), and are identified from a
provided catalogue within a certain angular offset from the pointing
centre. The catalogue name can either be a CSV file (contact
Operations for details about format, as this is being developed in
conjunction with WALLABY), or the special word 'HIPASS', which will
query the HIPASS catalogues (the main HICAT, the northern version
NHICAT, and the bright galaxy catalogue BGC).

In addition, the full spectral MSs can be copied to a nominated
location on disk at Pawsey. This can be either the MS imaged to form
the spectral cubes, or the MS prior to continuum subtraction. The
destination needs to be appropriate in terms of permissions, and the
data policies still apply if this option is utilised (ie. anything
published must come from data released on CASDA).



+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| Variable                                        | Default                             | Parset equivalent                       | Description                                                       |
+=================================================+=====================================+=========================================+===================================================================+
| **UV-grid export**                              |                                     |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``NUM_PIXELS_SPECTRAL_UVGRID``                  | 2560                                | Images.shape (`../calim/imager`)        | Size of the image & uv pixel grids.                               |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CELLSIZE_SPECTRAL_UVGRID``                    | 3.5                                 | Images.cellsize (`../calim/imager`)     | Cellsize *in the image plane* (in arcsec) of the images that would|
|                                                 |                                     |                                         | be created. This determines the angular size of the cell          |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_UVGRID_MAXUV``                       | 0                                   | MaxUV (:doc:`../calim/data_selection`)  | Maximum UV to be read from the dataset. If 0, no maximum UV       |
|                                                 |                                     |                                         | distance is applied.                                              |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_UVGRID_MINUV``                       | 0                                   | MinUV (:doc:`../calim/data_selection`)  | Minimum UV to be read from the dataset. If 0, no minimum UV       |
|                                                 |                                     |                                         | distance is applied.                                              |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_UVGRID_USE_UVCOORD``                 | false                               | write.grids.uvcoord                     | Whether the axes in the exported UVgrid FITS file should be       |
|                                                 |                                     | (:doc:`../calim/imager`)                | labelled with the UV coordinates, rather than the image plane     |
|                                                 |                                     |                                         | RA/Dec coordinates.                                               |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``CORES_PER_NODE_SPECTRAL_UVGRID``              | 10 (galaxy), 64 (petrichor), 72     | none                                    | Number of cores per node to be used for the uvgrid export imager  |
|                                                 | (setonix)                           |                                         | job.                                                              |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``NCHAN_PER_CORE_SPECTRAL_UVGRID``              | ""                                  | nchanpercore (:doc:`../calim/imager`)   | Number of channels per rank for the UVgrid export job. If blank   |
|                                                 |                                     |                                         | (the default), it will take the value of ``NCHAN_PER_CORE_SL``.   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| **MS cutouts**                                  |                                     |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``DO_SPECTRAL_MS_CUT``                          | false                               | none                                    | Whether to cut out a spectral range from certain beam MSs         |
|                                                 |                                     |                                         | corresponding to identified sources from a catalogue.             |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``MS_CUTOUT_CATALOGUE``                         | HIPASS                              | none                                    | The catalogue to search for sources from. The can be either       |
|                                                 |                                     |                                         | "HIPASS", which results in a query from static (downloaded)       |
|                                                 |                                     |                                         | catalogues of HICAT, NHICAT and BGC, or the name of a local CSV   |
|                                                 |                                     |                                         | file.                                                             |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``MS_CUTOUT_CATDIR``                            | ``$PIPELINEDIR/data``               | none                                    | Directory in which the HIPASS catalogues are located.             |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``MS_CUTOUT_WIDTH``                             | 6                                   | none                                    | Maximum search radius [deg] from the field centre for extraction  |
|                                                 |                                     |                                         | of sources from the catalogue.                                    |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``MS_CUTOUT_MAXSEP``                            | 1.5                                 | none                                    | Maximum allowed separation [deg] between the source and a beam    |
|                                                 |                                     |                                         | centre.                                                           |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``MS_CUTOUT_NUM_CHAN``                          | 250                                 | none                                    | Number of channels around the source's central velocity to extract|
|                                                 |                                     |                                         | (this is the full width)                                          |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| **MS copying**                                  |                                     |                                         |                                                                   |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``COPY_SPECTRAL_MS``                            | false                               | none                                    | Whether to copy the full spectral MS to a location on disk.       |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_MS_DESTINATION``                     | ""                                  | none                                    | The location on disk to which the spectral MSs should be copied.  |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+
| ``SPECTRAL_MS_TPYE_TO_COPY``                    | pre                                 | none                                    | Which type of MS to copy. Possible values are ``pre``, meaning the|
|                                                 |                                     |                                         | pre-continuum-subtracted data, or ``post``, meaning the data after|
|                                                 |                                     |                                         | continuum-subtraction (ie the MS that is imaged).                 |
+-------------------------------------------------+-------------------------------------+-----------------------------------------+-------------------------------------------------------------------+


Spectral polarisation imaging & leakage calibration
---------------------------------------------------

This table shows the parameters relevant to the jobs that apply the
leakages to the full-spectral-resolution measurement sets (see also
:doc:`ScienceFieldContinuumImaging` for the continuum-resolution
equivalent). The leakages are applied after the application of the
gains derived from self-calibration. Parameters for the creation of
the leakage tables can be found at :doc:`BandpassCalibrator`.

Additionally, instrumental leakage of Stokes-I into other Stokes parameters in off-axis
regions of the beams can be corrected during mosaicking. For more details, see the pipeline documentation
on linear mosaicking at :doc:`ScienceFieldMosaicking` and code documentation at :doc:`../calim/linmos`.


The Stokes parameters given by ``SPECTRAL_POLARISATIONS`` are iterated
over in the imaging (and mosaicking) to create a separate spectral
cube for each. The default is just to do Stokes I.

+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| Variable                                   | Default                             | Parset equivalent                                      | Description                                                   |
+============================================+=====================================+========================================================+===============================================================+
| ``JOB_TIME_APPLY_LEAKAGE``                 | ``JOB_TIME_DEFAULT`` (24:00:00)     | none                                                   | Time request for applying the leakage calibration             |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_APPLY_LEAKAGE_SPEC``                  | false                               | none                                                   | Whether to apply the leakage calibration to the               |
|                                            |                                     |                                                        | fully-calibrated spectral-line dataset, using the table       |
|                                            |                                     |                                                        | derived using ``DO_LEAKAGE_CAL_SPEC=true``                    |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SPECTRAL_POLARISATIONS``                 | ``"I"``                             | ImageName.polarisation (:doc:`../calim/imager`)        | List of polarisations to create cubes for. This should be     |
|                                            |                                     |                                                        | a comma-separated list of (upper-case) polarisations. Separate|
|                                            |                                     |                                                        | jobs will be launched for each polarisation given.            |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+


Joint imaging
-------------

ASKAPsoft offers the possibility of imaging multiple beams jointly - this can help recover signal at the largest scales, comparable to the beam scale. Within the pipeline, this is possible just (for now) fro the spectral-line data, to meet the requirements of the GASKAP-HI Survey Science Project.

When used, this has a slightly different workflow, whereby each beam is processed in the usual way up to the continuum subtraction, and then all (across all fields, if more than one is present) are combined in a single imaging job. No linmos of the spectral cubes is done (as that is part of the joint imaging), although image-based continuum subtraction can be.

+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| Variable                                     | Default                             | Parset equivalent                                      | Description                                                   |
+==============================================+=====================================+========================================================+===============================================================+
| ``JOB_TIME_JOINT_IMAGING_SPECTRAL``          | ``JOB_TIME_DEFAULT`` (24:00:00)     | none                                                   | Time request for the joint imaging job                        |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_JOINT_IMAGING_SPECTRAL``                | false                               | none                                                   | Whether to run joint imaging                                  |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``NUM_PIXELS_SUBSHAPE_SPECTRAL``             | 1024                                | ...                                                    |                                                               |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_JOINT_SPECTRAL``                   | AProjectWStack                      | gridder (:doc:`../calim/imager`)                       | Gridder to use for joint imaging. SHould be AWProject or      |
|                                              |                                     |                                                        | AProjectWStack.                                               |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_JOINT_SPECTRAL_WMAX``              | 18000                               | wmax (:doc:`../calim/gridder`)                         | The wmax parameter for the joint gridder.                     |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_WMAXCLIP``                         | false                               | wmaxclip (:doc:`../calim/gridder`)                     | The wmaxclip parameter, used for all gridders. By default, a  |
|                                              |                                     |                                                        | hard cut for w planes on [-wmax,wmax] is made and values      |
|                                              |                                     |                                                        | outside this range will trigger an error. When true, w values |
|                                              |                                     |                                                        | outside this range are ignored.                               |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_JOINT_SPECTRAL_NWPLANES``          | 9                                   | nwplanes (:doc:`../calim/gridder`)                     | The nwplanes parameter for the joint gridder.                 |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_JOINT_SPECTRAL_OVERSAMPLE``        | 8                                   | oversample (:doc:`../calim/gridder`)                   | The oversample parameter for the joint gridder                |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_JOINT_SPECTRAL_MAXFEEDS``          | 36                                  | maxfeeds (:doc:`../calim/gridder`)                     |                                                               |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_JOINT_SPECTRAL_MAXFIELDS``         | 4                                   | maxfields (:doc:`../calim/gridder`)                    |                                                               |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_JOINT_SPECTRAL_DIAMETER``          | 12m                                 | diameter (:doc:`../calim/gridder`)                     |                                                               |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_JOINT_SPECTRAL_BLOCKAGE``          | 2m                                  | blockage (:doc:`../calim/gridder`)                     |                                                               |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_JOINT_SPECTRAL_MAXSUPPORT``        | 2048                                | maxsupport (:doc:`../calim/gridder`)                   |                                                               |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_JOINT_SPECTRAL_SHARECF``           | false                               | sharecf (:doc:`../calim/gridder`)                      |                                                               |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_JOINT_SPECTRAL_SPHEROIDALTAPER``   | true                                | spheroidaltaper (:doc:`../calim/gridder`)              |                                                               |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_JOINT_SPECTRAL_SPHEROIDAL_CUTOFF`` | 0.05                                | spheroidalweightscutoff (:doc:`../calim/gridder`)      |                                                               |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_JOINT_SPECTRAL_ROBUSTNESS``        | 0                                   | gridder.robustness (:doc:`../calim/gridder`)           |                                                               |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_SPECTRAL_TOLERANCE``                 | 0.1                                 | tolerance (:doc:`../calim/solver`)                     |                                                               |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_SPECTRAL_WEIGHTCUTOFF``              | zero                                | weightcutoff (:doc:`../calim/solver`)                  |                                                               |
+----------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
