cddcalibrator
=============

The cddcalibrator program performs calibration in a parallel/distributed environment
or on a single computer system. The software leverages MPI, however can be run on
a simple laptop or a large supercomputer. It is based on the ccalibrator program, with
additional functionality for direction-dependent calibration.

Running the program
-------------------

It can be run with the following command, where "config.in" is a file containing
the configuration parameters described in the next section. ::

   $ <MPI wrapper> cddcalibrator -c config.in

Parallel/Distributed Execution
------------------------------
The program is distributed and used a master/worker pattern to distribute and manage work.
Please refer to the section entitled *Parallel/Distributed Execution* in the :doc:`cimager`
as parallel execution of the *cddcalibrator* is the same as the *cimager*.

Configuration Parameters
------------------------

Parset parameters understood by cddcalibrator are given in the following table (all
parameters must have **Cddcalibrator** prefix, i.e. **Cddcalibrator.dataset**). For a number
of parameters certain keywords are substituted, i.e. **%w** is replaced by the rank and
**%n** by the number of nodes in the parallel case. In the serial case, these special
strings are substituted by 0 and 1, respectively. This substitution allows to reuse the
same parameter file on all nodes of the cluster if the difference between jobs assigned to
individual nodes can be coded by using these keywords (e.g. using specially crafted file
names). If a parameter supports substitution, it is clearly stated in the description. 

The **cddcalibrator** is intended to process data from a sufficiently short timescale, where
calibratable unknowns can be assumed to be constant (in the ASKAPsoft approach these
corrections are applied upstream and each calibration cycle corresponds to a separate
execution of the calibration code. At this stage only antenna and direction dependent gain
(without cross-polarisation terms) can be calibrated. Note, that the code is still
experimental and has a number of parameters hard coded.

The output file with the result has a parset format understood by **Cimager**. This output
file is called **result.dat** and has complex-valued (stored as 2-element double vectors
of real and imaginary parts) keywords named like **gain.g11.x.b** and **gain.g22.y.c**,
where **x** and **y** are 0-based antenna numbers, **b** and **c** are 0-based direction
numbers and **g11**, **g22** corresponds to the first and second polarisations (in the
frame of the measurement). 
 
A number of other parameters allowing to narrow down the data selection are understood.
They are given in [:doc:`data_selection`] and should also have the **Cddcalibrator** prefix.

+-----------------------+----------------+--------------+-------------------------------------------------+
|**Parameter**          |**Type**        |**Default**   |**Description**                                  |
+=======================+================+==============+=================================================+
|dataset                |string or       |None          |File name of the measurement set that is to be   |
|                       |vector<string>  |              |calibrated. Cddcalibrator will not write to the  |
|                       |                |              |file - use **ccalapply** to apply calibration    |
|                       |                |              |solutions.                                       |
|                       |                |              |The usual substitution rules apply if            |
|                       |                |              |the parameter is a single string. If the         |
|                       |                |              |parameter is given as a vector of strings all    |
|                       |                |              |measurement sets given by this vector are        |
|                       |                |              |effectively concatenated together on-the-fly in  |
|                       |                |              |the serial case. In the parallel case, the size  |
|                       |                |              |of the vector is required to be either 1 or the  |
|                       |                |              |number of nodes - 1, which which case there is   |
|                       |                |              |one measurement set per worker node.             |
+-----------------------+----------------+--------------+-------------------------------------------------+
|solve                  |string          |"gains"       |String describing what to solve for. Only "gains"|
|                       |                |              |and "ionosphere" are currently supported.        |
+-----------------------+----------------+--------------+-------------------------------------------------+
|interval               |quantity string |"-1s"         |If a positive number is given, a separate        |
|                       |                |              |calibration solution will be made for each chunk |
|                       |                |              |of visibilities obtained within the time interval|
|                       |                |              |equal to the value of this parameter. For a      |
|                       |                |              |negative value, a single solution is made for the|
|                       |                |              |whole dataset                                    |
+-----------------------+----------------+--------------+-------------------------------------------------+
|solver                 |string          |SVD           |Name of the calibration solver. Further          |
|                       |                |              |parameters are given by *solver.something*.      |
|                       |                |              |See :doc:`calsolver` for details.                |
+-----------------------+----------------+--------------+-------------------------------------------------+
|ncycles                |int32           |1             |Number of solving iterations (and iterations over|
|                       |                |              |the dataset, which can be called major cycles).  |
+-----------------------+----------------+--------------+-------------------------------------------------+
|nAnt                   |uint            |36            |Number of antennas in the data buffers, and for  |
|                       |                |              |gain calibration, the number of antennas to solve|
|                       |                |              |for. The code will fail if a requested is made to|
|                       |                |              |solve for more antennas than it has the data for.|
+-----------------------+----------------+--------------+-------------------------------------------------+
|nChan                  |uint            |1             |Number of frequency channels in the data buffers.|
|                       |                |              |For gain calibration this will be 1, however for | 
|                       |                |              |ionospheric calibration there nChan should equal |
|                       |                |              |the number of frequency channels in the dataset. |
+-----------------------+----------------+--------------+-------------------------------------------------+
|nCal                   |uint            |1             |Number of simultaneous directions to solve for.  |
|                       |                |              |The code will fail if the number of directions   | 
|                       |                |              |does not match the number of source names in the |
|                       |                |              |sky model.                                       |
+-----------------------+----------------+--------------+-------------------------------------------------+
|freqframe              |string          |topo          |Frequency frame to work in (the frame is         |
|                       |                |              |converted when the dataset is read). Either lsrk |
|                       |                |              |or topo is supported.                            |
+-----------------------+----------------+--------------+-------------------------------------------------+
|refantenna             |int             |None          |If a valid antenna number is given (in the       |
|                       |                |              |range [0,nAnt-1]), this antenna is used for      |
|                       |                |              |phase referencing. The phases of the             |
|                       |                |              |resulting gains are rotated by the appropriate   |
|                       |                |              |polarisation of the reference antenna, such that |
|                       |                |              |the reference X and Y gains are both zero phase. |
|                       |                |              |Leakages are referenced against the XY phase     |
|                       |                |              |difference of the reference antenna.             |
+-----------------------+----------------+--------------+-------------------------------------------------+
|normalisegains         |bool            |false         |Optional parameter. If defined, and if solving   |
|                       |                |              |for "gains", the newly found antenna gains will  |
|                       |                |              |have their amplitdues set to unity when they are |
|                       |                |              |written to file. This is in lieu of true         |
|                       |                |              |phase-only gain calibration and should be used   |
|                       |                |              |with care.                                       |
+-----------------------+----------------+--------------+-------------------------------------------------+
|datacolumn             |string          |"DATA"        |The name of the data column in the measurement   |
|                       |                |              |set which will be the source of visibilities.This|
|                       |                |              |can be useful to process real telescope data     |
|                       |                |              |which were passed through **casapy** at some     |
|                       |                |              |stage (e.g. to run on calibrated data which are  |
|                       |                |              |stored in the **CORRECTED_DATA** column). In the |
|                       |                |              |measurement set convention, the **DATA** column  |
|                       |                |              |which is used by default contains raw            |
|                       |                |              |uncalibrated data as received directly from the  |
|                       |                |              |telescope. Calibration tasks in **casapy** make a|
|                       |                |              |copy when calibration is applied creating a new  |
|                       |                |              |data column.                                     |
+-----------------------+----------------+--------------+-------------------------------------------------+
|nUVWMachines           |int32           |1             |Size of uvw-machines cache. uvw-machines are used|
|                       |                |              |to convert uvw from a given phase centre to a    |
|                       |                |              |common tangent point. To reduce the cost to set  |
|                       |                |              |the machine up (calculation of the transformation|
|                       |                |              |matrix), a number of these machines is           |
|                       |                |              |cached. The key to the cache is a pair of two    |
|                       |                |              |directions: the current phase centre and the     |
|                       |                |              |tangent centre. If the required pair is within   |
|                       |                |              |the tolerances of that used to setup one of the  |
|                       |                |              |machines in the cache, this machine is reused. If|
|                       |                |              |none of the cache items matches the least        |
|                       |                |              |accessed one is replaced by the new machine which|
|                       |                |              |is set up with the new pair of directions. The   |
|                       |                |              |code would work faster if this parameter is set  |
|                       |                |              |to the number of phase centres encountered during|
|                       |                |              |imaging. In non-faceting case, the optimal       |
|                       |                |              |setting would be the number of synthetic beams   |
|                       |                |              |times the number of fields. For faceting (btw,   |
|                       |                |              |the performance gain is quite significant in this|
|                       |                |              |case), it should be further multiplied by the    |
|                       |                |              |number of facets. Direction tolerances are given |
|                       |                |              |as a separate parameter.                         |
+-----------------------+----------------+--------------+-------------------------------------------------+
|uvwMachineDirTolerance |quantity string |"1e-6rad"     |Direction tolerance for the management of the    |
|                       |                |              |uvw-machine cache (see **nUVWMachines** for      |
|                       |                |              |details). The value should be an angular         |
|                       |                |              |quantity. The default value corresponds roughly  |
|                       |                |              |to 0.2 arcsec and seems sufficient for all       |
|                       |                |              |practical applications within the scope of       |
|                       |                |              |ASKAPsoft.                                       |
+-----------------------+----------------+--------------+-------------------------------------------------+
|sources.definition     |string          |None          |Optional parameter. If defined, sky model        |
|                       |                |              |(i.e. source info given as **sources.something**)|
|                       |                |              |is read from a separate parset file (name is     |
|                       |                |              |given by this parameter). If this parameter is   |
|                       |                |              |not defined, source description should be given  |
|                       |                |              |in the main parset file. Usual substitution rules|
|                       |                |              |apply. The parameters to define sky model are    |
|                       |                |              |described in :doc:`csimulator` (with             |
|                       |                |              |Cddcalibrator prefix instead of Csimulator)      |
+-----------------------+----------------+--------------+-------------------------------------------------+
|imagetype              |string          |"casa"        |Format of images used for image-based sky models.|
+-----------------------+----------------+--------------+-------------------------------------------------+
|gridder                |string          |None          |Name of the gridder, further parameters are given|
|                       |                |              |by **gridder.something**. See :doc:`gridder` for |
|                       |                |              |details.                                         |
+-----------------------+----------------+--------------+-------------------------------------------------+
|rankstoringcf          |int             |1             |In the parallel mode, only this rank will attempt|
|                       |                |              |to export convolution functions if this operation|
|                       |                |              |is requested (see **tablename** option in the    |
|                       |                |              |:doc:`gridder`). This option is ignored in the   |
|                       |                |              |serial mode.                                     |
+-----------------------+----------------+--------------+-------------------------------------------------+
|visweights             |string          |""            |If this parameter is set to "MFS" gridders are   |
|                       |                |              |setup to degrid with the weight required for the |
|                       |                |              |models given as Taylor series                    |
|                       |                |              |(i.e. multi-frequency synthesis models). At the  |
|                       |                |              |moment, this parameter is decoupled from the     |
|                       |                |              |setup of the model parameters. The user has to   |
|                       |                |              |set it separately and in a consistent way with   |
|                       |                |              |the model setup (the **nterms** parameter in the |
|                       |                |              |model definition (see :doc:`csimulator` for more |
|                       |                |              |details) should be set to something greater than |
|                       |                |              |1 and there should be an appropriate number of   |
|                       |                |              |models defined).                                 |
+-----------------------+----------------+--------------+-------------------------------------------------+
|visweights.MFS.reffreq |double          |1.405e9       |Reference frequency in Hz for MFS-model          |
|                       |                |              |simulation (see above)                           |
+-----------------------+----------------+--------------+-------------------------------------------------+

The resulting parameters are stored into a solution source (or sink to be exact) as described in :doc:`calibration_solutions`

Parameters related to ionospheric calibration
---------------------------------------------

The following parameters must be supplied for ionospheric calibration.

+-----------------------+----------------+--------------+-------------------------------------------------+
|**Parameter**          |**Type**        |**Default**   |**Description**                                  |
+=======================+================+==============+=================================================+
|frequencies            |vector<string>  |None          |A 3-element vector describing the spectral window|
|                       |                |              |of the dataset, containing the number of         |
|                       |                |              |channels, frequency of the first channel         |
|                       |                |              |(quantity) and frequency increment (quantity)    |
|                       |                |              |(e.g. [50, 125MHz, 1MHz]).                       |
+-----------------------+----------------+--------------+-------------------------------------------------+
|antennas.definition    |string          |None          |Specify an antenna layout file. See              |
|                       |                |              |:doc:`csimulator` for a description of this      |
|                       |                |              |file. Note that only *local* coordinates are     |
|                       |                |              |allowed.                                         |
+-----------------------+----------------+--------------+-------------------------------------------------+

Examples
--------

Gain calibration

.. code-block:: bash

    Cddcalibrator.dataset                                     = imgtest.ms
    Cddcalibrator.refantenna                                  = 0

    Cddcalibrator.sources.names                               = [field1,field2]
    Cddcalibrator.sources.field1.direction                    = [12h30m00.000, -45.00.00.000, J2000]
    Cddcalibrator.sources.field1.model                        = image.field1
    Cddcalibrator.sources.field2.direction                    = [12h30m00.000, -45.00.00.000, J2000]
    Cddcalibrator.sources.field2.model                        = image.field1

    Cddcalibrator.gridder                                     = AProjectWStack
    Cddcalibrator.gridder.AProjectWStack.wmax                 = 15000
    Cddcalibrator.gridder.AProjectWStack.nwplanes             = 10
    Cddcalibrator.gridder.AProjectWStack.oversample           = 8
    Cddcalibrator.gridder.AProjectWStack.diameter             = 12m
    Cddcalibrator.gridder.AProjectWStack.blockage             = 2m
    Cddcalibrator.gridder.AProjectWStack.maxfeeds             = 2
    Cddcalibrator.gridder.AProjectWStack.maxsupport           = 1024
    Cddcalibrator.gridder.AProjectWStack.frequencydependent   = false

    Cddcalibrator.solver                                      = LSQR
    Cddcalibrator.interval                                    = 10s
    Cddcalibrator.ncycles                                     = 5

    Cddcalibrator.nAnt                                        = 36
    Cddcalibrator.nChan                                       = 1
    Cddcalibrator.nCal                                        = 2

    Cddcalibrator.calibaccess                                 = table
    Cddcalibrator.calibaccess.table                           = cddcal_add12.tab
    Cddcalibrator.calibaccess.table.maxant                    = 36
    Cddcalibrator.calibaccess.table.maxchan                   = 1
    Cddcalibrator.calibaccess.table.maxbeam                   = 2 # the beam dimension is used for cal directions

Ionospheric calibration

.. code-block:: bash

    Cddcalibrator.dataset                                     = comptest.ms
    Cddcalibrator.refantenna                                  = 0

    Cddcalibrator.sources.names                               = [field1,field2]
    Cddcalibrator.sources.field1.direction                    = [12h30m00.000, -45.00.00.000, J2000]
    Cddcalibrator.sources.field1.components                   = [comp1]
    Cddcalibrator.sources.comp1.flux.i                        = 0.52
    Cddcalibrator.sources.comp2.shape.bmaj                    = 0.00131
    Cddcalibrator.sources.comp2.shape.bmin                    = 0.00109
    Cddcalibrator.sources.comp2.shape.bpa                     = 1.855
    Cddcalibrator.sources.comp1.direction.ra                  =  0.006
    Cddcalibrator.sources.comp1.direction.dec                 = -0.004
    # phase centre is not handled properly at this time, so use a constant direction and specify offsets
    Cddcalibrator.sources.field2.direction                    = [12h30m00.000, -45.00.00.000, J2000]
    Cddcalibrator.sources.field2.components                   = [comp2]
    Cddcalibrator.sources.comp2.flux.i                        = 0.091
    Cddcalibrator.sources.comp2.flux.spectral_index           = -0.7
    Cddcalibrator.sources.comp2.flux.ref_freq                 = 1e9
    Cddcalibrator.sources.comp2.direction.ra                  = 0.200
    Cddcalibrator.sources.comp2.direction.dec                 = 0.012

    Cddcalibrator.gridder                                     = SphFunc

    Cddcalibrator.solver                                      = LSQR
    Cddcalibrator.ncycles                                     = 15

    Cddcalibrator.nAnt                                        = 224
    Cddcalibrator.nChan                                       = 100
    Cddcalibrator.nCal                                        = 2

    Cddcalibrator.calibaccess                                 = parset
    Cddcalibrator.calibaccess.parset                          = comptest.out

