msmerge (Measurement Merging Utility)
=====================================

The *msmerge* utility is used to merge measurement sets in frequency.

The intended use-cases of this tool are:

- Merge measurement sets from different frequency sub-bands into a single
  measurement set. It is used, for example, to create a single simulated
  measurement set when one needs to split a simulation into sub-bands or
  to combine the channel ranges for a single beam from the sub-band single
  beam files produced by ingest.

It is assumed all input files have the same number of channels and are listed in the correct order to combine them into the full spectrum output.
For ASKAP only, it can deal with the occasional missing integration for
one of the sub-band files, otherwise files should be identical apart
from frequency and data.

Running the program
-------------------

It can be run with the following command ::

   $ msmerge -o output_file list_of_input_files

The *msmerge* program is not parallel/distributed, it runs in a single process.

Configuration Parameters
------------------------

At this time *msmerge* does not accept the same parameter (Parset) file as used in other tasks.
However it has a number of command line flags ::

   -x <n> or --tileNcorr: specify the output tile size for correlations/polarisations
   -c <n> or --tileNchan: specify the output tile size for channels
   -r <n> or --tileNrow: specify the output tile size for rows
   -b <n> or --blocksize: blocksize (default 4MB)
   -B <n> or --bufferMB: maximum buffer size in MB (default 2000)
   -f <string> or --format: table file format - either separate, combined or hdf5, default is separate
   --odirect true : use direct (unbuffered) IO if possible (default false)
   -d true or --dryrun: do a 'dry run', checks the input files and reports missing
             integrations, but doesn't write any output. Runs very quickly.
   -o <output file> or --output-file: specify the output MeasurementSet
   -i <input files> or --input-file: specify the input files (or just list them at the end)
   -h or --help: print usage information
   -p or --parameter-file: parameter file with keyword=value specifications for above options

The default tiling is 4 polarizations, 1 channel, and as many rows as can be
cached efficiently.


Example
-------

**Example 1:**

.. code-block:: bash

   $ msmerge -o fullband.ms subband_???.ms

**Example 2:**

.. code-block:: bash

   $ msmerge -x 4 -c 54 -o output.ms channel1.ms channel2.ms channel3.ms
