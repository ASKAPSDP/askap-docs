Introduction
============

Welcome to the user documentation for the ASKAP Central Processor. These pages contain
documentation for the commissioning scientists and early-science users interested in
carrying out calibration, imaging, and analysis of ASKAP data products.

The bulk of the tools documented here are the individual processing
tasks that make up the typical ASKAP workflow. Many of these tasks
live in the *Yandasoft* calibration & imaging package.

Also documented is the ASKAP processing pipeline itself, that provides
a coherent workflow using these tasks.

More information on (including the scope of) the ASKAP processing
pipelines please refer to the document entitled *ASKAP Science
Processing* (`ASKAP-SW-0020`_).

  .. _ASKAP-SW-0020: http://www.atnf.csiro.au/projects/askap/ASKAP-SW-0020.pdf

A set of tutorials are provided in :doc:`../tutorials/index`
that introduce some basic data processing.
