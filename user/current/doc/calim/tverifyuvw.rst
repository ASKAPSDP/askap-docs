tVerifyUVW (sanity check for uvw)
=================================

This page provides instruction for tVerifyUVW program which can be used for basic sanity check of *uvw* 
baseline spacings in a measurement set. It uses antenna layout, epoch, field and beam information recorded in
the measurement set (accessed via standard YandaSoft interfaces) to predict *uvw* for each visibility record and
compares it with the values stored in the *UVW* column of the measurement set. The model used by this tool is
entirely casacore-based and is the same model as used during the early science phase when fringe rotation and
calculation of *UVW* were controlled by the ingest pipeline. This model doesn't take higher order relativistic
corrections into account, so at some level the difference is expected even for perfectly working instrument. 
The tool is serial and by default prints
just average statistics per integration, i.e. the minimum and maximum stretch of the baseline relative to the model 
as well as the minimum and maximum angle between the predicted and stored *uvw* vectors calculated across all rows
corresponding to the given time. In the verbose mode,
the results of comparison are reported separately for each baseline and beam. In addition, a warning is given if
baseline of zero length is encountered for a cross-correlation. Only unflagged data points are compared.


Running the program
-------------------

Unlike many YandaSoft routines, this tool does not require a parset.  It can be run with the following command
passing the measurement set on the command line ::

   $ tVerifyUVW [-v] measurement_set

The **-v** option enables the verbose mode. If either *askap.log_cfg* or *tVerifyUVW.log_cfg* files are present in the 
directory where the tool is executed and located, respectively, it will be used as log configuration. The former takes
precedence.

An example of the output when running the tool on an ATCA dataset (converted into the measurement set format)

.. code-block:: txt
 
   $ apps/tVerifyUVW 331.132.mjd57996.ms
   ...
   INFO  accessors.tVerifyUVW (, ) [2022-10-11 02:57:01.264] - For Epoch: 57996::11:44:54.9308 UVW min/max stretch values are -0.000107391 -0.000103426 min/max angles (deg) are 0.01787 0.0181429
   INFO  accessors.tVerifyUVW (, ) [2022-10-11 02:57:01.266] - For Epoch: 57996::11:45:04.9293 UVW min/max stretch values are -0.000107342 -0.000103416 min/max angles (deg) are 0.0178808 0.0181556
   INFO  accessors.tVerifyUVW (, ) [2022-10-11 02:57:01.268] - For Epoch: 57996::11:45:14.9302 UVW min/max stretch values are -0.000107301 -0.000105009 min/max angles (deg) are 0.0178805 0.0181558
   INFO  accessors.tVerifyUVW (, ) [2022-10-11 02:57:01.270] - For Epoch: 57996::11:45:24.9287 UVW min/max stretch values are -0.000107346 -0.00010495 min/max angles (deg) are 0.0178954 0.0181677
   INFO  accessors.tVerifyUVW (, ) [2022-10-11 02:57:01.271] - For Epoch: 57996::11:45:34.9297 UVW min/max stretch values are -0.000107137 -0.00010488 min/max angles (deg) are 0.017893 0.0181693
   ...

The stretch statistics is defined as 

.. code-block:: txt

   stretch = (measured_length - simulated_length) / simulated_length

and can be both positive and negative depending on whether the measured baseline is longer or shorter than expected.
