Calibration and Imaging Documentation
======================================

The synthesis package supports processing of radio synthesis observations, including
calibration, editing, and imaging. The emphasis is on the processing of ASKAP scale
data sets i.e. Hundereds of TB per hour.

**Contents:**

.. toctree::
   :maxdepth: 1

   imager.rst
   cimager.rst
   csimulator.rst
   ccalibrator.rst
   cbpcalibrator.rst
   cddcalibrator.rst
   cdeconvolver-mpi.rst
   ccalapply.rst
   ccontsubtract.rst
   imcontsub.rst
   gridder.rst
   solver.rst
   calsolver.rst
   data_selection.rst
   calibration_solutions.rst
   cmodel.rst
   cflag.rst
   makecube.rst
   mssplit.rst
   msmerge.rst
   msconcat.rst
   mslist.rst
   linmos.rst
   imagetofits.rst
   timagewrite.rst
   extractslice.rst
   tverifyuvw.rst
   tclearmscache.rst
