tImageWrite (Image Accessor Test)
=================================

This page provides instruction for tImageWrite test program which can be used to test standard image
access utilised by many YandaSoft applications, e.g. :doc:`linmos`. It is also a handy tool to 
explore I/O performance as it mimics access patterns which can be encountered in real-worl astronomy applications.

Running the program
-------------------

It can be run with the following command, where "config.in" is a file containing
the configuration parameters described in the next section. ::

   $ tImageWrite -c config.in

The *tImageWrite* program supports execution in parallel and can use distributed I/O if supported by the
particular accessor selected in the "config.in" parameter file.

Configuration Parameters
------------------------

The following table contains the configuration parameters to be specified in the *config.in*
file shown on above command line. Note that unlike the actual YandaSoft applications this
test tool exercises interfaces directly and doesn't require any prefix for parameters.
In general, all parameters supported by image access factory can be used here. 

+------------------+------------------+--------------+------------------------------------------------------------+
|**Parameter**     |**Type**          |**Default**   |**Description**                                             |
+==================+==================+==============+============================================================+
|mask              |boolean           |false         |If true, a masked cube will be written (i.e. it will contain|
|                  |                  |              |a  boolean mask for each pixel showing its validity).       |
+------------------+------------------+--------------+------------------------------------------------------------+
|mask.threshold    |float             |3.            |If masked cube is written (see the previous parameter), this|
|                  |                  |              |parameter describes "flagging" threshold. All pixels with   |
|                  |                  |              |the value exceeding *mask.threshold* will be masked out.    |
|                  |                  |              |Set to large positive value to have no masking (if you want |
|                  |                  |              |a masked cube in the output but with all pixels marked as   |
|                  |                  |              |good) or to a large negative value to have all pixels masked|
+------------------+------------------+--------------+------------------------------------------------------------+
|variance          |double            |1.            |Variance of the Gaussian noise to fill the simulated cube   |
|                  |                  |              |with. Note the value of this parameter when choosing the    |
|                  |                  |              |threshold for masking with *mask.threshold*.                |
+------------------+------------------+--------------+------------------------------------------------------------+
|name              |string            |fakecube      |Name of the output image.                                   |
+------------------+------------------+--------------+------------------------------------------------------------+
|stokes            |vector of strings |["I"]         |Desired Stokes axis for the simulated cube                  |
+------------------+------------------+--------------+------------------------------------------------------------+
|spectral_first    |boolean           |true          |If true, the simulated cube will have the spectral axis     |
|                  |                  |              |before the Stokes axis. If false, the order will be the     |
|                  |                  |              |opposite.                                                   |
+------------------+------------------+--------------+------------------------------------------------------------+
|size              |unsigned int      |1024          |Number of pixels on each side for the spatial coordinates of|
|                  |                  |              |the simulated cube. Only square planes are supported at the |
|                  |                  |              |moment.                                                     |
+------------------+------------------+--------------+------------------------------------------------------------+
|nchan             |unsigned int      |10 if serial, |The number of spectral channels in the sumulated cube.      |
|                  |                  |the number of |The default is different depending on the number of ranks   |
|                  |                  |ranks         |available: in the serial mode (i.e. only one rank is        |
|                  |                  |otherwise     |available) it is 10 planes, otherwise it is the number of   |
|                  |                  |              |ranks (suitable for parallel writers).                      |
+------------------+------------------+--------------+------------------------------------------------------------+
|mode              |string            |"parallel" or |This parameter can be either "parallel" or "serial" and     |
|                  |                  |"serial" as   |defines the type of the image accessor factory created.     |
|                  |                  |appropriate   |The default is "serial" if only one rank is available and   |
|                  |                  |              |"parallel" otherwise, so most users don't need to worry     |
|                  |                  |              |about it. However, it may be useful to force a particular   |
|                  |                  |              |type of accessor to exercise it in    unusual circumstances.|
+------------------+------------------+--------------+------------------------------------------------------------+

The following options are related to image accessors and are shared between all applications doing image writing:

+------------------+------------------+--------------+------------------------------------------------------------+
|imagetype         |string            |"casa"        |Type of the image handler (determines the format of         |
|                  |                  |              |the images, both which are written to or read from          |
|                  |                  |              |the disk). The default is to create casa images but         |
|                  |                  |              |"fits" can also be chosen.                                  |
+------------------+------------------+--------------+------------------------------------------------------------+
|imageaccess       |string            |"individual"  |The imageaccess parameters are for fits images only:        |
|                  |                  |              |Choose between "individual" and "collective" I/O operations.|
|                  |                  |              |For collective I/O the number of ranks must divide evenly   |
|                  |                  |              |into the number of channels.                                |
+------------------+------------------+--------------+------------------------------------------------------------+
|imageaccess.axis  |uint              |0             |Specify the channel axis of the cubes - usually 3           |
|                  |                  |              |This is only used for collective I/O on fits cubes          |
+------------------+------------------+--------------+------------------------------------------------------------+
|imageaccess.order |string            |"distributed" |How do the ranks read/write channels:                       |
|                  |                  |              |                                                            |
|                  |                  |              | - "distributed" over the spectrum, one section per rank    |
|                  |                  |              | - "contiguous" in rank order, a section at a time          |
|                  |                  |              |                                                            |
|                  |                  |              |For collective I/O "contiguous" is automatically selected   |
+------------------+------------------+--------------+------------------------------------------------------------+
|imageaccess.write |string            |"serial"      |How does the output cube get written:                       |
|                  |                  |              |                                                            |
|                  |                  |              | - "serial" one rank at a time                              |
|                  |                  |              | - "parallel" all ranks at the same time                    |
|                  |                  |              |                                                            |
|                  |                  |              | For casa images "serial" is used regardless.               |
+------------------+------------------+--------------+------------------------------------------------------------+

Example
--------

Example tImageWrite parset to create a fake fits image cube with 10 spectral planes and 4 Stokes planes (IQUV).
Each plane will contain 8192x8192 spatial pixels. The cube will be filled with the Gaussian noise with the 
variance of 1. Pixles stronger than 3 Jy will be masked out (given the variance, there will be a number of such pixels).

.. code-block:: bash

    mask=true
    mask.threshold=3
    mode=parallel
    imagetype=fits
    name=test
    spectral_first=true
    stokes=[I,Q,U,V]
    size=8192
    nchan=10

