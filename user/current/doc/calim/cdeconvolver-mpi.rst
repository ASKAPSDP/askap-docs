cdeconvolver-mpi
================

This software takes either the gridded visibilities, gridded weights and the PCF
grid or the image equivalents of those (dirty image, natural weighted PSF, and
PCF image) and runs a minor cycle solver to produce a deconvolved image.
The idea is to use this to do multi epoch integrations of large
spectral datasets by storing cubes with Nyquist grids in compressed format,
adding them together and then deconvolving them. This avoids having to store the
large spectral MeasurementSets at the cost of only being able to use minor cycle
solvers.

To generate the uv-grid or raw (unnormalised) image input cdeconvolver-mpi
expects you need to run imager with the following parameters present:

.. code-block:: bash

    Cimager.ncycles = 0
    Cimager.restore = false
    Cimager.nyquistgridding = true # not required, but reduces size of output
    Cimager.write.grids = true
    Cimager.write.grids.fft = false # for uv grid, or true for raw images
    Cimager.write.psfimage = false
    Cimager.write.modelimage = false
    Cimager.preconditioner.Names = [Wiener] # Wiener preconditioner is required
    Cimager.preconditioner.Wiener.robust = 0.0 # specify required value


The imager task should be run with only a single input MeasurementSet at a time,
as the grids will only contain data from the first MS specified. The use of the
Wiener preconditioner is required at present. Specify the parameters in the
imager run to generate the PCF grid cdeconvolver-mpi expects to find.

In addition the SpheroidalFunction parameter alpha (e.g.
Cimager.gridder.Wproject.alpha) should be left at the default value of 1.0 as
that is what cdeconvolver-mpi assumes when it does the convolution correction.

cdeconvolver-mpi can deal with a few different input types (matching the output
grid types in imager):
Input can be a complex valued casa image with uv-coordinate system,
a complex FITS image split in real and imaginary parts containing uv-grids but
with the image coordinates of the corresponding (FFT) image attached or a
standard FITS or casa image. In all cases specify the base name only: e.g.,
"visgrid.epoch1" will look for casa image "visgrid.epoch1" if imagetype=casa and
FITS image "visgrid.epoch1.fits", "visgrid.epoch1.real.fits" and
"visgrid.epoch1.imag.fits" if imagetype=fits.

In the case of a casa uv-grid, the user needs to supply the image direction,
cellsize and shape used during imaging in the parset.
The case of a FITS uv-grid with uv-coordinates is not currently implemented
because FITS readers don't support this very well.

The cdeconvolver-mpi task is, as the name implies, mpi-enabled and can be run
on cubes with a number of ranks that divides evenly into the number of channels.
E.g., for a 6 channel cube, you can use 1, 2, 3 or 6 ranks (including rank 0).

Parset parameters understood by cdeconvolver-mpi are given in the following
table. All parameters must have *Cdeconvolver* prefix, e.g.,
*Cdeconvolver.solver.Clean.algorithm*).

+------------------------------+--------------+--------------------+--------------------------------------------------------+
|*Parameter*                   |*Type*        |*Default*           |*Description*                                           |
+==============================+==============+====================+========================================================+
|imagetype                     |string        |fits                |the type (fits or casa) of images                       |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|visgrid                       |vector<string>|none                |Input visibility grids or dirty images. Names must start|
|                              |              |                    |with *visgrid*, the psf and pcf grids will be opened    |
|                              |              |                    |by replacing this with *psfgrid* and *pcfgrid*.         |
|                              |              |                    |All input grids will be added together before           |
|                              |              |                    |preconditioning and deconvolution.                      |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|oversampling                  |float         |1.0                 |Oversampling applied to images before they              |
|                              |              |                    |are passed to the deconvolver. Essential to set this to |
|                              |              |                    |1.5 or larger if Nyquist gridding was used in imager.   |
|                              |              |                    |To use the value imager determined and create images in |
|                              |              |                    |the same resolution as specified in imager search the   |
|                              |              |                    |imager log for "extraoversampling" and use that value   |
|                              |              |                    |here.                                                   |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|solver                        |string        |none                |Name of the solver. Currently supported                 |
|                              |              |                    |option is "Clean".                                      |
|                              |              |                    |Further parameters are given by                         |
|                              |              |                    |solver.solver_name.something.                           |
|                              |              |                    |See Solver Documentation for details.                   |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|solver.Clean.algorithm        |string        |BasisfunctionMFS    |Algorithm option for a solver:                          |
|                              |              |                    |Hogbom, Basisfunction, BasisfunctionMFS.                |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|threshold.minorcycle          |vector<string>|no threshold        |Use to set the clean limit (see Solvers)                |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|preconditioner.Names          |vector<string>|none                |Preconditioners to use: GaussianTaper and               |
|                              |              |                    |Wiener are available. See solvers                       |
|                              |              |                    |preconditioner documentation                            |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|Images.Names                  |vector<string>|None                |Base name of output images - only the first entry is    |
|                              |              |                    |used for this task. Image names must start with the     |
|                              |              |                    |word *image*                                            |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|Images.direction              |vector<string>|None                |When using imagetype=casa and a UV grid as input specify|
|                              |              |                    |the image direction exactly like in the original imaging|
|                              |              |                    |run that created the UV grids.                          |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|Images.cellsize               |vector<string>|None                |When using imagetype=casa and a UV grid as input specify|
|                              |              |                    |the size of pixels in the spatial axes in the original  |
|                              |              |                    |imaging run , e.g. [6.0arcsec, 6.0arcsec].  If Nyquist  |
|                              |              |                    |gridding was used in imager, specify the cellsize of the|
|                              |              |                    |Nyquist grid (search for "Changing cellsize" in the log)|
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|Images.shape                  |vector<int>   |None                |When using imagetype=casa and a UV grid as input specify|
|                              |              |                    |the shape from the original imaging run. If Nyquist     |
|                              |              |                    |gridding was used in imager, specify the shape of the   |
|                              |              |                    |Nyquist grid (search for "Changing shape" in the log)   |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|restore.beam                  |vector<string>|fit                 |Either a single word 'fit' or a quantity string         |
|                              |              |                    |describing the shape of the clean beam (to convolve the |
|                              |              |                    |model image with). If quantity is given it must have    |
|                              |              |                    |exactly 3 elements, e.g. [30arcsec, 10arcsec,           |
|                              |              |                    |40deg]. Otherwise an exception is thrown. This parameter|
|                              |              |                    |is only used if *write.restoredimage* is set to True. If|
|                              |              |                    |*restore.beam=fit*, the code will fit a 2D gaussian to  |
|                              |              |                    |the PSF image and use the results of this fit. In this  |
|                              |              |                    |case, each channel will have an independently-fitted    |
|                              |              |                    |beam.                                                   |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|restore.beamReference         |string        |mid                 |The channel to use as the reference for the beam - this |
|                              |              |                    |channel's beam is written to the cube header. Values can|
|                              |              |                    |be an integer indicating the channel number (0-based),  |
|                              |              |                    |or one of 'mid', 'first', or 'last'.                    |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|restore.beam.cutoff           |double        |0.5                 |Cutoff for the support search prior to beam fitting, as |
|                              |              |                    |a fraction of the PSF peak. This parameter is only used |
|                              |              |                    |if *restore.beam=fit*. The code does fitting on a       |
|                              |              |                    |limited support (to speed things up and to avoid        |
|                              |              |                    |sidelobes influencing the fit). The extent of this      |
|                              |              |                    |support is controlled by this parameter representing the|
|                              |              |                    |level of the PSF which should be included into          |
|                              |              |                    |support. This value should be above the first sidelobe  |
|                              |              |                    |level for meaningful results.                           |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|imageaccess.write             |string        |serial              |For FITS images "parallel" can be used to               |
|                              |              |                    |write the output in parallel which is often faster      |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|write.residualimage           |bool          |false               |If true write the residual image                        |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|write.psfimage                |bool          |false               |If true write the preconditioned PSF                    |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|write.modelimage              |bool          |false               |If true write the clean model                           |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|write.restoredimage           |bool          |true                |If true write the restored image                        |
|                              |              |                    |At least one image must be written out                  |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|calcstats                     |bool          |false               |Write per plane image statistics to image               |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|outputStats                   |string        |""                  |Write per plane image statistics to file                |
|                              |              |                    |with given name as well, or not if empty                |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|imageHistory                  |vector<string>|*none*              |Specify lines to add to the image history               |
+------------------------------+--------------+--------------------+--------------------------------------------------------+
|header.<keyword>              |vector<string>|["","","STRING"]    |Header keyword to write to the image header.            |
|                              |              |                    |Replace <keyword> with up to 8 character name           |
|                              |              |                    |of the keyword. Second string is the keyword            |
|                              |              |                    |description or comment (default no comment).            |
|                              |              |                    |Third string is the optional type (INT,DOUBLE,          |
|                              |              |                    |or STRING) with STRING the default.                     |
+------------------------------+--------------+--------------------+--------------------------------------------------------+

Examples
--------

**Example 1:**

Example cdeconvolver-mpi parset to first add the 2 input grids in FITS format, use preconditioning with a Wiener filter (robustness=0.5)
and then deconvolve the oversampled image using the BasisFunctionMFS cleaner. By default we're only writing out the resulting restored image cube.
Beam shape and statistics information will be embedded in the output file (as records for casa and as binary tables for FITS).

.. code-block:: bash

    Cdeconvolver.visgrid                          = [visgrid.epoch1,visgrid.epoch2]
    Cdeconvolver.Images.Names                     = [image.two-epochs]
    Cdeconvolver.oversampling                     = 2.0
    Cdeconvolver.solver                           = Clean
    Cdeconvolver.solver.Clean.algorithm           = BasisFunctionMFS
    Cdeconvolver.solver.Clean.scales              = [0, 3, 10]
    Cdeconvolver.solver.Clean.psfwidth            = 256
    Cdeconvolver.solver.Clean.gain                = 0.1
    Cdeconvolver.solver.Clean.niter               = 1000

    Cdeconvolver.preconditioner.Names             = [Wiener]
    Cdeconvolver.preconditioner.Wiener.robustness = 0.5
