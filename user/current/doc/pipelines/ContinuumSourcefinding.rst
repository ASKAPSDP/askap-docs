User Parameters - Continuum Source-finding
==========================================

Source-finding is run in two ways within these scripts. It can be run
after each individual beam is imaged, and then again after the
mosaicking has completed.

The source-finding applies a spatially-varying signal-to-noise
threshold, and fits 2D Gaussians to detected islands to create a
component catalogue.

When the source-finding operates on the mosaicked image, it uses the
weights image to apply a weights cutoff (as described at
:doc:`../analysis/thresholds`), so that the searching is only done on
the imaged region.

The RM Synthesis capabilities of Selavy have been made available. Full
control over this mode is provided through the pipeilne configuration,
so that RM Synthesis can be performed on the full-Stokes spectra of
continuum components. This is designed to work with the continuum cube
imaging (:doc:`ScienceFieldContinuumImaging`), and checks are made for
the existence of the required cubes at run-time for this mode to be
switched on.

Additionally, Selavy can extract spectra from the continuum-subtracted
cube (or the restored cube if image-based continuum subtraction is not
being used) at the location of each bright continuum component. Both
source and noise spectra will be extracted. The flux limit can be set
via ``SELAVY_EXTRACT_FLUX_LIMIT``, given in mJy (since that is how
fluxes are recorded in the Selavy catalogues).

Following completion of the source-finding, a validation script is run
that cross-matches the Selavy catalogue with those of SUMSS and NVSS,
comparing positions and fluxes with those catalogues to obtain
measures of the quality of the imaging. A full report is provided in
HTML format, with interactive plots, along with a summary table
listing a small number of metrics that have associated quality flags
(essentially "good", "uncertain", "bad"). These metrics are presented
within CASDA to assist with the validation of data. (See
`doc:validation` for details.)

This validation can be performed on the individual beam images,
although the default is to just do it on the mosaicked images (and it
is only the latter that are passsed to CASDA).


+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| Variable                                          |             Default                      | Parset equivalent                           | Description                                                 |
+===================================================+==========================================+=============================================+=============================================================+
| ``DO_SOURCE_FINDING_CONT``                        | ``""``                                   | none                                        | Whether to do the source-finding with Selavy on the         |
|                                                   |                                          |                                             | final mosaic continuum images. If not given in the config   |
|                                                   |                                          |                                             | file, it takes on the value of ``DO_CONT_IMAGING``.         |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
|  ``CATALOGUE_BASE_NAME``                          | selavy-%i.txt                            | Selavy.resultsFile                          | Base name for source catalogues. The %i wildcard will be    |
|                                                   |                                          | (:doc:`../analysis/selavy`)                 | resolved into the imageName corresponding to the continuum  |
|                                                   |                                          |                                             | image. You may find this parameter useful to rename new     |
|                                                   |                                          |                                             | catalogues generated from re-processing.                    |
|                                                   |                                          |                                             | NB: The same parameter is used for spectral source-finding. |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
|  ``CATALOGUE_ID_BASE``                            | SB%s                                     | Selavy.sourceIdBase                         | Base identifier for catalogue components. The %s wildcard   |
|                                                   |                                          | (:doc:`../analysis/selavy`)                 | will be resolved into the SBID of the observation.          |
|                                                   |                                          |                                             | You may find this parameter useful to rename new catalogue  |
|                                                   |                                          |                                             | components generated from re-processing.                    |
|                                                   |                                          |                                             | NB: The same parameter is used for spectral source-finding. |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``DO_SOURCE_FINDING_BEAMWISE``                    | false                                    | none                                        | If true, the source-finding will be run on the individual   |
|                                                   |                                          |                                             | beam images as well.                                        |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``DO_SOURCE_FINDING_FIELD_MOSAICS``               | false                                    | none                                        | If true, the source-finding will be run on the individual   |
|                                                   |                                          |                                             | field mosaics. Will be set to false if the number of fields |
|                                                   |                                          |                                             | is one.                                                     |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``JOB_TIME_SOURCEFINDING``                        | ``JOB_TIME_DEFAULT`` (24:00:00)          | none                                        | Time request for source-finding jobs.                       | 
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``DO_CONTINUUM_VALIDATION``                       | true                                     | none                                        | Run the continuum validation script following source        |
|                                                   |                                          |                                             | finding. This cross-matches the catalogue with publshed     |
|                                                   |                                          |                                             | radio calaogues from NVSS and SUMSS, and compares fluxes and|
|                                                   |                                          |                                             | positions. A number of metrics are obtained that describe   |
|                                                   |                                          |                                             | the quality of the image and catalogue, and these, along    |
|                                                   |                                          |                                             | with a more detailed HTML-based report, are sent to CASDA.  |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``CUBES_ON_SINGLE_OST``                           | false                                    | none                                        | If set to ``true``, the continuum cubes (when doing RM      |
|                                                   |                                          |                                             | synthesis) and/or the spectral cube (when doing extraction  |
|                                                   |                                          |                                             | of spectra) will be copied to a single OST prior to running |
|                                                   |                                          |                                             | selavy. This has been shown to speed up processing times.   |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+ 
| **Basic sourcefinding**                           |                                          |                                             |                                                             |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``CORES_PER_NODE_SELAVY``                         | 64 (setonix), 10 (petrichor/galaxy)      | none                                        | Number of cores used on each node. If not provided, it will |
|                                                   |                                          |                                             | be the lower of the number of cores requested or the maximum|
|                                                   |                                          |                                             | number of cores available per node.                         |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_NSUBX``                                  | 6                                        | nsubx                                       | Number of divisions in the x-direction that divide the image|
|                                                   |                                          | (:doc:`../analysis/selavy`)                 | up, allowing parallel processing in the source-detection.   |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_NSUBY``                                  | 6                                        | nsuby                                       | Number of divisions in the y-direction that divide the image|
|                                                   |                                          | (:doc:`../analysis/selavy`)                 | up, allowing parallel processing in the source-detection.   |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_OVERLAPX``                               | 0                                        | overlapx                                    | The overlap (in pixels) between neighbouring divisions in   | 
|                                                   |                                          | (:doc:`../analysis/selavy`)                 | the x-direction.                                            |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_OVERLAPY``                               | 0                                        | overlapy                                    | The overlap (in pixels) between neighbouring divisions in   |
|                                                   |                                          | (:doc:`../analysis/selavy`)                 | the y-direction.                                            | 
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_SNR_CUT``                                | 5                                        | snrcut                                      | The signal-to-noise ratio threshold to use in the           |
|                                                   |                                          | (:doc:`../analysis/selavy`)                 | source-detection.                                           |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+ 
| ``SELAVY_FLAG_GROWTH``                            | true                                     | flagGrowth                                  | A flag indicating whether to grow detections down to a      |
|                                                   |                                          | (:doc:`../analysis/selavy`)                 | lower threshold.                                            |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_GROWTH_CUT``                             | 3                                        | growthCut                                   | The secondary signal-to-noise threshold to which detections | 
|                                                   |                                          | (:doc:`../analysis/selavy`)                 | should be grown.                                            |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_FLUX_THRESHOLD``                         | ``""``                                   | threshold                                   | The flux threshold to use in the source-detection. If left  |
|                                                   |                                          | (:doc:`../analysis/selavy`)                 | blank, we use the SNR threshold ``SELAVY_SNR_CUT``.         |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_GROWTH_THRESHOLD``                       | ``""``                                   | growthCut                                   | The secondary signal-to-noise threshold to which detections | 
|                                                   |                                          | (:doc:`../analysis/selavy`)                 | should be grown. Only used if ``SELAVY_FLUX_THRESHOLD`` is  |
|                                                   |                                          |                                             | given.                                                      |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_WEIGHTS_CUTOFF``                         | ``""``                                   | Weights.weightsCutoff                       | The cutoff level, as a fraction of the peak in the weights  | 
|                                                   |                                          | (:doc:`../analysis/thresholds`)             | image, used in the source-finding. Only applies if the image|
|                                                   |                                          |                                             | being searched has a corresponding weights image. If not    |
|                                                   |                                          |                                             | given, the value used is the square of ``LINMOS_CUTOFF``    |
|                                                   |                                          |                                             | from :doc:`ScienceFieldMosaicking`.                         |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_VARIABLE_THRESHOLD``                     | true                                     | VariableThreshold                           | A flag indicating whether to determine the signal-to-noise  |
|                                                   |                                          | (:doc:`../analysis/thresholds`)             | threshold on a pixel-by-pixel basis based on local          |
|                                                   |                                          |                                             | statistics (that is, the statistics within a relatively     | 
|                                                   |                                          |                                             | small box centred on the pixel in question).                |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_BOX_SIZE``                               | 50                                       | VariableThreshold.boxSize                   | The half-width of the sliding box used to determine the     |
|                                                   |                                          | (:doc:`../analysis/thresholds`)             | local statistics.                                           |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_VARIABLE_THRESHOLD_REUSE``               | false                                    | VariableThreshold.reuse                     | A flag indicating whether to reuse any existing noise maps  |
|                                                   |                                          | (:doc:`../analysis/thresholds`)             | created by the variable-threshold algorithm. If false, they |
|                                                   |                                          |                                             | will be generated each time the job runs.                   |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_FLAG_ADJACENT``                          | true                                     | flagAdjacent                                | Whether to enforce pixels in islands to be contiguous.      |
|                                                   |                                          | (:doc:`../analysis/selavy`)                 |                                                             |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_SPATIAL_THRESHOLD``                      | 5                                        | threshSpatial                               | If ``SELAVY_FLAG_ADJACENT=false``, this is the threshold in |
|                                                   |                                          | (:doc:`../analysis/selavy`)                 | pixels within which islands are joined.                     |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_WRITE_DUCHAMP_FILES``                    | false                                    | none                                        | If true, the standard Duchamp catalogue files are written in|
|                                                   |                                          |                                             | addition to the Selavy ones (island, component, etc)        |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_INCLUDE_GALACTIC_COORDS``                | false                                    | catalogueHasGalacticCoords                  | Whether to include Galactic coordinates in the output       |
|                                                   |                                          | (:doc:`../analysis/selavy`)                 | catalogues (components & islands). If the input image is    |
|                                                   |                                          |                                             | GALACTIC (via ``LINMOS_DIRECTION_TYPE=GALACTIC``) this will |
|                                                   |                                          |                                             | be set to true automatically.                               |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| **Spectral indices**                              |                                          |                                             |                                                             |
|                                                   |                                          |                                             |                                                             |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_SPECTRAL_INDEX_THRESHOLD``               | ""                                       | spectralTerms.threshold                     | Threshold applied to component peak fluxes in determining   |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | which have a spectral index (and curvature) value reported  |
|                                                   |                                          |                                             | in the component catalogue. Not used if left blank. Takes   |
|                                                   |                                          |                                             | precedence over ``SELAVY_SPECTRAL_INDEX_THRESHOLD_SNR``.    |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
|      ``SELAVY_SPECTRAL_INDEX_THRESHOLD_SNR``      | 50.                                      | spectralTerms.thresholdSNR                  | Threshold applied to component peak signal-to-noise values  |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | in determining which have a spectral index (and curvature)  |
|                                                   |                                          |                                             | value reported in the component catalogue. Not used if left |
|                                                   |                                          |                                             | blank.                                                      |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``USE_CONTCUBE_FOR_SPECTRAL_INDEX``               | false                                    | none                                        | When true, the spectral indices in the component catalogue  |
|                                                   |                                          |                                             | are measured from the Stokes-I continuum cube. When false   |
|                                                   |                                          |                                             | (the default), the Taylor-term images are used. See         |
|                                                   |                                          |                                             | :doc:`../analysis/postprocessing` for information about each|
|                                                   |                                          |                                             | method.                                                     |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_NUM_SPECTRAL_TERMS``                     | 3                                        | spectralTerms.nterms                        | The number of terms to be fitted to the spectrum when using |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | the continuum cube to find the spectral index & curvature.  |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| **RM Synthesis**                                  |                                          |                                             |                                                             |
|                                                   |                                          |                                             |                                                             |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``DO_RM_SYNTHESIS``                               | false                                    | none                                        | Whether to perform RM Synthesis after continuum             |
|                                                   |                                          |                                             | source-finding.                                             |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_OUTPUT_BASE``                        | pol                                      | Forms part of                               | Base part of the filenames of extracted spectra and Faraday |
|                                                   |                                          | RMSynthesis.outputBase                      | Dispersion function. All files will go in a directory       |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | PolData within the Selavy directory, and will be called     |
|                                                   |                                          |                                             | "<outputBase>_<imageBase>_spec" or similar.                 |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_WRITE_SPECTRA``                      | true                                     | RMSynthesis.writeSpectra                    | Whether to write the extracted Stokes spectra to individual |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | files.                                                      |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_WRITE_COMPLEX_FDF``                  | false                                    | RMSynthesis.writeComplexFDF                 | Whether to write the Faraday Dispersion Function for each   |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | source as a single complex-valued spectrum (true) or as a   |
|                                                   |                                          |                                             | pair of real-valued spectra containing amplitude & phase    |
|                                                   |                                          |                                             | (false).                                                    |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
|     ``USE_FITS_TABLE_FOR_EXTRACTED_SPECTRA``      | false                                    | RMSynthesis.usefitstable                    | Whether to write the extracted spectra to a single FITS     |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | binary table (if ``true``), rather than individual FITS     |
|                                                   |                                          |                                             | files. The binary table option is only available from       |
|                                                   |                                          |                                             | askapsoft/1.14 onwards.                                     |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_BOX_WIDTH``                          | 5                                        | RMSynthesis.boxWidth                        | The width (N) of the NxN box to be applied in the extraction|
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | of Stokes spectra.                                          |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_NOISE_AREA``                         | 50                                       | RMSynthesis.noiseArea                       | The number of beam areas over which to measure the noise in |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | each channel.                                               |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_ROBUST_STATS``                       | true                                     | RMSynthesis.robust                          | Whether to use robust statistics in the calculation of the  |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | noise spectra.                                              |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_WEIGHT_TYPE``                        | variance                                 | RMSynthesis.weightType                      | The type of weighting to be used in the RM Synthesis -      |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | either "variance" or "uniform".                             |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_MODEL_TYPE``                         | taylor                                   | RMSynthesis.modelType                       | The type of Stokes-I model to use. Either "taylor"          |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | (Taylor-term decomposition from the MFS imaging), or "poly" |
|                                                   |                                          |                                             | (polynomial fit to the Stokes-I spectrum).                  |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_MODEL_ORDER``                        | 3                                        | RMSynthesis.modelPolyOrder                  | When ``SELAVY_POL_MODEL_TYPE=poly``, this gives the order of|
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | the polynomial that is fit to the Stokes-I spectrum.        |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_SNR_THRESHOLD``                      | 8                                        | RMSynthesis.polThresholdSNR                 | Signal-to-noise threshold (in the FDF) for a valid          |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | detection. Only sources above this threshold have their     |
|                                                   |                                          |                                             | spectra written to disk.                                    |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_SNR_THRESHOLD_BEAMWISE``             | 0                                        | RMSynthesis.polThresholdSNR                 | As for ``SELAVY_POL_SNR_THRESHOLD``, but applied to the     |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | beam-wise source-finding (ie. source-finding on the         |
|                                                   |                                          |                                             | individual beam images - when                               |
|                                                   |                                          |                                             | ``DO_SOURCE_FINDING_BEAMWISE=true``)                        |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_DEBIAS_THRESHOLD``                   | 5                                        | RMSynthesis.polThresholdDebias              | Signal-to-noise threshold (in the FDF) above which to       |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | perform debiasing.                                          |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_NUM_PHI_CHAN``                       | 30                                       | RMSynthesis.numPhiChan                      | Number of Faraday Depth channels used in RM Synthesis.      |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         |                                                             |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_DELTA_PHI``                          | 5                                        | RMSynthesis.deltaPhi                        | Spacing between the Faraday depth channels [rad/m2].        |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         |                                                             |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_POL_PHI_ZERO``                           | 0                                        | RMSynthesis.phiZero                         | Faraday depth [rad/m2] of the central channel of the FDF.   |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         |                                                             |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| **Spectral extraction**                           |                                          |                                             |                                                             |
|                                                   |                                          |                                             |                                                             |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``DO_EXTRACT_COMPONENT_SPECTRA``                  | false                                    | none                                        | Whether to extract spectra for each bright continuum        |
|                                                   |                                          |                                             | component. When true, both source and noise spectra are     |
|                                                   |                                          |                                             | extracted. The cube used will be the contsub cube (if made -|
|                                                   |                                          |                                             | else the regular restored cube). If the cube doesn't exist, |
|                                                   |                                          |                                             | this mode is turned off.                                    |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``USE_FITS_TABLE_FOR_EXTRACTED_SPECTRA``          | false                                    | Components.extractSpectra.usefitstable      | Whether to write the extracted spectra to a single FITS     |
|                                                   |                                          | Components.extractNoiseSpectra.usefitstable | binary table (if ``true``), rather than individual FITS     |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | files. The binary table option is only available from       |
|                                                   |                                          |                                             | askapsoft/1.14 onwards.                                     |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``USE_FITS_TABLE_FOR_EXTRACTED_SPECTRA_BEAMWISE`` | false                                    | Components.extractSpectra.usefitstable      | As above, but just for the case of the beam-wise            |
|                                                   |                                          | Components.extractNoiseSpectra.usefitstable | source-finding.                                             |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         |                                                             |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_EXTRACT_FLUX_LIMIT``                     | 10.                                      | Components.extractSpectra.fluxLimit         | The flux limit to apply when extracting. The value should be|
|                                                   |                                          | Components.extractNoiseSpectra.fluxLimit    | given **in mJy**, and is applied to the integrated flux of  |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | the component. The same flux limit is applied for both      |
|                                                   |                                          |                                             | source and nosie spectra.                                   |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_EXTRACT_BOX_WIDTH``                      | 5                                        | Components.extractSpectra.spectralBoxWidth  | The width (N) of the NxN box to be applied in the spectral  |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | extraction.                                                 |
|                                                   |                                          | (:doc:`../analysis/extraction`)             |                                                             |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``SELAVY_EXTRACT_NOISE_AREA``                     | 50                                       | Components.extractNoiseSpectra.noiseArea    | The number of beam areas over which to measure the noise in |
|                                                   |                                          | (:doc:`../analysis/postprocessing`)         | each channel.                                               |
|                                                   |                                          | (:doc:`../analysis/extraction`)             |                                                             |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
| ``PURGE_SPECTRA_AFTER_TAR``                       | false                                    | none                                        | If set, the spectra extracted either from                   |
|                                                   |                                          |                                             | ``DO_EXTRACT_COMPONENT_SPECTRA=true`` or                    |
|                                                   |                                          |                                             | ``DO_RM_SYNTHESIS=true`` are removed immediately after they |
|                                                   |                                          |                                             | are tarred up into tar files for archiving.                 |
+---------------------------------------------------+------------------------------------------+---------------------------------------------+-------------------------------------------------------------+
