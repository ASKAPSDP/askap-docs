User Parameters - Continuum imaging
===================================

Summary
-------

The parameters listed here allow the user to configure the parset and
the slurm job for continuum imaging of the science field. The defaults
have been set based on experience with science commissioning and ASKAP
Early Science, using the 12-antenna array. If you are processing
different data sets, adjusting these defaults may be necessary.

Some of the parameters can be set to blank, to allow cimager to choose
appropriate values based on its "advise" capability (which involves
examining the dataset and setting appropriate values for some
parameters.

For most observations to date, there is a single phase centre that
applies for all beams. Thus, if *advise* is used to determine the
image centre, the same direction will be used for each beam. This is
the way the BETA processing was done, and in this case the image size
must be chosen to be big enough to encompass all beams. This behaviour
is applied when ``IMAGE_AT_BEAM_CENTRES=false``.

The preferred behaviour, however, is to set a different image centre
per beam, as this allows a smaller image to be made when imaging. To
do this, the *footprint.py* tool is used to determine the locations of
the centres of each beam. The footprint specification is determined
preferentially from the scheduling block parset, or (if not available
there) from the parameters described at
:doc:`ScienceFieldMosaicking`. To use this mode, set
``IMAGE_AT_BEAM_CENTRES=true``.

While the default application used for the imaging is **cimager**, it
is possible to use the newer **imager** (:doc:`../calim/imager`). Much
of the functionality will be identical for continuum data (imager was
developed initially as a better spectral-line imaging tool).

Self-calibration
................

The default approach for continuum imaging is to do it in conjunction with self-calibration.
The algorithm here is as follows:

1. Image the data with cimager or imager
2. Run source-finding with Selavy with a relatively large threshold
   (unless we are using option 3c. below)
3. Use the results to calibrate the antenna-based gains by either:

   a. Create a component parset from the resulting component catalogue and use this parset in ccalibrator
   b. Create a model image from the component catalogue, and use in ccalibrator
   c. Use the clean model image from the most recent imaging as the
      model for ccalibrator (no source-finding will be done)

4. Re-run imaging, applying the latest gains table
5. Repeat steps 2-5 for a given number of loops

Each loop gets its own directory, where the intermediate images,
source-finding results, and gains calibration table are stored. At the
end, the final gains calibration table is kept in the main output
directory (as this can then be used by the spectral-line imaging
pipeline).

There are two options for how this is run. The first
(``MULTI_JOB_SELFCAL=true``) launches a separate slurm job for each
imaging task, and each "calibration" task - the calibration job
incorporates the selavy, cmodel and ccalibrator tasks. The latter uses
a single node only. If ``MULTI_JOB_SELFCAL=false``, these are all
incorporated into one slurm job, including all loops.

Some parameters are allowed to vary with the loop number of the
self-calibration. This way, you can decrease, say, the detection
threshold, or increase the number of major cycles, as the calibration
steadily improves. These parameters are for:

* Deconvolution: ``CLEAN_ALGORITHM``, ``CLEAN_GAIN``, ``CLEAN_PSFWIDTH``,
  ``CLEAN_THRESHOLD_MAJORCYCLE``, ``CLEAN_NUM_MAJORCYCLES``,
  ``CLEAN_MINORCYCLE_NITER``, ``CLEAN_THRESHOLD_MINORCYCLE`` and
  ``CLEAN_SCALES``
* Preconditioning: ``PRECONDITIONER_LIST``,
  ``PRECONDITIONER_GAUSS_TAPER``,
  ``PRECONDITIONER_GAUSS_TAPER_IS_PSF``,
  ``PRECONDITIONER_GAUSS_TAPER_TOL``,
  ``PRECONDITIONER_WIENER_ROBUSTNESS``, and
  ``PRECONDITIONER_WIENER_TAPER``
* Self-calibration: ``SELFCAL_SELAVY_THRESHOLD``, ``SELFCAL_INTERVAL``
  and ``SELFCAL_NORMALISE_GAINS``
* Data selection in imaging: ``CIMAGER_MINUV`` and ``CIMAGER_MAXUV``
* Data selection in calibration: ``CCALIBRATOR_MINUV`` and ``CCALIBRATOR_MAXUV``

To use this mode, the values for these parameters should be given as
an array in the form ``SELFCAL_INTERVAL="[1800,1800,900,300]"``, or,
when the parameter would take an array of values, those arrays should
be separated by semi-colons (with a space on either side):
``CLEAN_SCALES="[0] ; [0,20] ; [0,20,120,240] ; [0,20,120,240,480]"``.

The size of these arrays should be one more than
``SELFCAL_NUM_LOOPS``. This is because loop 0 is just the imaging, and
it is followed by ``SELFCAL_NUM_LOOPS`` loops of
source-find--model--calibrate--image. Only the parameters related to
the imaging (the deconvolution & preconditioning parameters in the list above) have
the first element of their array used. If a single value is given for
these parameters, it is used for every loop.

Instead of preconditioning, it is possible to use traditional
weighting through the the use of ``TRADITIONAL_WEIGHT_PARAM_LIST`` and
``TRADITIONAL_WEIGHT_ROBUSTNESS``, which can be loop dependent as
well.

Once the gains solution has been determined, it can be applied
directly to the continuum measurement set, creating a copy in the
process. This is necessary for continuum cube processing, and for
archiving purposes.
This work is done as a separate slurm job, that starts upon
completion of the self-calibration job.

There is a prototype option available (using
``SELFCAL_MASK_CLEAN_MODEL``) that allows the clean model that is used
for calibration to be modified. Initially, this process just applies a
single threshold to the image, masking out pixels below that
threshold, but the aim is to add further refinements to this
process. This only alters the image used by the calibration task.


External calibration
....................

A prototype option has been provided to allow the selfcal process
to start with a phase-only calibration against a user-provided source
catalogue (using the ``EXTERNAL_CATALOGUE`` parameter).
This catalogue is used to create a model image, which is
then used by ccalibrator to do phase-only calibration of the
data. This is done prior to imaging, so that the first image has
phases calibrated to the positions of sources in the catalogue. This
can help improve the quality of the first image used in the
selfcal. Self-calibration then proceeds as normal, with subsequent
calibration steps using the model derived from that initial image.

Two special values for ``EXTERNAL_CATALOGUE`` are recognised:
``racs_mid`` or ``racs_low``, which allow the querying of the
respective RACS whole-sky catalogues through the sky model service
interface in ``cmodel``. This mode also provides for the application
of primary-beam attenuation via the same interface used in linmos. 

Leakage correction
..................

The pipeline now has a feature to optionally derive and correct for
the effect of on-axis leakages of unpolarised ssources to the the other
Stokes parameters. The leakages are derived from the bandpass-corrected,
spectrally-averaged and flagged PKS1934-638 data. Users need to specify
``DO_LEAKAGE_CAL_CONT=true`` to derive the antenna leakages, and specify
``DO_APPLY_LEAKAGE=true`` to apply the leakage corrections to uvdata.
The leakage correction is applied to gain-corrected data obtained after
the self-calibration.

Further imaging : polarisation and cubes
........................................

Following this application of the gains and/or leakage calibration solution,
one can optionally image the continuum dataset as a cube, preserving the
frequency sampling. This task also allows the specification of
multiple polarisations, with a cube created for each polarisation
given.

A prototype option is available (using
``DO_MFS_STARTING_MODEL_CONTCUBE``) to use the model image from the
MFS (continuum) imaging as the starting point for the clean model in
the continuum cube imaging.

Following continuum-cube imaging, the cube statistics are calculated,
using a distributed task within the same slurm job as the spectral
imaging. This produces a file listing a series of statistics for each
channel, as well as a plot of the statistics. See :doc:`validation`
for examples. These statistics are then used to identify problematic
channels (for example, due to divergence in the imaging caused by RFI)
that are then masked. Blank channels (those not imaged, e.g. due to
barycentric correction or missing data) are also masked. The
statistics file is then regenerated.

A note on the imagers and the output formats. The default approach is
to now use **imager** (:doc:`../calim/imager`) for both continuum and
spectral imaging. The use of **simager** for spectral imaging (in this
case, the continuum cubes) is to be discouraged, as imager provides
much better functionality for integration into the pipeline. The use
of **cimager** for the MFS imaging is possible, although it provides
less flexibility in how the job can be distributed across nodes. The
choice of imaging task is governed by ``DO_ALT_IMAGER_CONT`` or
``DO_ALT_IMAGER_CONTCUBE``, with the main switch ``DO_ALT_IMAGER``
taking precedence.

The default output format is FITS images, although CASA files can be
written instead by setting ``IMAGETYPE_CONT`` or
``IMAGETYPE_CONTCUBE`` to ``casa`` (rather than ``fits``).

This version allows continuum imaging of various polarisation products.
The self-calibration stage is used for Stokes-I imaging. The calibrated
uv-data is then used to make images of other polarisations. Users need
to specify the list of polarisation using ``CONTIMG_POLARISATIONS`` in
their pipeline configuration file. In a later version, we aim to separate
the self-calibration from the imaging, so that all pols including Stokes-I
are imaged along with the other polarisations.


Basic imaging parameters
------------------------

This table summarises the fundamental parameters that govern the
continuum imaging, and that are used in most of the different types of
imaging jobs. This also has specific parameters related to the use of
:doc:`../calim/imager`.


+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| Variable                                       | Default                             | Parset equivalent                                      | Description                                                   |
+================================================+=====================================+========================================================+===============================================================+
| ``DO_CONT_IMAGING``                            | true                                | none                                                   | Whether to image the science MS                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``JOB_TIME_CONT_IMAGE``                        | ``JOB_TIME_DEFAULT`` (24:00:00)     | none                                                   | Time request for imaging the continuum (both types - with and |
|                                                |                                     |                                                        | without self-calibration)                                     |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``IMAGETYPE_CONT``                             | fits                                | imagetype (:doc:`../calim/cimager` and                 | Image format to use - can be either 'casa' or 'fits'.         |
|                                                |                                     | :doc:`../calim/imager`)                                |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``IMAGETYPE_CONTCUBE``                         | fits                                | imagetype (:doc:`../calim/imager`)                     | Image format to use - can be either 'casa' or 'fits',         |
|                                                |                                     |                                                        | although 'fits' can only be given in conjunction with         |
|                                                |                                     |                                                        | ``DO_ALT_IMAGER_CONTCUBE=true``.                              |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ```MULTI_JOB_SELFCAL``                         | false                               | none                                                   | Whether to break the selfcal up into separate slurm jobs for  |
|                                                |                                     |                                                        | each imaging and calibration task (``true``) or whether to    |
|                                                |                                     |                                                        | combine them all into a single slurm job.                     |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``JOB_TIME_CONT_SELFCAL``                      | ``JOB_TIME_DEFAULT`` (24:00:00)     | none                                                   | Time request for the calibration jobs when running with       |
|                                                |                                     |                                                        | ``MULTI_JOB_SELFCAL=true``.                                   |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| **Basic variables**                            |                                     |                                                        |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``IMAGE_AT_BEAM_CENTRES``                      | true                                | none                                                   | Whether to have each beam's image centred at the centre of    |
|                                                |                                     |                                                        | the beam (IMAGE_AT_BEAM_CENTRES=true), or whether to use a    |
|                                                |                                     |                                                        | single image centre for all beams.                            |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``NUM_CORES_CONTIMG_SCI``                      | ``""``                              | none                                                   | The number of cores in total to use for the continuum         |
|                                                |                                     |                                                        | imaging. If left blank (``""`` - the default), then this is   |
|                                                |                                     |                                                        | calculated based on the number of channels and Taylor terms.  |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CHANNEL_SELECTION_CONTIMG_SCI``              | ``""``                              | Channels (:doc:`../calim/data_selection`)              | If ``NUM_CORES_CONTIMG_SCI`` is given, the Channels selection |
|                                                |                                     |                                                        | is provided here. This can be left blank for no selection to  |
|                                                |                                     |                                                        | be applied, or a string (in quotes) conforming to the data    |
|                                                |                                     |                                                        | selection syntax can be provided.                             |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CORES_PER_NODE_CONT_IMAGING``                | 6 (galaxy), 36 (setonix/petrichor)  | Not for parset                                         | Number of cores to use on each node in the continuum imaging. |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``FAT_NODE_CONT_IMG``                          | true                                | Not for parset                                         | Whether the master process for the continuum imaging should be|
|                                                |                                     |                                                        | put on a node of its own (if ```true```), or just treated like|
|                                                |                                     |                                                        | all other processes.                                          |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DATACOLUMN``                                 | DATA                                | datacolumn (:doc:`../calim/cimager`)                   | The column in the measurement set from which to read the      |
|                                                |                                     |                                                        | visibility data. The default, 'DATA', is appropriate for      |
|                                                |                                     |                                                        | datasets processed within askapsoft, but if you are trying to |
|                                                |                                     |                                                        | image data processed, for instance, in CASA, then changing    |
|                                                |                                     |                                                        | this to CORRECTED_DATA may be what you want.                  |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``IMAGE_BASE_CONT``                            | i.%t.SB%s.cont                      | Helps form Images.Names                                | The base name for images: if ``IMAGE_BASE_CONT=i.blah`` then  |
|                                                |                                     | (:doc:`../calim/cimager`)                              | we'll get image.i.blah, image.i.blah.restored, psf.i.blah etc.|
|                                                |                                     |                                                        | The %s wildcard will be resolved into the scheduling block ID |
|                                                |                                     |                                                        | and %t will be replaced by the "target" or scheduling block   |
|                                                |                                     |                                                        | alias.                                                        |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DIRECTION_SCI``                              | none                                | Images.<imagename>.direction                           | The direction parameter for the images, i.e. the central      |
|                                                |                                     | (:doc:`../calim/cimager`)                              | position. Can be left out, in which case Cimager will get it  |
|                                                |                                     |                                                        | from either the beam location (for                            |
|                                                |                                     |                                                        | IMAGE_AT_BEAM_CENTRES=true) or from the measurement set using |
|                                                |                                     |                                                        | the "advise" functionality (for IMAGE_AT_BEAM_CENTRES=false). |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``NUM_PIXELS_CONT``                            | 6144                                | Images.shape                                           | The number of pixels on the side of the images to be created. |
|                                                |                                     | (:doc:`../calim/cimager`)                              | If negative, zero, or absent (i.e. ``NUM_PIXELS_CONT=""``),   |
|                                                |                                     |                                                        | this will be set automatically by the Cimager “advise”        |
|                                                |                                     |                                                        | function, based on examination of the MS. Note that this      |
|                                                |                                     |                                                        | default will be suitable for a single beam, but probably not  |
|                                                |                                     |                                                        | for an image to be large enough for the full set of beams     |
|                                                |                                     |                                                        | (when using IMAGE_AT_BEAM_CENTRES=false). The default value,  |
|                                                |                                     |                                                        | combined with the default for the cell size, should be        |
|                                                |                                     |                                                        | sufficient to cover a full field. If you have                 |
|                                                |                                     |                                                        | IMAGE_AT_BEAM_CENTRES=true then this needs only to be big     |
|                                                |                                     |                                                        | enough to fit a single beam.                                  |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CELLSIZE_CONT``                              | 2                                   | Images.cellsize                                        | Size of the pixels in arcsec. If negative, zero or absent,    |
|                                                |                                     | (:doc:`../calim/cimager`)                              | this will be set automatically by the Cimager “advise”        |
|                                                |                                     |                                                        | function, based on examination of the MS. The default is      |
|                                                |                                     |                                                        | chosen together with the default number of pixels to cover a  |
|                                                |                                     |                                                        | typical ASKAP beam with the sidelobes being imaged.           |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``NUM_TAYLOR_TERMS``                           | 2                                   | Images.image.${imageBase}.nterms                       | Number of Taylor terms to create in MFS imaging. If more than |
|                                                |                                     | (:doc:`../calim/cimager`)                              | 1, MFS weighting will be used (equivalent to setting          |
|                                                |                                     | linmos.nterms (:doc:`../calim/linmos`)                 | **Cimager.visweights=MFS** in the cimager parset).            |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``MFS_REF_FREQ``                               | no default                          | visweights.MFS.reffreq                                 | Frequency at which continuum image is made [Hz]. This is the  |
|                                                |                                     | (:doc:`../calim/cimager`)                              | reference frequency for the multi-frequency synthesis, which  |
|                                                |                                     |                                                        | should usually be the middle of the band. If negative, zero,  |
|                                                |                                     |                                                        | or absent (the default), this will be set automatically to    |
|                                                |                                     |                                                        | the average of the frequencies being processed.               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``RESTORING_BEAM_CONT``                        | fit                                 | restore.beam                                           | Restoring beam to use: 'fit' will fit the PSF to determine    |
|                                                |                                     | (:doc:`../calim/cimager` & :doc:`../calim/solver`)     | the appropriate beam, else give a size (such as               |
|                                                |                                     |                                                        | ``“[30arcsec, 30arcsec, 0deg]”``).                            |
|                                                |                                     |                                                        | NB: If ``PRECONDITIONER_GAUSS_TAPER_IS_PSF=true``             |
|                                                |                                     |                                                        | the restoring beam will be derived such that the output       |
|                                                |                                     |                                                        | image has the specified resolution.                           |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``RESTORING_BEAM_CUTOFF_CONT``                 | 0.5                                 | restore.beam.cutoff                                    | Cutoff value used in determining the support for the fitting  |
|                                                |                                     | (:doc:`../calim/simager`)                              | (ie. the rectangular area given to the fitting routine).      |
|                                                |                                     |                                                        | Value is a fraction of the peak.                              |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CIMAGER_MINUV``                              | 0                                   | MinUV (:doc:`../calim/data_selection`)                 | The minimum UV distance considered in the imaging - used to   |
|                                                |                                     |                                                        | exclude the short baselines. Can be given as an array with    |
|                                                |                                     |                                                        | different values for each self-cal loop                       |
|                                                |                                     |                                                        | (e.g. ``"[200,200,0]"``).                                     |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CIMAGER_MAXUV``                              | 0                                   | MaxUV (:doc:`../calim/data_selection`)                 | The maximum UV distance considered in the imaging. Only used  |
|                                                |                                     |                                                        | if greater than zero. Can be given as an array with different |
|                                                |                                     |                                                        | values for each self-cal loop (e.g. ``"[200,200,0]"``).       |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| **Gridding parameters**                        |                                     |                                                        |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER``                                    | MPIWProject                         | gridder                                                | Specify griddder to use: WProject or MPIWProject              |
|                                                |                                     | (:doc:`../calim/gridder`)                              | MPIWProject can save memory and time when using multiple ranks|
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_NYQUIST_GRIDDING_CONT``                   | true                                | Images.nyquistgridding                                 | Whether to turn on Nyquist gridding.                          |
|                                                |                                     | (:doc:`../calim/imager`)                               |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_SNAPSHOT_IMAGING``                   | false                               | snapshotimaging                                        | Whether to use snapshot imaging when gridding.                |
|                                                |                                     | (:doc:`../calim/gridder`)                              |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_SNAPSHOT_WTOL``                      | 2600                                | snapshotimaging.wtolerance                             | The wtolerance parameter controlling how frequently to        |
|                                                |                                     | (:doc:`../calim/gridder`)                              | snapshot.                                                     |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_SNAPSHOT_LONGTRACK``                 | true                                | snapshotimaging.longtrack                              | The longtrack parameter controlling how the best-fit W plane  |
|                                                |                                     | (:doc:`../calim/gridder`)                              | is determined when using snapshots.                           |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_SNAPSHOT_CLIPPING``                  | 0.01                                | snapshotimaging.clipping                               | If greater than zero, this fraction of the full image width   |
|                                                |                                     | (:doc:`../calim/gridder`)                              | is set to zero. Useful when imaging at high declination as    |
|                                                |                                     |                                                        | the edges can generate artefacts.                             |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_WMAX``                               | 2600                                | MPIWProject.wmax                                       | The wmax parameter for the gridder. The default for this      |
|                                                | (``GRIDDER_SNAPSHOT_IMAGING=true``) | (:doc:`../calim/gridder`)                              | depends on whether snapshot imaging is invoked or not         |
|                                                | or 35000                            |                                                        | (``GRIDDER_SNAPSHOT_IMAGING``).                               |
|                                                | (``GRIDDER_SNAPSHOT_IMAGING=false``)|                                                        |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_WMAXCLIP``                           | false                               | MPIWProject.wmaxclip (:doc:`../calim/gridder`)         | The wmaxclip parameter, used for all gridders. By default, a  |
|                                                |                                     |                                                        | hard cut for w planes on [-wmax,wmax] is made and values      |
|                                                |                                     |                                                        | outside this range will trigger an error. When true, w values |
|                                                |                                     |                                                        | outside this range are ignored.                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_NWPLANES``                           | 99                                  | MPIWProject.nwplanes                                   | The nwplanes parameter for the gridder. The default for this  |
|                                                | (``GRIDDER_SNAPSHOT_IMAGING=true``) | (:doc:`../calim/gridder`)                              | depends on whether snapshot imaging is invoked or not         |
|                                                | or 999                              |                                                        | (``GRIDDER_SNAPSHOT_IMAGING``).                               |
|                                                | (``GRIDDER_SNAPSHOT_IMAGING=false``)|                                                        |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_OVERSAMPLE``                         | 8                                   | MPIWProject.oversample                                 | The oversampling factor for the gridder.                      |
|                                                |                                     | (:doc:`../calim/gridder`)                              |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_MAXSUPPORT``                         | 512                                 | MPIWProject.maxsupport                                 | The maxsupport parameter for the gridder. The default for this|
|                                                | (``GRIDDER_SNAPSHOT_IMAGING=true``) | (:doc:`../calim/gridder`)                              | depends on whether snapshot imaging is invoked or not         |
|                                                | or 3072                             |                                                        | (``GRIDDER_SNAPSHOT_IMAGING``).                               |
|                                                | (``GRIDDER_SNAPSHOT_IMAGING=false``)|                                                        |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_SHARECF``                            | true                                | MPIWProject.sharecf (:doc:`../calim/gridder`)          | Whether to use a (static) cache for the convolution functions |
|                                                |                                     |                                                        | in the WProject gridder.                                      |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GRIDDER_CFRANK``                             | 16                                  | MPIWProject.cfrank (:doc:`../calim/gridder`)           | Number of ranks per node to use to speed up convolution       |
|                                                |                                     |                                                        | function calculation. Useful range: ~4 to #ranks/node         |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| **Preconditioning / weighting parameters**     |                                     |                                                        |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_LIST``                        | ``"[Wiener]"``                      | preconditioner.Names                                   | List of preconditioners to apply. Can be varied for each      |
|                                                |                                     | (:doc:`../calim/solver`)                               | selfcal cycle (e.g. ``"[Wiener] ; [Wiener,                    |
|                                                |                                     |                                                        | GaussianTaper]"``. Notice the delimiter ``" ; "`` and the     |
|                                                |                                     |                                                        | spaces around it.                                             |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_GAUSS_TAPER``                 | ``"[10arcsec, 10arcsec, 0deg]"``    | preconditioner.GaussianTaper                           | Size of the Gaussian taper - either single value (for         |
|                                                |                                     | (:doc:`../calim/solver`)                               | circular taper) or 3 values giving an elliptical size.        |
|                                                |                                     |                                                        | Can be varied for each selfcal cycle.                         |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_GAUSS_TAPER_IS_PSF``          | ``"false"``                         | preconditioner.GaussianTaper.isPsfSize                 | Decide if rather than applying the specified taper, the taper |
|                                                |                                     | (:doc:`../calim/solver`)                               | should be adjusted to ensure that the output image has the    |
|                                                |                                     |                                                        | specified resolution.                                         |
|                                                |                                     |                                                        | NB: This parameter is also passed to coarse cube imaging.     |
|                                                |                                     |                                                        | Can be varied for each selfcal cycle.                         |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_GAUSS_TAPER_TOL``             | 0.005                               | preconditioner.GaussianTaper.tolerance                 | Fractional tolerance for the fitted beam size when            |
|                                                |                                     | (:doc:`../calim/solver`)                               | ``PRECONDITIONER_GAUSS_TAPER_IS_PSF = true``                  |
|                                                |                                     |                                                        | The default is set to 0.5%                                    |
|                                                |                                     |                                                        | Can be varied for each selfcal cycle.                         |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_WIENER_ROBUSTNESS``           | -0.5                                | preconditioner.Wiener.robustness                       | Robustness value for the Wiener filter.                       |
|                                                |                                     | (:doc:`../calim/solver`)                               | Can be varied for each selfcal cycle.                         |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_WIENER_TAPER``                | ``""``                              | preconditioner.Wiener.taper                            | Size of gaussian taper applied in image domain to Wiener      |
|                                                |                                     | (:doc:`../calim/solver`)                               | filter. Ignored if blank (ie. “”).                            |
|                                                |                                     |                                                        | Can be varied for each selfcal cycle.                         |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``RESTORE_PRECONDITIONER_LIST``                | ``""``                              | restore.preconditioner.Names                           | List of preconditioners to apply at the restore stage, to     |
|                                                |                                     | (:doc:`../calim/cimager` & :doc:`../calim/solver`)     | produce an additional restored image.                         |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``RESTORE_PRECONDITIONER_EXTENSION``           | ``alt``                             | restore.preconditioner.suffix                          | The additional suffix or extension added to the image name for|
|                                                |                                     | (:doc:`../calim/cimager` & :doc:`../calim/solver`)     | the additional restored images with alternate preconditioning.|
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``RESTORE_PRECONDITIONER_GAUSS_TAPER``         | ``"[10arcsec, 10arcsec, 0deg]"``    | restore.preconditioner.GaussianTaper                   | Size of the Gaussian taper for the restore preconditioning -  |
|                                                |                                     | (:doc:`../calim/cimager` & :doc:`../calim/solver`)     | either single value (for circular taper) or 3 values giving   |
|                                                |                                     |                                                        | an elliptical size.                                           |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``RESTORE_PRECONDITIONER_GAUSS_TAPER_IS_PSF``  | ``"false"``                         | preconditioner.GaussianTaper.isPsfSize                 | Decide if rather than applying the specified taper for images |
|                                                |                                     | (:doc:`../calim/solver`)                               | restored with alternate preconditioning, the taper should be  |
|                                                |                                     |                                                        | adjusted to ensure that the output image has the specified    |
|                                                |                                     |                                                        | resolution.                                                   |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``RESTORE_PRECONDITIONER_GAUSS_TAPER_TOL``     | 0.005                               | preconditioner.GaussianTaper.tolerance                 | Fractional tolerance for the fitted beam size when            |
|                                                |                                     | (:doc:`../calim/solver`)                               | ``RESTORE_PRECONDITIONER_GAUSS_TAPER_IS_PSF = true``          |
|                                                |                                     |                                                        | The default is set to 0.5%                                    |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``RESTORE_PRECONDITIONER_WIENER_ROBUSTNESS``   | -2                                  | restore.preconditioner.Wiener.robustness               | Robustness value for the Wiener filter in the restore         |
|                                                |                                     | (:doc:`../calim/cimager` & :doc:`../calim/solver`)     | preconditioning.                                              |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``RESTORE_PRECONDITIONER_WIENER_TAPER``        | ``""``                              | restore.preconditioner.Wiener.taper                    | Size of gaussian taper applied in image domain to Wiener      |
|                                                |                                     | (:doc:`../calim/cimager` & :doc:`../calim/solver`)     | filter in the restore preconditioning. Ignored if blank       |
|                                                |                                     |                                                        | (ie. “”).                                                     |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``TRADITIONAL_WEIGHT_PARAM_LIST``              | ``""``                              | uvweight (:doc:`../calim/imager`)                      | List of parameters for application of traditional weights.    |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``TRADITIONAL_WEIGHT_ROBUSTNESS``              | -0.5                                | uvweight.robustness (:doc:`../calim/imager`)           | The value of robustness. Only used if there is Robust among   |
|                                                |                                     |                                                        | the list of parameters given by                               |
|                                                |                                     |                                                        | ``TRADITIONAL_WEIGHT_PARAM_LIST``.                            |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ***New imager parameters**                     |                                     |                                                        |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_ALT_IMAGER_CONT``                         | ``""``                              | none                                                   | If true, the continuum imaging is done by imager              |
|                                                |                                     |                                                        | (:doc:`../calim/imager`). If false, it is done by cimager     |
|                                                |                                     |                                                        | (:doc:`../calim/cimager`). If left blank (the default), the   |
|                                                |                                     |                                                        | value is given by the overall parameter ``DO_ALT_IMAGER`` (see|
|                                                |                                     |                                                        | :doc:`ControlParameters`).                                    |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+


Self-calibration
----------------

This table summarises the parameters related to stokes-I imaging and the self-calibration procedure. The Stokes I imaging gets its own set of deconvolution (cleaning) parameters (the Stokes I images will be made in the self-cal part, not the "contpol" part of the pipeline). There are parameters related to the source-finding and calibration parts of the self-calibration




+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| Variable                                        | Default                              | Parset equivalent                                      | Description                                                   |
+=================================================+======================================+========================================================+===============================================================+
| **Cleaning parameters**                         |                                      |                                                        |                                                               |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SOLVER``                                      | Clean                                | solver                                                 | Which solver to use. You will mostly want to leave this as    |
|                                                 |                                      | (:doc:`../calim/cimager`)                              | 'Clean', but there is a 'Dirty' solver available.             |
|                                                 |                                      | (:doc:`../calim/solver`)                               |                                                               |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_ALGORITHM``                             |  BasisfunctionMFS                    | Clean.algorithm                                        | The name(s) of clean algorithm(s) to use.                     |
|                                                 |                                      | (:doc:`../calim/solver`)                               | To use different algorithms in different selfcal cycles, use: |
|                                                 |                                      |                                                        | ``CLEAN_ALGORITHM="Hogbom,BasisfunctionMFS"``                 |
|                                                 |                                      |                                                        | If the number of comma-separated algorithms is less than      |
|                                                 |                                      |                                                        | ``SELFCAL_NUM_LOOPS + 1``, the first algorithm specified will |
|                                                 |                                      |                                                        | be used for ALL selfcal loops.                                |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_MINORCYCLE_NITER``                      | ``"[400,800]"``                      | Clean.niter                                            | The number of iterations for the minor cycle clean. Can be    |
|                                                 |                                      | (:doc:`../calim/solver`)                               | varied for each selfcal cycle. (e.g. ``"[200,800,1000]"``)    |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_GAIN``                                  | 0.2                                  | Clean.gain                                             | The loop gain (fraction of peak subtracted per minor cycle).  |
|                                                 |                                      | (:doc:`../calim/solver`)                               | Can be varied for each selfcal                                |
|                                                 |                                      |                                                        | cycle. (e.g. ``"[0.1,0.2,0.1]"``)                             |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_PSFWIDTH``                              | 256                                  | Clean.psfwidth                                         | The width of the psf patch used in the minor cycle. Can be    |
|                                                 |                                      | (:doc:`../calim/solver`)                               | varied for each selfcal cycle. (e.g. ``"[256,512,4096]"``)    |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_SCALES``                                | ``"[0,3,10]"``                       | Clean.scales                                           | Set of scales (in pixels) to use with the multi-scale clean.  |
|                                                 |                                      | (:doc:`../calim/solver`)                               | Can be varied for each selfcal cycle (e.g. ``"[0] ;           |
|                                                 |                                      |                                                        | [0,10]"``) Notice the delimiter ``" ; "`` and the spaces      |
|                                                 |                                      |                                                        | around it.                                                    |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_THRESHOLD_MINORCYCLE``                  | ``"[30%, 5.0sigma, 0.5sigma]"``      | threshold.minorcycle                                   | Threshold for the minor cycle loop. Can be varied for each    |
|                                                 |                                      | (:doc:`../calim/cimager`)                              | selfcal cycle. (e.g. ``"[30%,1.8mJy,0.03mJy] ;                |
|                                                 |                                      | (:doc:`../calim/solver`)                               | [20%,0.5mJy,0.03mJy]"``) Notice the delimiter ``" ; "`` and   |
|                                                 |                                      |                                                        | the spaces around it.                                         |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_THRESHOLD_MAJORCYCLE``                  | ``"0.035mJy"``                       | threshold.majorcycle                                   | The target peak residual. Major cycles stop if this is        |
|                                                 |                                      | (:doc:`../calim/cimager`)                              | reached. A negative number ensures all major cycles requested |
|                                                 |                                      | (:doc:`../calim/solver`)                               | are done. Can be given as an array with different values for  |
|                                                 |                                      |                                                        | each self-cal loop (e.g. ``"[3mJy,1mJy,-1mJy]"``).            |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_NUM_MAJORCYCLES``                       | ``"[5,10]"``                         | ncycles                                                | Number of major cycles. Can be given as an array with         |
|                                                 |                                      | (:doc:`../calim/cimager`)                              | different values for each self-cal loop (e.g. ``"[2,4,6]"``). |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_WRITE_AT_MAJOR_CYCLE``                  | false                                | Images.writeAtMajorCycle                               | If true, the intermediate images will be written (with a      |
|                                                 |                                      | (:doc:`../calim/cimager`)                              | .cycle suffix) after the end of each major cycle.             |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_SOLUTIONTYPE``                          | MAXBASE                              | Clean.solutiontype (see discussion at                  | The type of peak finding algorithm to use in the              |
|                                                 |                                      | :doc:`../recipes/Imaging`)                             | deconvolution. Choices are MAXCHISQ, MAXTERM0, or MAXBASE.    |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
|  ``CLEAN_DETECT_DIVERGENCE``                    | true                                 | Clean.detectdivergence (:doc:`../calim/solver`)        | Whether to detect that the deconvolution is starting to       |
|                                                 |                                      |                                                        | diverge (in which case it is stopped).                        |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_DETECT_MILD_DIVERGENCE``                | true                                 | Clean.detectmilddivergence (:doc:`../calim/solver`)    | Whether to use the mild form of divergence detection, that    |
|                                                 |                                      |                                                        | should skip to the next major cycle if residuals increase by  |
|                                                 |                                      |                                                        | 10% or more.                                                  |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_NOISEBOXSIZE``                          | 0                                    | Clean.noiseboxsize(:doc:`../calim/solver`)             | Size of the box used to determine the local noise value for   |
|                                                 |                                      |                                                        | setting thresholds (when a "sigma" threshold is given). A     |
|                                                 |                                      |                                                        | value of 0 means the entire image is used.                    |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| **Self-calibration - basics**                   |                                      |                                                        |                                                               |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_SELFCAL``                                  | true                                 | none                                                   | Whether to self-calibrate the science data when imaging.      |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``EXTERNAL_CATALOGUE``                          | ``""``                               | NONE                                                   | If provided, this catalogue is used to generate a model that  |
|                                                 |                                      |                                                        | is then used to provide phase-only calibration prior to the   |
|                                                 |                                      |                                                        | first round of imaging. Special values of this are "racs_mid" |
|                                                 |                                      |                                                        | or "racs_low", to search those whole-sky catalogues through   |
|                                                 |                                      |                                                        | the online Sky Model Service interface in                     |
|                                                 |                                      |                                                        | :doc:`../calim/cmodel`.                                       |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_METHOD``                              | Cmodel                               | none                                                   | How to do the self-calibration. There are three options:      |
|                                                 |                                      |                                                        | "Cmodel" means create a model image from the                  |
|                                                 |                                      |                                                        | source-finding results; "Components" means use the            |
|                                                 |                                      |                                                        | detected components directly through a parset (created by     |
|                                                 |                                      |                                                        | Selavy); "CleanModel" means use the clean model image from the|
|                                                 |                                      |                                                        | most recent imaging as the model for ccalibrator. Anything    |
|                                                 |                                      |                                                        | else will default to "Cmodel".                                |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CMODEL_NEAREST_SELFCAL``                      | true                                 | Cmodel.nearest (:doc:`../calim/cmodel`)                | For ``SELFCAL_METHOD=Cmodel``, whether to use nearest         |
|                                                 |                                      |                                                        | neighbour interpolation for point sources. If set to false,   |
|                                                 |                                      |                                                        | Lanczos5 interpolation will be used.                          |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_SOLVER``                              | SVD                                  | solver (:doc:`../calim/calsolver`)                     | Selection of solver - either "SVD" or "LSQR"                  |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_ALPHA``                               | 0.01                                 | solver (:doc:`../calim/calsolver`)                     | Damping parameter for "LSQR" solver. Ignored for SVD.         |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_NUM_LOOPS``                           | 1                                    | none                                                   | Number of loops of self-calibration.                          |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_INTERVAL``                            | ``"[200,200]"``                      | interval                                               | Interval [sec] over which to solve for self-calibration. Can  |
|                                                 |                                      | (:doc:`../calim/ccalibrator`)                          | be given as an array with different values for each self-cal  |
|                                                 |                                      |                                                        | loop, as for the default, or a single value that applies to   |
|                                                 |                                      |                                                        | each loop.                                                    |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``GAINS_CAL_TABLE``                             | calparameters.selfcal.%t.SB%s.%b.tab | none (directly)                                        | The table name to hold the final gains solution. Once         |
|                                                 |                                      |                                                        | the self-cal loops have completed, the cal table in the       |
|                                                 |                                      |                                                        | final loop is copied to a table of this name in the base      |
|                                                 |                                      |                                                        | directory. This can then be used for the spectral-line        |
|                                                 |                                      |                                                        | imaging if need be. If this is blank, both ``DO_SELFCAL``     |
|                                                 |                                      |                                                        | and ``DO_APPLY_CAL_SL`` will be set to false. The %s wildcard |
|                                                 |                                      |                                                        | will be resolved into the scehduling block ID, %t will be     |
|                                                 |                                      |                                                        | replaced with the "target", or scheduling block alise, and the|
|                                                 |                                      |                                                        | %b will be replaced with "FIELD_beamBB", where FIELD is the   |
|                                                 |                                      |                                                        | field id, and BB the (zero-based) beam number.                |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_NORMALISE_GAINS``                     | true                                 | normalisegains                                         | Whether to normalise the amplitudes of the gains to 1,        |
|                                                 |                                      | (:doc:`../calim/ccalibrator`)                          | approximating the phase-only self-calibration approach. Can   |
|                                                 |                                      |                                                        | be given as an array with different values for each self-cal  |
|                                                 |                                      |                                                        | loop (e.g. "[true,true,false]").                              |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_INTERPOLATE_SELFCAL_GAINS``                | false                                | calibrate.interpolatetime                              | Whether to interpolate the self-cal gains (amp and pha).      |
|                                                 |                                      | (:doc:`../calim/imager`, :doc:`../calim/cimager` and   | If true, interpolated solutions will be used for the interim  |
|                                                 |                                      | :doc:`../calim/ccalapply`)                             | imaging within the selfcal loops, and in the final stages of  |
|                                                 |                                      |                                                        | calibration application to the continuum and spectral-line    |
|                                                 |                                      |                                                        | measurement sets used for the respective cube imaging.        |
|                                                 |                                      |                                                        | Only linear interpolation is allowed.                         |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_REF_ANTENNA``                         | ``""``                               | refantenna (:doc:`../calim/ccalibrator`)               | Reference antenna to use in the calibration. Should be        |
|                                                 |                                      |                                                        | antenna number, 0 - nAnt-1, that matches the antenna          |
|                                                 |                                      |                                                        | numbering in the MS.                                          |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_REF_GAINS``                           | ``""``                               | refgains (:doc:`../calim/ccalibrator`)                 | Reference gains to use in the calibration - something like    |
|                                                 |                                      |                                                        | gain.g11.0.0.                                                 |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_SCALENOISE``                          | false                                | calibrate.scalenoise                                   | Whether the noise estimate will be scaled in accordance       |
|                                                 |                                      | (:doc:`../calim/cimager`)                              | with the applied calibrator factor to achieve proper          |
|                                                 |                                      |                                                        | weighting.                                                    |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CCALIBRATOR_MINUV``                           | 0                                    | MinUV (:doc:`../calim/data_selection`)                 | The minimum UV distance considered in the calibration - used  |
|                                                 |                                      |                                                        | to exclude the short baselines. Can be given as an array with |
|                                                 |                                      |                                                        | different values for each self-cal loop                       |
|                                                 |                                      |                                                        | (e.g. ``"[200,200,0]"``).                                     |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CCALIBRATOR_MAXUV``                           | 0                                    | MaxUV (:doc:`../calim/data_selection`)                 | The maximum UV distance considered in the calibration. Only   |
|                                                 |                                      |                                                        | used if greater than zero. Can be given as an array with      |
|                                                 |                                      |                                                        | different values for each self-cal loop                       |
|                                                 |                                      |                                                        | (e.g. ``"[200,200,0]"``).                                     |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_KEEP_IMAGES``                         | true                                 | none                                                   | Should we keep the images from the intermediate selfcal       |
|                                                 |                                      |                                                        | loops?                                                        |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``MOSAIC_SELFCAL_LOOPS``                        | false                                | none                                                   | Should we make full-field mosaics for each loop of the        |
|                                                 |                                      |                                                        | self-calibration? This is done for each field separately.     |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_MASK_CLEAN_MODEL``                    | false                                | none                                                   | If true, a copy of the clean model image is made to be used   |
|                                                 |                                      |                                                        | for calibration, and it is then masked to remove pixels below |
|                                                 |                                      |                                                        | a certain threshold - to avoid negative or weak components.   |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_MASK_CLEAN_MODEL_THRESHOLD``          | 0.001                                | none                                                   | The threshold below which pixels are masked in the modified   |
|                                                 |                                      |                                                        | clean model image. Units are given by the next parameter.     |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_MASK_CLEAN_MODEL_THRESHOLD_TYPE``     | absolute                             | none                                                   | The units of the threshold: 'absolute' means the native image |
|                                                 |                                      |                                                        | flux values (typically Jy/pixel); 'sigmaBased' means the      |
|                                                 |                                      |                                                        | threshold is in multiples of the image noise.                 |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| **Self-calibration - source-finding**           |                                      |                                                        |                                                               |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_SELAVY_THRESHOLD``                    | 8                                    | snrCut                                                 | SNR threshold for detection with Selavy in determining        |
|                                                 |                                      | (:doc:`../analysis/selavy`)                            | selfcal sources. Can be given as an array with different      |
|                                                 |                                      |                                                        | values for each self-cal loop (e.g. ``"[15,10,8]"``).         |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_SELAVY_NSUBX``                        | 6                                    | nsubx                                                  | Division of image in x-direction for source-finding in        |
|                                                 |                                      | (:doc:`../analysis/selavy`)                            | selfcal.                                                      |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_SELAVY_NSUBY``                        | 3                                    | nsuby                                                  | Division of image in y-direction for source-finding in        |
|                                                 |                                      | (:doc:`../analysis/selavy`)                            | selfcal.                                                      |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_SELAVY_GAUSSIANS_FROM_GUESS``         | true                                 | Selavy.Fitter.numGaussFromGuess                        | Whether to fit the number of Gaussians given by the initial   |
|                                                 |                                      | (:doc:`../analysis/postprocessing`)                    | estimate (true), or to only fit a fixed number (false). The   |
|                                                 |                                      |                                                        | number is given by ``SELFCAL_SELAVY_NUM_GAUSSIANS``.          |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_SELAVY_NUM_GAUSSIANS``                | 1                                    | Selavy.Fitter.maxNumGauss                              | The number of Gaussians to fit to each island when            |
|                                                 |                                      | (:doc:`../analysis/postprocessing`)                    | ``SELFCAL_SELAVY_GAUSSIANS_FROM_GUESS=false``.                |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_SELAVY_FIT_TYPE``                     | full                                 | Selavy.Fitter.fitTypes                                 | The type of fit to be used in the Selavy job. The possible    |
|                                                 |                                      | (:doc:`../analysis/postprocessing`)                    | options are 'full', 'psf', 'shape', or 'height'.              |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_SELAVY_WEIGHTSCUT``                   | 0.95                                 | Selavy.Weights.weightsCutoff                           | Pixels with weight less than this fraction of the peak        |
|                                                 |                                      | (:doc:`../analysis/thresholds`)                        | weight will not be considered by the source-finding. If       |
|                                                 |                                      |                                                        | the value is negative, or more than one, no consideration     |
|                                                 |                                      |                                                        | of the weight is made.                                        |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_SELAVY_SPECTRAL_INDEX_THRESHOLD``     | ""                                   | spectralTerms.threshold                                | Threshold applied to component peak fluxes in determining     |
|                                                 |                                      | (:doc:`../analysis/postprocessing`)                    | which have a spectral index (and curvature) value reported    |
|                                                 |                                      |                                                        | in the component catalogue. Not used if left blank. Takes     |
|                                                 |                                      |                                                        | precedence over                                               |
|                                                 |                                      |                                                        | ``SELFCAL_SELAVY_SPECTRAL_INDEX_THRESHOLD_SNR``.              |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_SELAVY_SPECTRAL_INDEX_THRESHOLD_SNR`` | 50.                                  | spectralTerms.thresholdSNR                             | Threshold applied to component peak signal-to-noise values    |
|                                                 |                                      | (:doc:`../analysis/postprocessing`)                    | in determining which have a spectral index (and curvature)    |
|                                                 |                                      |                                                        | value reported in the component catalogue. Not used if left   |
|                                                 |                                      |                                                        | blank.                                                        |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_COMPONENT_SNR_LIMIT``                 | 10                                   | Used to create Cmodel.flux_limit                       | The signal-to-noise level used to set the flux limit for      |
|                                                 |                                      | (:doc:`../calim/cmodel`)                               | components that are used by Cmodel. The image noise values    |
|                                                 |                                      |                                                        | reported for all components are averaged, then multiplied by  |
|                                                 |                                      |                                                        | this value to form the Cmodel flux limit. If left blank       |
|                                                 |                                      |                                                        | (``""``), the flux limit is determined by                     |
|                                                 |                                      |                                                        | ``SELFCAL_MODEL_FLUX_LIMIT``.                                 |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SELFCAL_MODEL_FLUX_LIMIT``                    | 10uJy                                | Cmodel.flux_limit (:doc:`../calim/cmodel`)             | The minimum integrated flux for components to be included in  |
|                                                 |                                      |                                                        | the model used for self-calibration.                          |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_POSITION_OFFSET``                          | false                                | none                                                   | Whether to add a fixed RA & Dec offset to the positions of    |
|                                                 |                                      |                                                        | sources in the final self-calibration catalogue (prior to it  |
|                                                 |                                      |                                                        | being used to calibrate the data). This has been implemented  |
|                                                 |                                      |                                                        | to help with commissioning - do not use unless you understand |
|                                                 |                                      |                                                        | what it is doing! This makes use of the script                |
|                                                 |                                      |                                                        | *legacy/fix_position_offsets.py*.                             |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``RA_POSITION_OFFSET``                          | 0                                    | none                                                   | The offset in position in the RA direction, in arcsec. This is|
|                                                 |                                      |                                                        | taken from the **offset_pipeline_params.txt** file produced by|
|                                                 |                                      |                                                        | the continuum validation script, where the sense of the offset|
|                                                 |                                      |                                                        | is **REFERENCE-ASKAP**.                                       |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DEC_POSITION_OFFSET``                         | 0                                    | none                                                   | The offset in position in the DEC direction, in arcsec. This  |
|                                                 |                                      |                                                        | is taken from the **offset_pipeline_params.txt** file produced|
|                                                 |                                      |                                                        | by the continuum validation script, where the sense of the    |
|                                                 |                                      |                                                        | offset is **REFERENCE-ASKAP**.                                |
+-------------------------------------------------+--------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+


Applying the gains and leakage calibration
------------------------------------------

This table shows the parameters relevant to the jobs that apply the self-cal gains to the averaged dataset, and then the leakages to the gain-refined measurement sets. This is done before the continuum-polarisation imaging and the continuum-cube imaging.


+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| Variable                                   | Default                             | Parset equivalent                                      | Description                                                   |
+============================================+=====================================+========================================================+===============================================================+
| **Application of gains calibration**       |                                     |                                                        |                                                               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_APPLY_CAL_CONT``                      | true                                | none                                                   | Whether to apply the calibration to the averaged              |
|                                            |                                     |                                                        | ("continuum") dataset.                                        |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``JOB_TIME_CONT_APPLYCAL``                 | ``JOB_TIME_DEFAULT`` (24:00:00)     | none                                                   | Time request for applying the calibration                     |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``KEEP_RAW_AV_MS``                         | true                                | none                                                   | Whether to make a copy of the averaged MS before applying     |
|                                            |                                     |                                                        | the gains calibration (true), or to just overwrite with       |
|                                            |                                     |                                                        | the calibrated data (false).                                  |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_APPLY_LEAKAGE``                       | false                               | none                                                   | Whether to apply the leakage calibration to the averaged and  |
|                                            |                                     |                                                        | self-calibrated ("continuum") dataset derived using:          |
|                                            |                                     |                                                        | ``DO_LEAKAGE_CAL_CONT=true``                                  |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+

Additionally, instrumental leakage of Stokes-I into other Stokes parameters in off-axis
regions of the beams can be corrected during mosaicking. For more details, see the pipeline documentation
on linear mosaicking at :doc:`ScienceFieldMosaicking` and code documentation at :doc:`../calim/linmos`.

Continuum Polarisation imaging
------------------------------

Once the gains have been applied and/or leakages have been corrected, MFS imaging
may be done in a selected set of polarisations - use ``DO_CONTPOL_IMAGING=true`` to
turn this mode on.

If "I" is given as one of them, it is not re-imaged, as we have an
image from the self-calibration routine. The cleaning parameters are
given separately here (as thresholds may need to be different),
although gridding and preconditioning are assumed to be the same. Many
of these parameters can be given as arrays, with different values for
each polarisation. If a single value is given, that applies to all
polarisations.

+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| Variable                                   | Default                             | Parset equivalent                                      | Description                                                   |
+============================================+=====================================+========================================================+===============================================================+
| **Imaging parameters**                     |                                     |                                                        |                                                               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_CONTPOL_IMAGING``                     | false                               | none                                                   | Whether to create continuum polarisation images               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CONTIMG_POLARISATIONS``                  | ``"I,V"``                           | ImageName.polarisation                                 | Comma separated list of Polarisations to be imaged            |
|                                            |                                     | (:doc:`../calim/cimager`)                              | Stokes-I is ignored here, since it is imaged in the           |
|                                            |                                     |                                                        | self-cal loop                                                 |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CIMAGER_CONTPOL_MINUV``                  | 0                                   | MinUV (:doc:`../calim/data_selection`)                 | The minimum UV distance considered in the imaging - used to   |
|                                            |                                     |                                                        | exclude the short baselines. Can be given as an array with    |
|                                            |                                     |                                                        | different values for each pol loop                            |
|                                            |                                     |                                                        | (e.g. ``"[200,200,0]"``).                                     |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CIMAGER_CONTPOL_MAXUV``                  | 0                                   | MaxUV (:doc:`../calim/data_selection`)                 | The maximum UV distance considered in the imaging. Only used  |
|                                            |                                     |                                                        | if greater than zero. Can be given as an array with different |
|                                            |                                     |                                                        | values for each pol loop (e.g. ``"[200,200,0]"``).            |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| **Cleaning parameters**                    |                                     |                                                        |                                                               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTPOL_ALGORITHM``                |  BasisfunctionMFS                   | Clean.algorithm                                        | The name(s) of clean algorithm(s) to use.                     |
|                                            |                                     | (:doc:`../calim/solver`)                               | To use different algorithms in different selfcal cycles, use: |
|                                            |                                     |                                                        | ``CLEAN_CONTPOL_ALGORITHM="Hogbom,BasisfunctionMFS"``         |
|                                            |                                     |                                                        | If the number of comma-separated algorithms is less than      |
|                                            |                                     |                                                        | the number of pol specified, the first algorithm specified    |
|                                            |                                     |                                                        | will be used for ALL pol loops.                               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTPOL_MINORCYCLE_NITER``         | ``"800"``                           | Clean.niter                                            | The number of iterations for the minor cycle clean. Can be    |
|                                            |                                     | (:doc:`../calim/solver`)                               | varied for each pol. (e.g. ``"[200,800,1000]"``)              |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTPOL_GAIN``                     | 0.2                                 | Clean.gain                                             | The loop gain (fraction of peak subtracted per minor cycle).  |
|                                            |                                     | (:doc:`../calim/solver`)                               | Can be varied for each pol                                    |
|                                            |                                     |                                                        | cycle. (e.g. ``"[0.1,0.2,0.1]"``)                             |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTPOL_PSFWIDTH``                 | 256                                 | Clean.psfwidth                                         | The width of the psf patch used in the minor cycle. Can be    |
|                                            |                                     | (:doc:`../calim/solver`)                               | varied for each pol (e.g. ``"[256,512,4096]"``)               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTPOL_SCALES``                   | ``"[0,3,10]"``                      | Clean.scales                                           | Set of scales (in pixels) to use with the multi-scale clean.  |
|                                            |                                     | (:doc:`../calim/solver`)                               | Can be varied for each specified polarisation (e.g. ``"[0] ;  |
|                                            |                                     |                                                        | [0,10]"``) Notice the delimiter ``" ; "`` and the spaces      |
|                                            |                                     |                                                        | around it.                                                    |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTPOL_THRESHOLD_MINORCYCLE``     | ``"[30%, 5.0sigma, 0.5sigma]"``     | threshold.minorcycle                                   | Threshold for the minor cycle loop. Can be varied for each    |
|                                            |                                     | (:doc:`../calim/cimager`)                              | polarisation. (e.g. ``"[30%,1.8mJy,0.03mJy] ;                 |
|                                            |                                     | (:doc:`../calim/solver`)                               | [20%,0.5mJy,0.03mJy]"``) Notice the delimiter ``" ; "`` and   |
|                                            |                                     |                                                        | the spaces around it.                                         |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTPOL_THRESHOLD_MAJORCYCLE``     | ``"0.035mJy"``                      | threshold.majorcycle                                   | The target peak residual. Major cycles stop if this is        |
|                                            |                                     | (:doc:`../calim/cimager`)                              | reached. A negative number ensures all major cycles requested |
|                                            |                                     | (:doc:`../calim/solver`)                               | are done. Can be given as an array with different values for  |
|                                            |                                     |                                                        | each pol loop (e.g. ``"[3mJy,1mJy,-1mJy]"``).                 |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTPOL_NUM_MAJORCYCLES``          | ``"3"``                             | ncycles                                                | Number of major cycles. Can be given as an array with         |
|                                            |                                     | (:doc:`../calim/cimager`)                              | different values for each pol loop (e.g. ``"[2,4,6]"``).      |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTPOL_WRITE_AT_MAJOR_CYCLE``     | false                               | Images.writeAtMajorCycle                               | If true, the intermediate images will be written (with a      |
|                                            |                                     | (:doc:`../calim/cimager`)                              | .cycle suffix) after the end of each major cycle.             |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTPOL_SOLUTIONTYPE``             | MAXBASE                             | Clean.solutiontype (see discussion at                  | The type of peak finding algorithm to use in the              |
|                                            |                                     | :doc:`../recipes/Imaging`)                             | deconvolution. Choices are MAXCHISQ, MAXTERM0, or MAXBASE.    |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
|  ``CLEAN_CONTPOL_READ_SCALEMASK``          | ``""``                              | Clean.readscalemask (:doc:`../calim/solver`)           | If not blank, this is the name of the scalemask image. This   |
|                                            |                                     |                                                        | functionality needs further development and so users should   |
|                                            |                                     |                                                        | use this at their own risk!                                   |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTPOL_NOISEBOXSIZE``             | 0                                   | Clean.noiseboxsize(:doc:`../calim/solver`)             | Size of the box used to determine the local noise value for   |
|                                            |                                     |                                                        | setting thresholds (when a "sigma" threshold is given). A     |
|                                            |                                     |                                                        | value of 0 means the entire image is used.                    |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+


Optional Image products
-----------------------

Imager allows users to specify whether or not to write optional output
products such as the residual, weights, natural psf, preconditioned psf etc. images.
These are aimed at minimising disk I/O and occupancy where possible.

Note that information on weights is required by the linear mosaicking applications: linmos
and linmos-mpi. For non A-Projection gridders like WProject, where the weights value
across the image extent is constant, imager can write out the weight value
into an ascii text file that linmos can make use of. This can be specified using
``WRITE_WEIGHTS_LOG_CONT`` parameter for the continuum imaging case.

For A-project gridders, or when snapshot imaging is turned on, one will have to
necessarily write out the weights images for use in linmos. An exception will be
thrown by processASKAP.sh if this condition is violated.

More details on writing these optional image products are discussed in the sections for
continuum and continuum cube imaging below. See also :doc:`ScienceFieldSpectralLineImaging`
and :doc:`ScienceFieldMosaicking`.

+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| Variable                                   | Default                             | Parset equivalent                                      | Description                                                   |
+============================================+=====================================+========================================================+===============================================================+
| **Optional outputs**                       |                                     |                                                        |                                                               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_RESIDUAL_CONT``                    | true                                | write.residualimage                                    | Whether to write the residual image                           |
|                                            |                                     | (:doc:`../calim/imager`)                               |                                                               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_PSF_RAW_CONT``                     | false                               | write.psfrawimage                                      | Whether to write the naturally weighted psf image             |
|                                            |                                     | (:doc:`../calim/imager`)                               |                                                               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_PSF_IMAGE_CONT``                   | false                               | write.psfimage                                         | Whether to write the preconditioned psf image                 |
|                                            |                                     | (:doc:`../calim/imager`)                               |                                                               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_WEIGHTS_IMAGE_CONT``               | false                               | write.weightsimage                                     | Whether to write the weights image                            |
|                                            |                                     | (:doc:`../calim/imager`)                               |                                                               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_WEIGHTS_LOG_CONT``                 | true                                | write.weightslog                                       | Option to write the constant weight into an ascii text file   |
|                                            |                                     | (:doc:`../calim/imager`)                               |                                                               |
|                                            |                                     |                                                        | Note 1: Should not be used with A-project gridders.           |
|                                            |                                     |                                                        |                                                               |
|                                            |                                     |                                                        | Note 2: For snapshot imaging, this option is internally forced|
|                                            |                                     |                                                        | to "false" by the pipeline scripts. Set                       |
|                                            |                                     |                                                        | ``WRITE_WEIGHTS_IMAGE_CONT=true`` in this case.               |
|                                            |                                     |                                                        |                                                               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_MODEL_IMAGE_CONT``                 | true                                | write.modelimage                                       | Whether to write the model image                              |
|                                            |                                     | (:doc:`../calim/imager`)                               |                                                               |
|                                            |                                     |                                                        | Note: The pipeline ensures that intermediate model images get |
|                                            |                                     |                                                        | generated if required by selfcal jobs.                        |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_MASK_IMAGE_CONT``                  | false                               | write.maskimage                                        | Whether to write the mask image (continuum only)              |
|                                            |                                     | (:doc:`../calim/imager`)                               |                                                               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_SCALEMASK_IMAGE_CONT``             | false                               | write.scalemask                                        | Whether to write the scalemask image (continuum only)         |
|                                            |                                     | (:doc:`../calim/imager`)                               |                                                               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_SENSITIVITY_IMAGE_CONT``           | false                               | write.sensitivityimage                                 | Whether to write sensitivity image (continuum only)           |
|                                            |                                     | (:doc:`../calim/imager`)                               |                                                               |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_FIRST_RESTORE_CONT``               | true                                | write.firstrestore                                     | Whether to write the first restored image when alt-restored   |
|                                            |                                     | (:doc:`../calim/imager`)                               | images are requested (continuum only)                         |
+--------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+



Continuum cube imaging
----------------------

Once the gains have been applied to the averaged dataset, it can be
imaged as "continuum cubes" in a variety of polarisations - use
``DO_CONTCUBE_IMAGING=true`` to turn this mode on.

This table lists all relevant parameters that are different to the MFS
imaging. Some parameters, notably gridding and preconditioning, are
the same. There are also parameters related to the use of
:doc:`../calim/imager`.


+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| Variable                                       | Default                             | Parset equivalent                                      | Description                                                   |
+================================================+=====================================+========================================================+===============================================================+
| **Imaging parameters**                         |                                     |                                                        |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_CONTCUBE_IMAGING``                        | false                               | none                                                   | Whether to create continuum cubes                             |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``JOB_TIME_CONTCUBE_IMAGE``                    | ``JOB_TIME_DEFAULT`` (24:00:00)     | none                                                   | Time request for individual continuum cube jobs               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``IMAGE_BASE_CONTCUBE``                        | i.%t.SB%s.contcube                  | Helps form Images.name (:doc:`../calim/imager`)        | Base name for the continuum cubes. It should include "i.", as |
|                                                |                                     |                                                        | the actual base name will include the correct polarisation    |
|                                                |                                     |                                                        | ('I' will produce i.contcube, Q will produce q.contcube and   |
|                                                |                                     |                                                        | so on).  The %s wildcard will be resolved into the scheduling |
|                                                |                                     |                                                        | block ID, and %t will be replaced by the "target" or          |
|                                                |                                     |                                                        | scheduling block alias.                                       |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``NUM_PIXELS_CONTCUBE``                        | 4096                                | Images.shape (:doc:`../calim/imager`)                  | Number of pixels on the spatial dimension for the continuum   |
|                                                |                                     |                                                        | cubes.                                                        |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CELLSIZE_CONTCUBE``                          | ``""``                              | Images.cellsize (:doc:`../calim/imager`)               | Angular size of spatial pixels for the continuum cubes. If    |
|                                                |                                     |                                                        | not provided, it defaults to the value of ``CELLSIZE_CONT``   |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CONTCUBE_POLARISATIONS``                     | ``"I"``                             | Images.polarisation (:doc:`../calim/imager`)           | List of polarisations to create cubes for. This should be a   |
|                                                |                                     |                                                        | comma-separated list of (upper-case) polarisations. Separate  |
|                                                |                                     |                                                        | jobs will be launched for each polarisation given.            |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``REST_FREQUENCY_CONTCUBE``                    | ``""``                              | Images.restFrequency (:doc:`../calim/imager`)          | Rest frequency to be written to the continuum cube. If left   |
|                                                |                                     |                                                        | blank, no rest frequency is written.                          |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CONTCUBE_IMAGE_MAXUV``                       | 0                                   | MaxUV                                                  | A maximum UV distance (in metres) to apply in the data        |
|                                                |                                     | (:doc:`../calim/data_selection`)                       | selection step. Only used if a positive value is applied.     |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CONTCUBE_IMAGE_MINUV``                       | 0                                   | MinUV                                                  | A minimum UV distance (in metres) to apply in the data        |
|                                                |                                     | (:doc:`../calim/data_selection`)                       | selection step. Only used if a positive value is applied.     |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_NYQUIST_GRIDDING_CONTCUBE``               | true                                | Images.nyquistgridding                                 | Whether to turn on Nyquist gridding.                          |
|                                                |                                     | (:doc:`../calim/imager`)                               |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``RESTORING_BEAM_CONTCUBE``                    | fit                                 | restore.beam                                           | Restoring beam to use: 'fit' will fit the PSF in each channel |
|                                                |                                     | (:doc:`../calim/imager` & :doc:`../calim/solver`)      | separately to determine the appropriate beam for that         |
|                                                |                                     |                                                        | channel, else give a size (such as                            |
|                                                |                                     |                                                        | ``"[30arcsec, 30arcsec, 0deg]”``).                            |
|                                                |                                     |                                                        | NB: If ``PRECONDITIONER_GAUSS_TAPER_IS_PSF=true``             |
|                                                |                                     |                                                        | the restoring beams will be derived such that the output      |
|                                                |                                     |                                                        | image planes have the fixed specified resolution.             |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``RESTORING_BEAM_CUTOFF_CONTCUBE``             | 0.5                                 | restore.beam.cutoff                                    | Cutoff value used in determining the support for the fitting  |
|                                                |                                     | (:doc:`../calim/imager`)                               | (ie. the rectangular area given to the fitting routine).      |
|                                                |                                     |                                                        | Value is a fraction of the peak.                              |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``RESTORING_BEAM_CONTCUBE_REFERENCE``          | mid                                 | restore.beamReference (:doc:`../calim/imager`)         | Which channel to use as the reference when writing the        |
|                                                |                                     |                                                        | restoring beam to the image cube. Can be an integer as the    |
|                                                |                                     |                                                        | channel number (0-based), or one of 'mid' (the middle         |
|                                                |                                     |                                                        | channel), 'first' or 'last'                                   |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``NUM_CORES_CONTCUBE_SCI``                     | ``""``                              | none                                                   | Total number of cores to use for the continuum cube job. If   |
|                                                |                                     |                                                        | left blank, this will be chosen to match the number of        |
|                                                |                                     |                                                        | channels (taking into account ``NCHAN_PER_CORE_CONTCUBE`` if  |
|                                                |                                     |                                                        | necessary), plus an additional core for the master process.   |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``NCHAN_PER_CORE_CONTCUBE``                    | 3 (galaxy), 4 (petrichor), 6        | nchanpercore (:doc:`../calim/imager`)                  | If imager (:doc:`../calim/imager`) is used, this determines   |
|                                                | (setonix)                           |                                                        | how many channels each *worker* will process.                 |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CORES_PER_NODE_CONTCUBE_IMAGING``            | 8 (galaxy), 48 (petrichor), 73      | none                                                   | How many of the cores on each node to use.                    |
|                                                | (setonix)                           |                                                        |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| **Preconditioning**                            |                                     |                                                        |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_LIST_CONTCUBE``               | ``""``                              | preconditioner.Names (:doc:`../calim/solver`)          | List of preconditioners to apply. If left blank, the          |
|                                                |                                     |                                                        | preconditioning used for the continuum imaging is used        |
|                                                |                                     |                                                        | instead, and any values given in the config file for the      |
|                                                |                                     |                                                        | subsequent parameters are ignored.                            |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_CONTCUBE_GAUSS_TAPER``        | ``"[10arcsec, 10arcsec, 0deg]"``    | preconditioner.GaussianTaper                           | Size of the Gaussian taper - either single value (for circular|
|                                                |                                     | (:doc:`../calim/solver`)                               | taper) or 3 values giving an elliptical size.                 |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_CONTCUBE_GAUSS_TAPER_IS_PSF`` | ``"false"``                         | preconditioner.GaussianTaper.isPsfSize                 | Decide if rather than applying the specified taper, the taper |
|                                                |                                     | (:doc:`../calim/solver`)                               | should be adjusted to ensure that the output image planes have|
|                                                |                                     |                                                        | the specified resolution.                                     |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
|  ``PRECONDITIONER_CONTCUBE_GAUSS_TAPER_TOL``   | 0.005                               | preconditioner.GaussianTaper.tolerance                 | Fractional tolerance for the fitted beam size when            |
|                                                |                                     | (:doc:`../calim/solver`)                               | ``PRECONDITIONER_CONTCUBE_GAUSS_TAPER_IS_PSF = true``         |
|                                                |                                     |                                                        | The default is set to 0.5%                                    |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_CONTCUBE_WIENER_ROBUSTNESS``  | -0.5                                | preconditioner.Wiener.robustness                       | Robustness value for the Wiener filter.                       |
|                                                |                                     | (:doc:`../calim/solver`)                               |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_CONTCUBE_WIENER_TAPER``       | ``""``                              | preconditioner.Wiener.taper                            | Size of gaussian taper applied in image domain to Wiener      |
|                                                |                                     | (:doc:`../calim/solver`)                               | filter. Ignored if blank (ie. ``""``).                        |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``TRADITIONAL_WEIGHT_PARAM_CONTCUBE_LIST``     | ``""``                              | uvweight (:doc:`../calim/imager`)                      | List of parameters for application of traditional weights.    |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``TRADITIONAL_WEIGHT_CONTCUBE_ROBUSTNESS``     | -0.5                                | uvweight.robustness (:doc:`../calim/imager`)           | The value of robustness. Only used if there is Robust among   |
|                                                |                                     |                                                        | the list of parameters given by                               |
|                                                |                                     |                                                        | ``TRADITIONAL_WEIGHT_PARAM_CONTCUBE_LIST``.                   |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| **Continuum cube cleaning**                    |                                     |                                                        | Different cleaning parameters used for the continuum cubes    |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``SOLVER_CONTCUBE``                            | Clean                               | solver                                                 | Which solver to use. You will mostly want to leave this as    |
|                                                |                                     | (:doc:`../calim/imager`)                               | 'Clean', but there is a 'Dirty' solver available.             |
|                                                |                                     | (:doc:`../calim/solver`)                               |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTCUBE_ALGORITHM``                   | BasisfunctionMFS                    | Clean.algorithm (:doc:`../calim/solver`)               | The name of the clean algorithm to use.                       |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_MFS_STARTING_MODEL_CONTCUBE``             | false                               | none                                                   | Whether to use the MFS model image as the starting point for  |
|                                                |                                     |                                                        | the cleaning of the continuum cube.                           |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTCUBE_MINORCYCLE_NITER``            | 600                                 | Clean.niter (:doc:`../calim/solver`)                   | The number of iterations for the minor cycle clean.           |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTCUBE_GAIN``                        | 0.2                                 | Clean.gain (:doc:`../calim/solver`)                    | The loop gain (fraction of peak subtracted per minor cycle).  |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTCUBE_PSFWIDTH``                    | 256                                 | Clean.psfwidth (:doc:`../calim/solver`)                | The width of the psf patch used in the minor cycle.           |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTCUBE_SCALES``                      | ``"[0,3,10]"``                      | Clean.scales (:doc:`../calim/solver`)                  | Set of scales (in pixels) to use with the multi-scale clean.  |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTCUBE_THRESHOLD_MINORCYCLE``        | ``"[40%, 5.0sigma, 0.5sigma]"``     | threshold.minorcycle                                   | Threshold for the minor cycle loop.                           |
|                                                |                                     | (:doc:`../calim/solver`)                               |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTCUBE_THRESHOLD_MAJORCYCLE``        | 0.06mJy                             | threshold.majorcycle (:doc:`../calim/solver`)          | The target peak residual. Major cycles stop if this is        |
|                                                |                                     |                                                        | reached. A negative number ensures all major cycles requested |
|                                                |                                     |                                                        | are done.                                                     |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTCUBE_NUM_MAJORCYCLES``             | 3                                   | ncycles (:doc:`../calim/imager`)                       | Number of major cycles.                                       |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTCUBE_WRITE_AT_MAJOR_CYCLE``        | false                               | Images.writeAtMajorCycle (:doc:`../calim/imager`)      | If true, the intermediate images will be written (with a      |
|                                                |                                     |                                                        | .cycle suffix) after the end of each major cycle.             |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTCUBE_SOLUTIONTYPE``                | MAXBASE                             | Clean.solutiontype (see discussion at                  | The type of peak finding algorithm to use in the              |
|                                                |                                     | :doc:`../recipes/Imaging`)                             | deconvolution. Choices are MAXCHISQ, MAXTERM0, or MAXBASE.    |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
|  ``CLEAN_CONTCUBE_DETECT_DIVERGENCE``          | true                                | Clean.detectdivergence (:doc:`../calim/solver`)        | Whether to detect that the deconvolution is starting to       |
|                                                |                                     |                                                        | diverge (in which case it is stopped).                        |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTCUBE_DETECT_MILD_DIVERGENCE``      | true                                | Clean.detectmilddivergence (:doc:`../calim/solver`)    | Whether to use the mild form of divergence detection, that    |
|                                                |                                     |                                                        | should skip to the next major cycle if residuals increase by  |
|                                                |                                     |                                                        | 10% or more.                                                  |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_CONTCUBE_NOISEBOXSIZE``                | 0                                   | Clean.noiseboxsize(:doc:`../calim/solver`)             | Size of the box used to determine the local noise value for   |
|                                                |                                     |                                                        | setting thresholds (when a "sigma" threshold is given). A     |
|                                                |                                     |                                                        | value of 0 means the entire image is used.                    |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
|  ``CLEAN_CONTCUBE_READ_SCALEMASK``             | ``""``                              | Clean.readscalemask (:doc:`../calim/solver`)           | If not blank, this is the name of the scalemask image. This   |
|                                                |                                     |                                                        | functionality needs further development and so users should   |
|                                                |                                     |                                                        | use this at their own risk!                                   |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| **New imager parameters**                      |                                     |                                                        |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_ALT_IMAGER_CONTCUBE``                     | ``""``                              | none                                                   | If true, the continuum cube imaging is done by imager         |
|                                                |                                     |                                                        | (:doc:`../calim/imager`). If false, it is done by cimager     |
|                                                |                                     |                                                        | (:doc:`../calim/cimager`). When true, the following           |
|                                                |                                     |                                                        | parameters are used. If left blank (the default), the value   |
|                                                |                                     |                                                        | is given by the overall parameter ``DO_ALT_IMAGER``.          |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``NCHAN_PER_CORE``                             | 12 (galaxy/petrichor), 24 (setonix) | nchanpercore                                           | The number of channels each core will process.                |
|                                                |                                     | (:doc:`../calim/imager`)                               |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``NUM_SPECTRAL_WRITERS_CONTCUBE``              | ``""``                              | nwriters (:doc:`../calim/imager`)                      | The number of writers used by imager. Unless                  |
|                                                |                                     |                                                        | ``ALT_IMAGER_SINGLE_FILE_CONTCUBE=true``, this will equate to |
|                                                |                                     |                                                        | the number of distinct spectral cubes produced.In the case of |
|                                                |                                     |                                                        | multiple cubes, each will be a sub-band of the full           |
|                                                |                                     |                                                        | bandwidth. No combination of the sub-cubes is currently       |
|                                                |                                     |                                                        | done. The number of writers will be reduced to the number of  |
|                                                |                                     |                                                        | workers in the job if necessary. If a single image is         |
|                                                |                                     |                                                        | produced, the default is to have the same number of writers as|
|                                                |                                     |                                                        | workers.                                                      |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``ALT_IMAGER_SINGLE_FILE_CONTCUBE``            | true                                | singleoutputfile                                       | Whether to write a single cube, even with multiple writers    |
|                                                |                                     | (:doc:`../calim/imager`)                               | (ie. ``NUM_SPECTRAL_WRITERS_CONTCUBE>1``). Only works when    |
|                                                |                                     |                                                        | ``IMAGETYPE_SPECTRAL=fits``                                   |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``DO_MASK_BAD_CHANNELS``                       | true                                | none                                                   | Whether to mask out bad or blank channels from the continuum  |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``MASK_CHANS_USE_SIG``                         | false                               | none                                                   | Whether to use the "signifcance", or the ratio of the         |
|                                                |                                     |                                                        | 1-percent statistic to the MADFM, to determine the bad        |
|                                                |                                     |                                                        | channels.                                                     |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``MASK_CHANS_SIG_THRESHOLD``                   | 10.                                 | none                                                   | The significance level at which to reject channels.           |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``MASK_CHANS_USE_NOISE``                       | true                                | none                                                   | Whether to mask out bad channels on the basis of the MADFM    |
|                                                |                                     |                                                        | value alone.                                                  |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``MASK_CHANS_NOISE_THRESHOLD``                 | 1000.                               | none                                                   | The value of MADFM (in mJy/beam), above which a channel is    |
|                                                |                                     |                                                        | deemed bad.                                                   |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``MASK_CHANS_BLANK``                           | true                                | none                                                   | Whether to mask out blank channels from the continuum         |
|                                                |                                     |                                                        | cube.                                                         |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| **Optional outputs**                           |                                     |                                                        |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_RESIDUAL_CONTCUBE``                    | true                                | write.residualimage                                    | Whether to write the residual image cube                      |
|                                                |                                     | (:doc:`../calim/imager`)                               |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_PSF_RAW_CONTCUBE``                     | false                               | write.psfrawimage                                      | Whether to write the naturally weighted psf image cube        |
|                                                |                                     | (:doc:`../calim/imager`)                               |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_PSF_IMAGE_CONTCUBE``                   | false                               | write.psfimage                                         | Whether to write the preconditioned psf image cube            |
|                                                |                                     | (:doc:`../calim/imager`)                               |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_WEIGHTS_IMAGE_CONTCUBE``               | false                               | write.weightsimage                                     | Whether to write the weights image cube                       |
|                                                |                                     | (:doc:`../calim/imager`)                               |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_WEIGHTS_LOG_CONTCUBE``                 | true                                | write.weightslog                                       | Option to write the weight spectra into an ascii text file    |
|                                                |                                     | (:doc:`../calim/imager`)                               |                                                               |
|                                                |                                     |                                                        | Note 1: Should not be used with A-project gridders.           |
|                                                |                                     |                                                        |                                                               |
|                                                |                                     |                                                        | Note 2: For snapshot imaging, this option is internally forced|
|                                                |                                     |                                                        | to "false" by the pipeline scripts. Set                       |
|                                                |                                     |                                                        | ``WRITE_WEIGHTS_IMAGE_CONT=true`` in this case.               |
|                                                |                                     |                                                        |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_MODEL_IMAGE_CONTCUBE``                 | true                                | write.modelimage                                       | Whether to write the model image cube                         |
|                                                |                                     | (:doc:`../calim/imager`)                               |                                                               |
|                                                |                                     |                                                        | Note: The pipeline ensures that intermediate model images get |
|                                                |                                     |                                                        | generated if required by selfcal jobs.                        |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``WRITE_UVGRIDS_CONTCUBE``                     | false                               | write.grids                                            | Whether to write the UV-grids (cubes only)                    |
|                                                |                                     | (:doc:`../calim/imager`)                               |                                                               |
+------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+



Fast / Transient imaging
------------------------

The pipeline presents a transient-imaging workflow, where continuum images are made over intervals shorter than the total observation time. The workflow is summarised as follows:
 
* For a given beam dataset, the deep continuum clean model (from the full continuum imaging) is subtracted using ``ccontsubtract``, to create a coarse-resolution continuum-subtracted MS.
* This MS is imaged sequentially in each of a number of time intervals spanning the full observation duration:

  - The intervals are calculated by the python script ``transientIntervals.py``. This takes a nominated interval length (``TRANSIENT_INTERVAL``, in seconds), and adjusts it to be a whole number of integrations.
  - A series of start and end times are then calculated, which are presented to ``imager`` via the parset parameter ``Cimager.TimeRange`` (see :doc:`../calim/imager`).
  - Each interval is then imaged in sequence, producing a single restored image for each interval. 

* These images are then given to the *Vaster* module, a containerised set of scripts provided by the VAST Survey Science Team, to perform transient detection. This uses the deep component catalogue for the beam (to determine locations to search), and so the per-beam sourcefinding is activated when transient imaging is turned on.
* This results in a tar.gz file containing the *Vaster* outputs, that can be copied elsewhere for immediate use (a more complete archiving scheme is under development).

This is all done separately for each beam, in a specific transientImaging subdirectory.
  
The imaging is much simpler than the other continuum imaging. Since the dataset has already had the bulk of the continuum emission removed, only a very simple, shallow clean is done. Some parameters are provided to alter the preconditioning or deconvolution (see table below), although it is anticipated the defaults should suffice. The image can be made a different size/shape to the main continuum images, and no MFS imaging is done by default.

+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| Variable                                        | Default                             | Parset equivalent                                      | Description                                                   |
+=================================================+=====================================+========================================================+===============================================================+
| ``DO_TRANSIENT_IMAGING``                        | false                               | none                                                   | Whether to run the transient imaging & detection              |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``TRANSIENT_INTERVAL``                          | 900                                 | none                                                   | Length of time in seconds for a single interval to be imaged. |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| **Imaging parameters**                          |                                     |                                                        |                                                               |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``NUM_PIXELS_TRANSIENT``                        | 3584                                | Images.shape (:doc:`../calim/imager`)                  | Number of pixels on the spatial dimension for the transient   |
|                                                 |                                     |                                                        | images                                                        |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CELLSIZE_TRANSIENT``                          | ``""``                              | Images.cellsize(:doc:`../calim/imager`)                | Angular size of spatial pixels for the transient images. If   |
|                                                 |                                     |                                                        | not provided, it defaults to the value of ``CELLSIZE_CONT``   |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_TRANSIENT_NUM_MAJORCYCLES``             | 1                                   | ncycles (:doc:`../calim/imager`)                       | Number of major cycles                                        |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_TRANSIENT_ALGORITHM``                   | BasisfunctionMFS                    | Clean.algorithm (:doc:`../calim/solver`)               | The name of the clean algorithm to use.                       |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_TRANSIENT_MINORCYCLE_NITER``            | 200                                 | Clean.niter (:doc:`../calim/solver`)                   | The number of iterations for the minor cycle clean.           |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_TRANSIENT_THRESHOLD_MINORCYCLE``        | "[30%, 5sigma, 0.5sigma]"           | threshold.minorcycle (:doc:`../calim/solver`)          | Thresholds for the minor cycle loop                           |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_TRANSIENT_GAIN``                        | 0.2                                 | Clean.gain (:doc:`../calim/solver`)                    | The loop gain (fraction of peak subtracted per minor cycle).  |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_TRANSIENT_PSFWIDTH``                    | 256                                 | Clean.psfwidth (:doc:`../calim/solver`)                | The width of the psf patch used in the minor cycle            |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_TRANSIENT_SCALES``                      | "[0]"                               | Clean.scales (:doc:`../calim/solver`)                  |  Set of scales (in pixels) to use with the multi-scale clean. |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``CLEAN_TRANSIENT_NOISEBOXSIZE``                | 0                                   | Clean.noiseboxsize(:doc:`../calim/solver`)             | Size of the box used to determine the local noise value for   |
|                                                 |                                     |                                                        | setting thresholds (when a "sigma" threshold is given). A     |
|                                                 |                                     |                                                        | value of 0 means the entire image is used.                    |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_TRANSIENT_LIST``               |  "[Wiener]"                         | preconditioner.Names (:doc:`../calim/solver`)          | List of preconditioners to apply.                             |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_TRANSIENT_WIENER_ROBUSTNESS``  | -0.5                                | preconditioner.Wiener.robustness                       | Robustness value for the Wiener filter.                       |
|                                                 |                                     | (:doc:`../calim/solver`)                               |                                                               |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_TRANSIENT_WIENER_TAPER``       | ``""``                              | preconditioner.Wiener.taper (:doc:`../calim/solver`)   | Size of gaussian taper applied in image domain to Wiener      |
|                                                 |                                     |                                                        | filter. Ignored if blank (ie. ``""``).                        |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_TRANSIENT_GAUSS_TAPER``        | "[10arcsec, 10arcsec, 0deg]"        | preconditioner.GaussianTaper (:doc:`../calim/solver`)  | Size of the Gaussian taper (if provided in the preconditioner |
|                                                 |                                     |                                                        | list) - either single value (for circular taper) or 3 values  |
|                                                 |                                     |                                                        | giving an elliptical size.                                    |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_TRANSIENT_GAUSS_TAPER_TOL``    | 0.005                               | preconditioner.GaussianTaper.tolerance                 | Fractional tolerance for the fitted beam size when            |
|                                                 |                                     | (:doc:`../calim/solver`)                               | ``PRECONDITIONER_CONTCUBE_GAUSS_TAPER_IS_PSF = true``.  The   |
|                                                 |                                     |                                                        | default is set to 0.5%                                        |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``PRECONDITIONER_TRANSIENT_GAUSS_TAPER_IS_PSF`` | false                               | preconditioner.GaussianTaper.isPsfSize                 | Decide if rather than applying the specified taper, the taper |
|                                                 |                                     | (:doc:`../calim/solver`)                               | should be adjusted to ensure that the output image planes have|
|                                                 |                                     |                                                        | the specified resolution.                                     |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``TRADITIONAL_WEIGHT_PARAM_TRANSIENT_LIST``     | ``""``                              | uvweight (:doc:`../calim/imager`)                      | List of parameters for application of traditional weights.    |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``TRADITIONAL_WEIGHT_TRANSIENT_ROBUSTNESS``     | -0.5                                | uvweight.robustness (:doc:`../calim/imager`)           | The value of robustness. Only used if there is Robust among   |
|                                                 |                                     |                                                        | the list of parameters given by                               |
|                                                 |                                     |                                                        | ``TRADITIONAL_WEIGHT_PARAM_TRANSIENT_LIST``.                  |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| **Transient Detection**                         |                                     |                                                        |                                                               |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``TRANSIENT_DETECTION_KTYPELIST``               | "chisquare, peak, std"              | none                                                   | Input parameters for Vaster                                   |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``TRANSIENT_DETECTION_G_WIDTH``                 | 4                                   | none                                                   | Input parameter for Vaster                                    |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``TRANSIENT_DETECTION_CHUNK_0``                 | 100                                 | none                                                   | Input parameter for Vaster                                    |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``TRANSIENT_DETECTION_CHUNK_1``                 | 100                                 | none                                                   | Input parameter for Vaster                                    |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| **Data handling**                               |                                     |                                                        |                                                               |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+
| ``TRANSIENT_OUTPUT_DESTINATION``                | ""                                  | none                                                   | Location to copy the output tar.gz files to - should be a     |
|                                                 |                                     |                                                        | directory. Leaving blank will keep the output files in the    |
|                                                 |                                     |                                                        | transientImaging directory.                                   |
+-------------------------------------------------+-------------------------------------+--------------------------------------------------------+---------------------------------------------------------------+

