.. YandaSoft documentation master file, created by
   sphinx-quickstart on Wed Jul 24 17:27:05 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

YandaSoft
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   docs/calim/index.rst
   docs/analysis/index.rst
   docs/utils/index.rst
   docs/tutorials/index.rst
   docs/recipes/index.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
