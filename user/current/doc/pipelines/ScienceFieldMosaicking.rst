User Parameters - Mosaicking
============================

Levels of Mosaicking
--------------------

The default mode of the pipeline is to produce mosaics at several
levels for a given scheduling block. Each field in the measurement set
will have its own mosaic, combining all requested beams for that
field.

There is a choice for how the weighting is done at this step. The
original method was to use
*weighttype=FromPrimaryBeamModel*, which simply applies a primary beam
correction (given that the weights image is essentially flat for
W-projection). The new default, however, is *weighttype=Combined*,
which combines the primary beam weighting with the value from the
weights image, better accounting for differences in effective
integration time or flagging. The former method will produce mosaic
weights images normalised to a peak of 1, while the latter does not
apply any normalisation. The choice between the two is made with the
``LINMOS_SINGLE_FIELD_WEIGHTTYPE`` config parameter.

When the scheduling block has used the *tilesky* mode, the fields in the
measurement set will be of the form *NAME_Tx-yI*, where *x* & *y* give
the offsets in the tiling grid, and *I* is one of A,B,C,D, indicating
the interleaving offset for that tile position. *NAME* is some
identifying text. In this situation, each *x-y* tile will have its own
mosaic, where the mosaics of each interleave position for that tile
are combined.

Finally, the mosaics of all fields and/or tiles are combined to form a
single mosaic image for the scheduling block. This second stage of
mosaicking is not done if there is only a single field in the
measurement set (instead the single field mosaic is copied to the
top-level directory at the completion of the initial mosaicking job).

Additionally, if there are multiple fields in the measurement set, but
they should *not* be mosaicked together (for instance, if they are
quite separate locations on the sky), then you can set
``DO_MOSAIC_FIELDS=false``. Then the individual fields will be
mosaicked but nothing more.

The default behaviour is to create mosaics with the same direction
type and projection as the beam images (which means J2000 equatorial,
in SIN projection). These can be changed by using
``LINMOS_DIRECTION_TYPE`` and ``LINMOS_PROJECTION_TYPE``. For
instance, to create mosaics in Galactic coordinates, one would set
``LINMOS_DIRECTION_TYPE=GALACTIC``. The full lists of possible
direction & projection values are linked from :doc:`../calim/linmos`
(under the ``outputcentre`` parameter).

The mosaicking is done for all image product types -- continuum
images, continuum cubes, and spectral-line cubes. For each type, the
mosaicking is done as separate jobs for different image variants. The
user may provide a list of codes for these images: available codes are
"restored" (restored image), "convrestored" (restored image convolved
to a common resolution across PAF beams), "image" (clean model),
"residual" (residual flux from the clean model), "altrestored"
(restored image made with alternative preconditioning), "contsub"
(continuum-subtracted cube, made with the image-based method). The
parameters, and their defaults, are as follows for the different
types:

* continuum image: ``MOSAIC_IMAGECODE_CONT`` - "restored convrestored altrestored image residual"
* continuum cube: ``MOSAIC_IMAGECODE_CONTCUBE`` - "restored convrestored image residual"
* spectral cube: ``MOSAIC_IMAGECODE_SPECTRAL`` - "restored contsub image residual"

If codes other than those listed are provided, the pipeline will exit
at the start before submitting any jobs.
  
For the continuum imaging case when self-calibration has been used, a
mosaic can be made of each loop of the self-calibration process. This
is done for each field (ie. each interleave position of each tile),
and can be turned on/off with the parameter ``MOSAIC_SELFCAL_LOOPS``.

Common Resolution across beams
------------------------------

Each beam is imaged independently, and will most likely have a
different point-spread-function (PSF). If these images are mosaicked,
the PSF stored in the header of the mosaic is taken from one of them
(indicated by the ``LINMOS_PSF_REF`` parameter), and so may not
accurately reflect the PSF in other parts of the image.

To address this, the beam images may be convolved to a common
resolution. For the continuum images, this is done with a single task
that looks at all the beam images, determines the best
common-resolution PSF, and convolves each image to ensure it has that
resolution. For the continuum cubes, the determination of the PSF and
the convolution are broken up into separate tasks.

If ``DO_MOSAIC_FIELDS`` is true, all beam images in all fields are
looked at together, but if false each field is considered
independently.

For the continuum-cube case, there are two options for how the
frequency-dependence of the PSF is dealt with - either to have a
frequency-independent common PSF (``total``), or to consider each
channel separately (``natural``).

There are also parameters that allow the user to fix the resolution, 
impose a maximum major-axis size on the PSF, or force the output 
PSF to be circular. See the table below.

+--------------------------------------+-----------+-------------------------------------------------------------+
| Variable                             | Default   | Description                                                 |
+======================================+===========+=============================================================+
| ``CONVOLVE_MODE_CUBE``               | "natural" | Two modes are allowed:                                      |
|                                      |           |                                                             |
|                                      |           | ``total`` - convolves all planes of an image cube to the psf|
|                                      |           | common across all beams and frequency channels of the input |
|                                      |           | cubes.                                                      |
|                                      |           |                                                             |
|                                      |           | ``natural`` - convolves a plane of an image cube at a given |
|                                      |           | frequency channel to the psf common across all beams of the |
|                                      |           | input cubes for that channel.                               |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``RENAME_IMAGES``                    | true      | Whether to rename the restored image to "raw" following the |
|                                      |           | convolution.                                                |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``NUM_CORES_CONVOLVE_2D``            | 20        | The number of cores to be used for the 2D convolution job   |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``BMAJ_CONVOLVE``                    | ""        | The target value for the major axis of the PSF. If blank,   |
|                                      |           | this is determined from the input images.                   |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``BMIN_CONVOLVE``                    | ""        | The target value for the minor axis of the PSF. If blank,   |
|                                      |           | this is determined from the input images.                   |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``BPA_CONVOLVE``                     | ""        | The target value for the position angle of the PSF. If      |
|                                      |           | blank, this is determined from the input images.            |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``BMAJ_CUTOFF_CONVOLVE``             | ""        | The imposed maximum value for the major axis of the PSF. If |
|                                      |           | blank, no maximum value is imposed and the input images     |
|                                      |           | completely determine the value.                             |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``BMAJ_CONVOLVE_CONTCUBE``           | ""        | The target value for the major axis of the PSF. If blank,   |
|                                      |           | this is determined from the input image cubes.              |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``BMIN_CONVOLVE_CONTCUBE``           | ""        | The target value for the minor axis of the PSF. If blank,   |
|                                      |           | this is determined from the input image cubes.              |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``BPA_CONVOLVE_CONTCUBE``            | ""        | The target value for the position angle of the PSF. If      |
|                                      |           | blank, this is determined from the input image cubes.       |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``BMAJ_CUTOFF_CONVOLVE_CONTCUBE``    | ""        | The imposed maximum value for the major axis of the PSF. If |
|                                      |           | blank, no maximum value is imposed and the input images     |
|                                      |           | completely determine the value.                             |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``BMAJ_CONVOLVE_ALT``                | ""        | The target value for the major axis of the PSF for the      |
|                                      |           | convolved "alt" (restore preconditioner) images. If blank,  |
|                                      |           | this is determined from the input images.                   |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``BMIN_CONVOLVE_ALT``                | ""        | The target value for the minor axis of the PSF for the      |
|                                      |           | convolved "alt" (restore preconditioner) images. If blank,  |
|                                      |           | this is determined from the input images.                   |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``BPA_CONVOLVE_ALT``                 | ""        | The target value for the position angle of the PSF for the  |
|                                      |           | convolved "alt" (restore preconditioner) images. If         |
|                                      |           | blank, this is determined from the input images.            |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``BMAJ_CUTOFF_CONVOLVE_ALT``         | ""        | The imposed maximum value for the major axis of the PSF for |
|                                      |           | the convolved "alt" (restore preconditioner) images. If     |
|                                      |           | blank, no maximum value is imposed and the input images     |
|                                      |           | completely determine the value.                             |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``FORCE_CIRCULAR_PSF_CONVOLVE``      | false     | If true, the output PSF is forced to be circular.           |
|                                      |           | BMIN is set to BMAJ, and BPA is set to 0.                   |
+--------------------------------------+-----------+-------------------------------------------------------------+
| ``FORCE_CIRCULAR_PSF_CONVOLVE_ALT``  | false     | If true, the output PSF for the convolved alt images is     |
|                                      |           | forced to be circular. BMIN is set to BMAJ, and BPA is set  |
|                                      |           | to 0.                                                       |
+--------------------------------------+-----------+-------------------------------------------------------------+


Beam locations
--------------

The linmos tools require beam offsets to know where to place the
primary beam models. For recent ASKAP scheduling blocks, this is
generated in a self-consistent manner using the command-line
interfaces to the scheduling block and footprint services. This uses
the recorded beam footprint information in the scheduling block parset
to regenerate the central location of each beam.

The services are hosted at the MRO, and in the event of an
interruption to service, the use of these can be bypassed by setting
``USE_CLI=false``. This requires, however, that you have
previously-made metadata files available in the metadata directory (in
particular the footprint file).

Recent observations rely on the **src%d.footprint.rotation** parameter
in the scheduling block parset to determine the reference angle from
which the **pol_axis** parameter gives a field-based rotation. Some
scheduling blocks make use of this parameter yet do not report it in
the parset. If this is the case, you can manually specify the
reference value via the ``FOOTPRINT_ROTATION_REFERENCE`` parameter in the
configuration file. The scheduling block parset value always takes
precendence. 

If you are processing an older SBID, for which the footprint
information is not available in the scheduling block, you must specify
the footprint information yourself using the parameters given
below. It is possible the footprint service does not know about your
footprint in this case - we then fall back to using the legacy script
*footprint.py* (originally developed within the ACES commissioning team), which can accept a broader range of footprint names.

For re-processing of BETA data, you will need to set the ``IS_BETA``
parameter (so that the pipeline does not try to access the scheduling
block service), and provide the beam information using the parameters
below. 

It may be that you are using a non-standard arrangement that
footprint.py doesn’t cover. In this case you need to directly specify
the ``LINMOS_BEAM_OFFSETS`` variable (this is normally set by the
beamArrangements.sh script). Here is an example of how you would do
this (this is for the “diamond” footprint, band 1, PA=0)::
  
  LINMOS_BEAM_OFFSETS="linmos.feeds.beam0 = [-0.000,  0.000]
  linmos.feeds.beam1 = [-0.000,  1.244]
  linmos.feeds.beam2 = [-1.077,  0.622]
  linmos.feeds.beam3 = [-1.077, -0.622]
  linmos.feeds.beam4 = [-0.000, -1.244]
  linmos.feeds.beam5 = [ 1.077, -0.622]
  linmos.feeds.beam6 = [ 1.077,  0.622]
  linmos.feeds.beam7 = [-2.154,  0.000]
  linmos.feeds.beam8 = [ 2.154, -0.000]"

Note that ``LINMOS_BEAM_OFFSETS`` is only needed when all beams have
the same centre position (ie. when
``IMAGE_AT_BEAM_CENTRES=false``). If this is not the case, each beam
image has a different centre, and linmos simply uses that, putting a
primary beam at the centre of each image.

The pipeline allows users to specify the reference direction for the output 
mosaics and over-ride the linmos default that uses the direction of a 
central beam as the reference direction for the output mosaic. 
See ``FORCE_FIELD_MOSAIC_CENTRES`` and ``FIELD_MOSAIC_CENTRE`` 
for details. 

Users can specify realistic beam-models (e.g., from holography observations), 
to correct for primary beam gains using ``ASKAP_PB_TT_IMAGE`` for correcting 
the MFS images, and ``ASKAP_PB_CUBE_IMAGE`` for correcting the cubes. 
Furthermore, full-Stokes beam models derived from holography can be used to 
additionally correct leakages of Stokes-I into the other Stokes parameters 
in off-axis regions of the beams. See below for more details on these parameters. 

If weights are constant across input images being mosaicked, linmos can be 
provided with the corresponding ascii text weights files. This feature makes 
use of the ``write.weightslog`` feature of imager and avoids the need for 
weights images that can be very large, especially for cubes (See :doc:`../calim/imager` 
for details). 

Note: Although ``USE_WEIGHTS_LOG_CONT``, ``USE_WEIGHTS_LOG_CONTCUBE`` and ``USE_WEIGHTS_LOG_SPECTRAL`` 
are exposed to the user, these may be safely left to their default values. The pipleine 
internally forces these to be consistent with ``WRITE_WEIGHTS_LOG_CONT``, 
``WRITE_WEIGHTS_LOG_CONTCUBE`` and ``WRITE_WEIGHTS_LOG_SPECTRAL`` respectively.

Due to the cutoff in the primary beam attenuation, there is likely to
be border of NaN (Not-A-Number) values around the edge of the
mosaic. This can be trimmed using linmos's ``linmos.trimming``
functionality, by setting one of ``LINMOS_CONT_TRIMMING``,
``LINMOS_CONTCUBE_TRIMMING``, ``LINMOS_SPECTRAL_TRIMMING``, according
to the type of mosaic. Options are either "conservative" or
"aggressive". This trimming will reduce the spatial extent of the
mosaic and hence the overall size of the image file.

+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| Variable                           | Default                                    | Parset equivalent                      | Description                                                  |
+====================================+============================================+========================================+==============================================================+
| ``DO_MOSAIC``                      | true                                       | none                                   | Whether to mosaic the individual beam images, forming a      |
|                                    |                                            |                                        | single, primary-beam-corrected image.                        |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``DO_MOSAIC_FIELDS``               | true                                       | none                                   | Whether to mosaic the different fields together (for when    |
|                                    |                                            |                                        | there is more than one in the MS). If set to false, only the |
|                                    |                                            |                                        | field-based mosaicking will be done.                         |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``JOB_TIME_LINMOS``                | ``JOB_TIME_DEFAULT`` (24:00:00)            | none                                   | Time request for mosaicking                                  |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``NUM_CORES_CONTCUBE_LINMOS``      | ``""``                                     | none                                   | Total number of cores used for each of the contcube          |
|                                    |                                            |                                        | mosaicking jobs. If blank (the default), the number used is  |
|                                    |                                            |                                        | equal to the smaller of the number of cores used for the     |
|                                    |                                            |                                        | continuum cube imaging (``NUM_CORES_CONTCUBE_SCI``) or the   |
|                                    |                                            |                                        | number of continuum channels.                                |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``CORES_PER_NODE_CONTCUBE_LINMOS`` | 6 (galaxy/petrichor), 32 (setonix)         | none                                   | Number of cores on each node used for each of the contcube   |
|                                    |                                            |                                        | mosaicking jobs.                                             |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``NCHAN_PER_CORE_SPECTRAL_LINMOS`` | 8 (galaxy/petrichor), 18 (setonix)         | none                                   | Number of channels to be handled at once by a single process |
|                                    |                                            |                                        | in the spectral-line mosaicking. This should divide evenly   |
|                                    |                                            |                                        | into the number of spectral channels. This will determine the|
|                                    |                                            |                                        | number of cores assigned to the spectral-line mosaicking,    |
|                                    |                                            |                                        | unless ``NUM_CORES_SPECTRAL_LINMOS`` is provided.            |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``NUM_CORES_SPECTRAL_LINMOS``      | ``""``                                     | none                                   | Total number of cores used for each of the spectral-line     |
|                                    |                                            |                                        | mosaicking jobs. If blank, the number used is deterined by   |
|                                    |                                            |                                        | the total number of channels and                             |
|                                    |                                            |                                        | ``NCHAN_PER_CORE_SPECTRAL_LINMOS``.                          |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``CORES_PER_NODE_SPECTRAL_LINMOS`` | 20 (galaxy), 64 (petrichor), 72 (setonix)  | none                                   | Number of cores on each node used for each of the            |
|                                    |                                            |                                        | spectral-line mosaicking jobs.                               |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``MOSAIC_IMAGECODE_CONT``          | ``"convrestored raw convaltrestored        | none                                   | The list of image codes that are made into continuum         |
|                                    | image residual"``                          |                                        | mosaics. See text for permitted values. Anything else will   |
|                                    |                                            |                                        | result in the pipeline exiting before jobs are submitted.    |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``MOSAIC_IMAGECODE_CONTCUBE``      | ``"convrestored raw image residual"``      | none                                   | The list of image codes that are made into continuum cube    |
|                                    |                                            |                                        | mosaics. See text for permitted values. Anything else will   |
|                                    |                                            |                                        | result in the pipeline exiting before jobs are submitted.    |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``MOSAIC_IMAGECODE_SPECTRAL``      | ``"restored contsub image residual"``      | none                                   | The list of image codes that are made into spectral          |
|                                    |                                            |                                        | mosaics. See text for permitted values. Anything else will   |
|                                    |                                            |                                        | result in the pipeline exiting before jobs are submitted.    |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``USE_WEIGHTS_LOG_CONT``           | true                                       | write.weightslog                       | If true, use weights from imager-generated the text file.    |
|                                    |                                            | (:doc:`../calim/linmos`)               |                                                              |
|                                    |                                            |                                        | Note: It will be set to ``WRITE_WEIGHTS_LOG_CONT`` in case an|
|                                    |                                            |                                        | inconsistency is encountered when running the pipeline.      |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``USE_WEIGHTS_LOG_CONTCUBE``       | true                                       | write.weightslog                       | If true, use weights from imager-generated the text file.    |
|                                    |                                            | (:doc:`../calim/linmos`)               |                                                              |
|                                    |                                            |                                        | Note: It will be set to ``WRITE_WEIGHTS_LOG_CONTCUBE`` in    |
|                                    |                                            |                                        | case an inconsistency is encountered when running the        |
|                                    |                                            |                                        | pipeline.                                                    |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``USE_WEIGHTS_LOG_SPECTRAL``       | true                                       | write.weightslog                       | If true, use weights from imager-generated the text file.    |
|                                    |                                            | (:doc:`../calim/linmos`)               |                                                              |
|                                    |                                            |                                        | Note: It will be set to ``WRITE_WEIGHTS_LOG_SPECTRAL`` in    |
|                                    |                                            |                                        | case an inconsistency is encountered when running the        |
|                                    |                                            |                                        | pipeline.                                                    |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``MOSAIC_SELFCAL_LOOPS``           | false                                      | none                                   | Whether to make mosaics of each self-calibration loop.       |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``FOOTPRINT_ROTATION_REFERENCE``   | ``""``                                     | none                                   | The reference rotation angle for the footprint. This should  |
|                                    |                                            |                                        | only be given if the scheduling block parset does not have   |
|                                    |                                            |                                        | the **common.src.%d.footprint.rotation** parameter, or if    |
|                                    |                                            |                                        | you want to over-ride that value. If not given, the          |
|                                    |                                            |                                        | footprint.rotation value will be used, or (in its absence),  |
|                                    |                                            |                                        | zero.                                                        |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``FP_ROTATION_HOLO``               | ``""``                                     | none                                   | The footprint.rotation parameter for the holography          |
|                                    |                                            |                                        | observation used to determine the shape & orientation of the |
|                                    |                                            |                                        | beams. Leave blank to use the metadata determined            |
|                                    |                                            |                                        | automatically from the holography SB parset.                 |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``FP_POLAXIS_ANGLE_HOLO``          | ``""``                                     | none                                   | The pol_axies rotation parameter for the holography          |
|                                    |                                            |                                        | observation used to determine the sahpe & orientation of the |
|                                    |                                            |                                        | beams. Leave blank to use the metadata determined            |
|                                    |                                            |                                        | automatically from the holography SB parset.                 |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``BEAM_FOOTPRINT_NAME``            | diamond                                    | none                                   | The name of the beam footprint. This needs to be recognised  |
|                                    |                                            |                                        | by the legacy tool *footprint.py*, which generates the       |
|                                    |                                            |                                        | offsets required by the linmos application.                  |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``BEAM_FOOTPRINT_PA``              | 0                                          | none                                   | The position angle of the beam footprint pattern. Passed to  |
|                                    |                                            |                                        | footprint.py.                                                |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``BEAM_PITCH``                     | ``""``                                     | none                                   | The pitch, or beam spacing, in degrees. Passed to            |
|                                    |                                            |                                        | footprint.py.                                                |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``FREQ_BAND_NUMBER``               | ``""``                                     | none                                   | Which frequency band are we in - determines beam arrangement |
|                                    |                                            |                                        | (1,2,3,4). Passed to footprint.py. If not given, the pitch   |
|                                    |                                            |                                        | value is used to set the beam separation. The band is        |
|                                    |                                            |                                        | overridden by the pitch as well.                             |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``NUM_BEAMS_FOOTPRINT``            | 36                                         | none                                   | The number of beams in the footprint. In regular operation,  |
|                                    |                                            |                                        | this will be determined from the footprint service, but will |
|                                    |                                            |                                        | need to be specified in the case of non-standard or BETA     |
|                                    |                                            |                                        | footprints.                                                  |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``LINMOS_BEAM_OFFSETS``            | no default                                 | feeds.beam{i}                          | Parset entries that specify the beam offsets for use by      |
|                                    |                                            | (:doc:`../calim/linmos`)               | linmos. Needs to have one entry for each beam being          |
|                                    |                                            |                                        | mosaicked. See above for an example. Only provide this if    |
|                                    |                                            |                                        | running footprint.py is not going to give you what you want  |
|                                    |                                            |                                        | (eg. non-standard beam locations).                           |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``LINMOS_BEAM_SPACING``            | ``"1deg"``                                 | feeds.spacing                          | Scale factor for beam arrangement, in format like ‘1deg’.    |
|                                    |                                            | (:doc:`../calim/linmos`)               | This should not be altered if you are using a standard       |
|                                    |                                            |                                        | footprint from footprint.py (ie. with                        |
|                                    |                                            |                                        | ``BEAM_FOOTPRINT_NAME``).                                    |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``LINMOS_CUTOFF``                  | 0.2                                        | linmos.cutoff                          | The primary beam cutoff, as a fraction of the peak           |
|                                    |                                            | (:doc:`../calim/linmos`)               |                                                              |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``LINMOS_PSF_REF``                 | 0                                          | linmos.psfref                          | Reference beam for PSF (0-based index of the list of beams,  |
|                                    |                                            | (:doc:`../calim/linmos`)               | *not* the beam number) - which beam to take the PSF          |
|                                    |                                            |                                        | information from.                                            |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``LINMOS_SINGLE_FIELD_WEIGHTTYPE`` | Combined                                   | linmos.weighttype                      | How to do the weighting in the first stage of mosaicking (all|
|                                    |                                            | (:doc:`../calim/linmos`)               | beams of a single field). Can be either "Combined" or        |
|                                    |                                            |                                        | "FromPrimaryBeamModel".                                      |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``LINMOS_DIRECTION_TYPE``          | J2000                                      | forms part of linmos.outputcentre      | The direction type to be used by the mosaic output from      |
|                                    |                                            | (:doc:`../calim/linmos`)               | linmos. A list of valid direction types can be found linked  |
|                                    |                                            |                                        | from the (:doc:`../calim/linmos`) page.                      |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``LINMOS_PROJECTION_TYPE``         | SIN                                        | forms part of linmos.outputcentre      | The projection type to be used by the mosaic output from     |
|                                    |                                            | (:doc:`../calim/linmos`)               | linmos. A list of valid projection types can be found linked |
|                                    |                                            |                                        | from the (:doc:`../calim/linmos`) page.                      |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``FORCE_FIELD_MOSAIC_CENTRES``     | true                                       | none                                   | Allows users to define the output mosaic reference direction |
|                                    |                                            |                                        | set by default to that of a central beam in the input list.  |
|                                    |                                            |                                        | If true, and ``FIELD_MOSAIC_CENTRE=""``, then the centres of |
|                                    |                                            |                                        | the corresponding field is used as reference direction for   |
|                                    |                                            |                                        | the output mosaic.                                           |
|                                    |                                            |                                        | If this is set to false, linmos will use the default.        |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``FIELD_MOSAIC_CENTRE``            | ``""``                                     | linmos.outputcentre                    | Using this parameter, users can define a specific RA-Dec to  |
|                                    |                                            | (:doc:`../calim/linmos`)               | be used as the reference direction for the output mosaic.    |
|                                    |                                            |                                        | RA-Dec string format ``"hh:mm:ss.ss,+dd.mm.ss.ss"``          |
|                                    |                                            |                                        | You must also set ``FORCE_FIELD_MOSAIC_CENTRES=true``.       |
|                                    |                                            |                                        | For SBIDs with multiple fields, it may be best to allow the  |
|                                    |                                            |                                        | pipeline to derive the reference direction based on the      |
|                                    |                                            |                                        | ``FORCE_FIELD_MOSAIC_CENTRES`` parameter.                    |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| **Primary beam correction**        |                                            |                                        |                                                              |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``OVERRIDE_PB_WEIGHTS_CHECK``      | false                                      | none                                   | Whether to ignore the fact that the weights ID for the       |
|                                    |                                            |                                        | holography and science observations may differ, and continue |
|                                    |                                            |                                        | on with the pipeline processing.                             |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``OVERRIDE_PB_FOOTPRINT_CHECK``    | false                                      | none                                   | Whether to ignore the fact that the footprint parameters for |
|                                    |                                            |                                        | the holography and science observations may differ, and      |
|                                    |                                            |                                        | continue on with the pipeline processing.                    |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``ASKAP_PB_TT_IMAGE``              | ``""``                                     | linmos.primarybeam.ASKAP_PB.image      | The 4D/5D ASKAP primary beam model FITS image to use for     |
|                                    |                                            | (:doc:`../calim/linmos`)               | correcting mosaics of MFS images. The 3rd dimension contains |
|                                    |                                            |                                        | models for Taylor-terms, while the 4th dimension has beams.  |
|                                    |                                            |                                        | The default ``""`` forces the correction using the           |
|                                    |                                            |                                        | analytical PB models in the code (viz., 2D Gaussians).       |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``ASKAP_PB_TT_IMAGE_NTERM``        | 3                                          | linmos.primarybeam.ASKAP_PB.nterms     | The number of Taylor-term planes to use from the specified   |
|                                    |                                            | (:doc:`../calim/linmos`)               | ``ASKAP_PB_TT_IMAGE`` when generating the correction.        |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``ASKAP_PB_CUBE_IMAGE``            | ``""``                                     | linmos.primarybeam.ASKAP_PB.image      | The 4D/5D ASKAP primary beam model FITS image to use for     |
|                                    |                                            | (:doc:`../calim/linmos`)               | correcting mosaics of the coarse and fine spectral cubes.    |
|                                    |                                            |                                        | The 3rd dimension contains models for different frequencies. |
|                                    |                                            |                                        | The correction for images at specific frequencies is derived |
|                                    |                                            |                                        | using interpolation/scaling scheme adopted in the "linmos"   |
|                                    |                                            |                                        | application.                                                 |
|                                    |                                            |                                        | The default ``""`` forces the correction using the           |
|                                    |                                            |                                        | analytical PB models in the code (viz., 2D Gaussians).       |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| **Trimming of mosaics**            |                                            |                                        |                                                              |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``LINMOS_CONT_TRIMMING``           | ``""``                                     | linmos.trimming.type                   | Whether to trim the border of the continuum image            |
|                                    |                                            | (:doc:`../calim/linmos`)               | mosaics. Value can be blank (``""``), where no trimming is   |
|                                    |                                            |                                        | done, or one of ``conservative`` or ``aggressive``.          |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``LINMOS_CONTCUBE_TRIMMING``       | ``""``                                     | linmos.trimming.type                   | Whether to trim the border of the continuum cube             |
|                                    |                                            | (:doc:`../calim/linmos`)               | mosaics. Value can be blank (``""``), where no trimming is   |
|                                    |                                            |                                        | done, or one of ``conservative`` or ``aggressive``.          |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+
| ``LINMOS_SPECTRAL_TRIMMING``       | ``""``                                     | linmos.trimming.type                   | Whether to trim the border of the spectral cube              |
|                                    |                                            | (:doc:`../calim/linmos`)               | mosaics. Value can be blank (``""``), where no trimming is   |
|                                    |                                            |                                        | done, or one of ``conservative`` or ``aggressive``.          |
+------------------------------------+--------------------------------------------+----------------------------------------+--------------------------------------------------------------+


Off-axis leakage correction
---------------------------

The following table summarises the parameters to turn on off-axis leakage correction. For the off-axis correction to take effect, the user must also specify a valid 5D 
``ASKAP_PB_TT_IMAGE`` for correcting the continuum images and a valid 5D ``ASKAP_PB_CUBE_IMAGE`` for correcting the image cubes (see above for description of the ASKAP_PB model images). 
The 5 dimensions referred to for the model beam images are: RA, Dec, Frequency/TaylorTerm, Polarisation and Beam. 


+----------------------------------------+-------------------------------------------+----------------------------------------+-----------------------------------------------------------------+
| Variable                               | Default                                   | Parset equivalent                      | Description                                                     |
+========================================+===========================================+========================================+=================================================================+
| ``REMOVE_LEAKAGE_OFF_AXIS_CONT``       | false                                     | linmos.removeleakage                   | If true, the 5D beam model specified by ``ASKAP_PB_TT_IMAGE``   |
|                                        |                                           | (:doc:`../calim/linmos`)               | will be used to correct leakage of Stokes-I in the off-axis     |
|                                        |                                           |                                        | regions of the single beam continuum images for a given         |
|                                        |                                           |                                        | Stokes parameter being mosaicked.                               |
+----------------------------------------+-------------------------------------------+----------------------------------------+-----------------------------------------------------------------+
| ``REMOVE_LEAKAGE_OFF_AXIS_CONTCUBE``   | false                                     | linmos.removeleakage                   | If true, the 5D beam model specified by ``ASKAP_PB_CUBE_IMAGE`` |
|                                        |                                           | (:doc:`../calim/linmos`)               | will be used to correct leakage of Stokes-I in the off-axis     |
|                                        |                                           |                                        | regions of the single beam coarse cube images for a given       |
|                                        |                                           |                                        | Stokes parameter being mosaicked.                               |
+----------------------------------------+-------------------------------------------+----------------------------------------+-----------------------------------------------------------------+
| ``REMOVE_LEAKAGE_OFF_AXIS_SPECTRAL``   | false                                     | linmos.removeleakage                   | If true, the 5D beam model specified by ``ASKAP_PB_CUBE_IMAGE`` |
|                                        |                                           | (:doc:`../calim/linmos`)               | will be used to correct leakage of Stokes-I in the off-axis     |
|                                        |                                           |                                        | regions of the single beam fine-channel cube images for a given |
|                                        |                                           |                                        | Stokes parameter being mosaicked.                               |
|                                        |                                           |                                        | NB: Currently the pipeline does spectral imaging for Stokes-I   |
|                                        |                                           |                                        | only. So this parameter does not take effect at the moment.     |
+----------------------------------------+-------------------------------------------+----------------------------------------+-----------------------------------------------------------------+
| ``REMOVE_LEAKAGE_OFF_AXIS_STOKES_V``   | true                                      | linmos.removeleakage                   | Allos the Stokes V mosaic to be treated differently. If the     |
|                                        |                                           | (:doc:`../calim/linmos`)               | relevant ``REMOVE_LEAKAGE_OFF_AXIS_`` parameter is true, then   |
|                                        |                                           |                                        | setting this to false will turn off the ``removeleakage`` linmos|
|                                        |                                           |                                        | parameter for the Stokes V image/cube only.                     |
+----------------------------------------+-------------------------------------------+----------------------------------------+-----------------------------------------------------------------+

