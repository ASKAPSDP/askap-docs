extractslice (spectral slice from a cube)
=========================================

This page provides instruction for extractslice program which can be used to extract spectral slices from
a multidimensional cube. It works via the standard interfaces and supports any image type currently supported
by the library. Also it is flexible in terms of the axis order and supports distributed access. As a result,
it is also a handy tool to explore I/O performance as it mimics access patterns which can be encountered in 
real-world astronomy applications. If the cube has more than 3 dimensions, the slice will be
presented as a flattened vector for all extra dimensions in additional columns of the resulting ascii file. Each
resulting file will have a header (started by the hash symbol) with general info about slice parameters. The first
column is 0-based channel number, followed by frequency or velocity (native units of the spectral axis are used,
no conversion is attempted), and then the extracted values (just one if there are no additional axis or four
polarisation products, each in a separate column, for a full polarisation cube). If polarisation axis is present
(even if it is degenerate), the header will contain the label listing the polarisation products in the order they
appear in the slice (natural order of the cube).

Running the program
-------------------

It can be run with the following command, where "config.in" is a file containing
the configuration parameters described in the next section. ::

   $ <MPI wrapper> extractslice -c config.in -l extractslice.log_cfg

The *extractslice* program supports execution in parallel and can use distributed I/O if supported by the
particular accessor selected in the "config.in" parameter file. If multiple ranks are available, slices to extract
are distributed between available ranks (note, no attempt is made to allocate slices in a particular way based on
their proximity - ranks are simply allocated in a round robin fashion).

Configuration Parameters
------------------------

The following table contains the configuration parameters to be specified in the *config.in*
file shown on above command line. Note that unlike the actual YandaSoft applications this
tool exercises interfaces directly and doesn't require any prefix for parameters.
In general, all parameters supported by image access factory can be used here. 

+------------------+------------------+--------------+------------------------------------------------------------+
|**Parameter**     |**Type**          |**Default**   |**Description**                                             |
+==================+==================+==============+============================================================+
|image             |string            |none          |Name of the input cube. There is no default.                |
+------------------+------------------+--------------+------------------------------------------------------------+
|mode              |string            |"parallel" or |This parameter can be either "parallel" or "serial" and     |
|                  |                  |"serial" as   |defines the type of the image accessor factory created.     |
|                  |                  |appropriate   |The default is "serial" if only one rank is available and   |
|                  |                  |              |"parallel" otherwise, so most users don't need to worry     |
|                  |                  |              |about it. However, it may be useful to force a particular   |
|                  |                  |              |type of accessor to exercise it in    unusual circumstances.|
|                  |                  |              |As this application requires only read access, creating a   |
|                  |                  |              |serial accessor in parallel would be equivalent to running a|
|                  |                  |              |number of jobs independently. Parallel accessor is passed   |
|                  |                  |              |a communicator and could perform optimised action, but this |
|                  |                  |              |is implementation detail for a particular accessor.         |
+------------------+------------------+--------------+------------------------------------------------------------+
|imagetype         |string            |"casa"        |Type of the image handler (determines the format of         |
|                  |                  |              |the images, this application requires only read access).    |
|                  |                  |              |The default is casa images but "fits" can also be chosen.   |
+------------------+------------------+--------------+------------------------------------------------------------+
|prefix            |string            |""            |Prefix added to the file name for all extracted slices      | 
|                  |                  |              |(i.e if the prefix is "temp/" and the slice name is src1,   |
|                  |                  |              |the slice will be written into the file "temp/src1.dat"     |
+------------------+------------------+--------------+------------------------------------------------------------+
|slices.names      |string vector     |none          |List of names, one for each slice requested. For each name  |
|                  |                  |              |in the list there should be matching "slices.name.direction"|
|                  |                  |              |keyword. Slice name is used to compose the file name where  |
|                  |                  |              |the result is written.                                      |
+------------------+------------------+--------------+------------------------------------------------------------+
|slices.\ **xxx**\ |string vector     |none          |Spatial direction where to extract slice **xxx**\ . Either a| 
|.direction        |                  |              |standard direction vector as understood by the :doc:`imager`|
|                  |                  |              |or                                                          |
|                  |                  |              |a special case where explicit pixel can be given, in which  |
|                  |                  |              |case the frame keyword should be 'pixel'. Pixel coordinates |
|                  |                  |              |do not have to be integer numbers. They will be rounded.    |
|                  |                  |              |The same applies to the physical coordinates (i.e. they are |
|                  |                  |              |not required to be spot on at the pixel.                    |
+------------------+------------------+--------------+------------------------------------------------------------+

Example
--------

Example extractslice parset to get two slices, one at explicit spatial pixel and the other at some physical
coordinate. 

.. code-block:: bash

    imagetype=fits

    image=testcube

    slices.names=[src1,src2]
    slices.src1.direction = [512, 512, pixel]
    slices.src2.direction = [19:37:6.1, -59.58.10.9, J2000]

